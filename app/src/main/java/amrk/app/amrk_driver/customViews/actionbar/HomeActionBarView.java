package amrk.app.amrk_driver.customViews.actionbar;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.activity.MainActivity;
import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.customViews.menu.NavigationDrawerView;
import amrk.app.amrk_driver.databinding.LayoutActionBarHomeBinding;
import amrk.app.amrk_driver.utils.session.UserHelper;

public class HomeActionBarView extends RelativeLayout {
    public LayoutActionBarHomeBinding layoutActionBarHomeBinding;
    NavigationDrawerView navigationDrawerView;
    DrawerLayout drawerLayout;
    private Context context;
    BaseViewModel baseViewModel = new BaseViewModel();

    public HomeActionBarView(Context context) {
        super(context);
        this.context = context;
        init();
    }


    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutActionBarHomeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_action_bar_home, this, true);
        setEvents();
    }

    private void setEvents() {
        layoutActionBarHomeBinding.imgHomeBarMenu.setOnClickListener(view -> connectDrawer(HomeActionBarView.this.drawerLayout, false));
    }

    public void changeDriverStatus(int online) {
        if (online == 1)
            ((MainActivity) context).homeActionBarView.layoutActionBarHomeBinding.imgOnline.setImageResource(R.drawable.switch_fill);
        else
            ((MainActivity) context).homeActionBarView.layoutActionBarHomeBinding.imgOnline.setImageResource(R.drawable.switchempty);
    }

    public void connectDrawer(DrawerLayout drawerLayout, boolean firstConnect) {
        baseViewModel.userData = UserHelper.getInstance(context).getUserData();
        if (firstConnect) {
            this.drawerLayout = drawerLayout;
            return;
        } else {
            if (drawerLayout.isDrawerOpen(GravityCompat.END))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    public void setNavigation(NavigationDrawerView navigationDrawerView) {
        this.navigationDrawerView = navigationDrawerView;
    }

    public void setTitle(String string) {
        layoutActionBarHomeBinding.tvHomeBarText.setText(string);
    }
}