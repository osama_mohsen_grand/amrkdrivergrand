package amrk.app.amrk_driver.customViews.menu;


import java.util.ArrayList;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.utils.resources.ResourceManager;

public class MenuViewModel extends BaseViewModel {
    MenuAdapter menuAdapter = new MenuAdapter();

    public MenuViewModel() {
        ArrayList<MenuModel> list = new ArrayList<>();
        list.add(new MenuModel("1", ResourceManager.getString(R.string.menuHome), R.drawable.ic_uber_home));
        list.add(new MenuModel("2", ResourceManager.getString(R.string.menuAccount), R.drawable.uber_user));
        list.add(new MenuModel("3", ResourceManager.getString(R.string.menuHistroy), R.drawable.uber_history));
        list.add(new MenuModel("4", ResourceManager.getString(R.string.menuNotifications), R.drawable.uber_notifications));
        list.add(new MenuModel("5", ResourceManager.getString(R.string.menuPayment), R.drawable.ic_uber_wallet));
        list.add(new MenuModel("6", ResourceManager.getString(R.string.privacy), R.drawable.ic_uber_terms));
        list.add(new MenuModel("7", ResourceManager.getString(R.string.menu_document), R.drawable.ic_docs));
//        list.add(new MenuModel("8", ResourceManager.getString(R.string.menu_categries), R.drawable.ic_menu_cat));
        list.add(new MenuModel("9", ResourceManager.getString(R.string.lang), R.drawable.ic_lang));
        list.add(new MenuModel("10", ResourceManager.getString(R.string.menu_about), R.drawable.ic_app));
        list.add(new MenuModel("11", ResourceManager.getString(R.string.contact_us), R.drawable.ic_contact));
        list.add(new MenuModel("12", ResourceManager.getString(R.string.exit), R.drawable.ic_uber_logout));
        menuAdapter.update(list);
    }

    public MenuAdapter getMenuAdapter() {
        return menuAdapter;
    }
}
