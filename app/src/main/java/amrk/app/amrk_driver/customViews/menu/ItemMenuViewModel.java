package amrk.app.amrk_driver.customViews.menu;

import androidx.databinding.Bindable;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.utils.Constants;

public class ItemMenuViewModel extends BaseViewModel {
    public MenuModel menuModel;

    public ItemMenuViewModel(MenuModel menuModel) {
        this.menuModel = menuModel;
    }

    @Bindable
    public MenuModel getMenuModel() {
        return menuModel;
    }

    public void itemAction() {
        //TODO Item Action with liveData
        getLiveData().setValue(Constants.MENu);
    }

}
