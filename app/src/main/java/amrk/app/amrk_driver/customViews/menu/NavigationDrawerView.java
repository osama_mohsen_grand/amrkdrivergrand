package amrk.app.amrk_driver.customViews.menu;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Window;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;

import java.util.Objects;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.ParentActivity;
import amrk.app.amrk_driver.customViews.actionbar.HomeActionBarView;
import amrk.app.amrk_driver.databinding.ExistLayoutBinding;
import amrk.app.amrk_driver.databinding.LayoutNavigationDrawerBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.balance.PaymentFragment;
import amrk.app.amrk_driver.pages.history.HistoryFragment;
import amrk.app.amrk_driver.pages.notifications.NotificationsFragment;
import amrk.app.amrk_driver.pages.profile.ProfileFragment;
import amrk.app.amrk_driver.pages.settings.AboutAppFragment;
import amrk.app.amrk_driver.pages.settings.ContactUsFragment;
import amrk.app.amrk_driver.pages.settings.DocumentsFragment;
import amrk.app.amrk_driver.pages.settings.LangFragment;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.helper.MovementHelper;
import amrk.app.amrk_driver.utils.resources.ResourceManager;
import amrk.app.amrk_driver.utils.session.LanguagesHelper;

import static amrk.app.amrk_driver.utils.resources.ResourceManager.getString;


public class NavigationDrawerView extends RelativeLayout {
    public MutableLiveData<Mutable> liveData;
    public LayoutNavigationDrawerBinding layoutNavigationDrawerBinding;
    AppCompatActivity context;
    HomeActionBarView homeActionBarView;
    private MenuViewModel menuViewModel;

    public NavigationDrawerView(AppCompatActivity context) {
        super(context);
        this.context = context;
        liveData = new MutableLiveData<>();
        init();
    }

    public NavigationDrawerView(AppCompatActivity context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();

    }

    public NavigationDrawerView(AppCompatActivity context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutNavigationDrawerBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_navigation_drawer, this, true);
        menuViewModel = new MenuViewModel();
        layoutNavigationDrawerBinding.setMenuViewModel(menuViewModel);
        setEvents();
    }

    public void setActionBar(HomeActionBarView homeActionBarView) {
        this.homeActionBarView = homeActionBarView;
    }

    private void setEvents() {
        menuViewModel.getMenuAdapter().getLiveDataAdapter().observeForever(position -> {
            layoutNavigationDrawerBinding.dlMainNavigationMenu.closeDrawer(GravityCompat.START);
            if (position == 1) { // account
                MovementHelper.startActivity(context, ProfileFragment.class.getName(), getString(R.string.menuAccountTitle), null);
            } else if (position == 2) { // history
                MovementHelper.startActivity(context, HistoryFragment.class.getName(), getString(R.string.menuHistroy), null);
            } else if (position == 3) {// notifications
                MovementHelper.startActivity(context, NotificationsFragment.class.getName(), getString(R.string.menuNotifications), null);
            } else if (position == 4) {// balance
                MovementHelper.startActivity(context, PaymentFragment.class.getName(), getString(R.string.menuPayment), null);
            } else if (position == 5) {// privacy
                MovementHelper.openCustomTabs(context, Constants.POLICY_URL + "2/" + LanguagesHelper.getCurrentLanguage(), ResourceManager.getString(R.string.privacy));
//                MovementHelper.startActivityWithBundle(context, new PassingObject(Constants.PRIVACY), ResourceManager.getString(R.string.privacy), TermsFragment.class.getName(), null);
            } else if (position == 6) {// docs
                MovementHelper.startActivity(context, DocumentsFragment.class.getName(), getString(R.string.menu_document), null);
            }
//            else if (position == 7) {// cars
//                MovementHelper.startActivity(context, CarCategoriesFragment.class.getName(), ResourceManager.getString(R.string.menu_categries), null);
//            }
            else if (position == 7) {// Lang
                MovementHelper.startActivity(context, LangFragment.class.getName(), getString(R.string.lang), null);
            } else if (position == 8) {// About
                MovementHelper.startActivity(context, AboutAppFragment.class.getName(), getString(R.string.menu_about), null);
            } else if (position == 9) {// Contact
                MovementHelper.startActivity(context, ContactUsFragment.class.getName(), getString(R.string.contact_us), null);
            } else if (position == 10) {// logout
                exitDialog(getString(R.string.log_out_text));
            }
        });
    }

    public void exitDialog(String text) {
        Dialog dialog = new Dialog(context, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ExistLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(dialog.getContext()), R.layout.exist_layout, null, false);
        dialog.setContentView(binding.getRoot());
        binding.logoutTxt.setText(text);
        binding.agree.setOnClickListener(v -> {
            dialog.dismiss();
            ((ParentActivity) context).handleActions(new Mutable(Constants.LOGOUT));
        });
        binding.decline.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }
}
