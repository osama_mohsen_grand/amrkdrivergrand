package amrk.app.amrk_driver.customViews.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.inputmethod.EditorInfo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.utils.helper.AppHelper;
import amrk.app.amrk_driver.utils.resources.ResourceManager;
import amrk.app.amrk_driver.utils.validation.Validate;


public class CustomEditText extends TextInputLayout {
    private int inputType, gravity = 0;
    private Drawable drawable;
    int validator = 0;
    boolean isCome = false, focusable = true;
    boolean isFirst = false;
    public int validatorCount = 0;
    public boolean notFirstTime = false;
    public boolean lastCaseWrong = false, lastCaseCorrect = false;

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(R.styleable.CustomEditText);
        typedArray.recycle();
        inputType(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(R.styleable.CustomEditText);
        validator = typedArray.getInt(R.styleable.CustomEditText_isValidate, 0);
        typedArray.recycle();
        inputType(context, attrs);
    }

    private void init() {
        setWillNotDraw(false);
        TextInputEditText editText = new TextInputEditText(new ContextThemeWrapper(getContext(), R.style.inputEditTextLayout));
        createEditBox(editText);
    }


    private void createEditBox(TextInputEditText editText) {
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        editText.setInputType(inputType);
        if (!focusable)
            editText.setFocusable(focusable);
        if (gravity != 0)
            editText.setGravity(gravity);
        editText.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        editText.setLayoutParams(layoutParams);
        addView(editText);
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    private static final String TAG = "CustomEditText";

    private void inputType(@NonNull Context context, @Nullable AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomEditText,
                0, 0);
        // Gets you the 'value' number - 0 or 666 in your example
        int n = a.getIndexCount();
        for (int i = 0; i < n; i++) {
            int attr = a.getIndex(i);
            //note that you are accessing standard attributes using your attrs identifier
            if (attr == R.styleable.CustomEditText_android_inputType) {
                inputType = a.getInt(attr, EditorInfo.TYPE_TEXT_VARIATION_NORMAL);
            } else if (attr == R.styleable.CustomEditText_drawable) {
                drawable = a.getDrawable(R.styleable.CustomEditText_drawable);
                drawable = RoundedBitmapDrawableFactory.create(context.getResources(), AppHelper.resizeVectorIcon(drawable, drawable.getMinimumWidth(), drawable.getMinimumHeight()));
            } else if (attr == R.styleable.CustomEditText_focusable) {
                focusable = a.getBoolean(R.styleable.CustomEditText_focusable, false);
            } else if (attr == R.styleable.CustomEditText_gravity) {
                gravity = a.getInt(R.styleable.CustomEditText_gravity, 0);
            }
        }
        a.recycle();
        init();
    }

    @BindingAdapter(value = "isValidateAttrChanged")
    public static void setListener(final CustomEditText view, final InverseBindingListener textAttrChanged) {
        if (textAttrChanged != null) {
            view.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

//                    if (view.notFirstTime) {
                    if (Validate.isEmpty(view.getEditText().getText().toString())) {
                        view.validatorCount = 0;
                        view.setError(ResourceManager.getString(R.string.fieldRequired));
                    } else if (view.getEditText().getInputType() == InputType.TYPE_CLASS_PHONE && !Validate.isPhone(view.getEditText().getText().toString())) {
                        view.validatorCount = 0;
                        view.setError("Wrong phone");
                    } else if ((view.getEditText().getInputType() == 33 || view.getEditText().getInputType() == InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS) && !Validate.isMail(view.getEditText().getText().toString())) {
                        view.validatorCount = 0;
                        view.setError(ResourceManager.getString(R.string.invalidEmail));
                    } else if ((view.getEditText().getInputType() == 129 || view.getEditText().getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD || view.getEditText().getInputType() == InputType.TYPE_NUMBER_VARIATION_PASSWORD ||
                            view.getEditText().getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                            || view.getEditText().getInputType() == InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD)
                            && !Validate.isPassword(view.getEditText().getText().toString())) {
                        view.validatorCount = 0;
                        view.setError(ResourceManager.getString(R.string.invalidPassword));
                    } else {
                        view.validatorCount = 1;
                        view.setFirst(true);
                        view.setError(null);
                    }


                    if (view.validatorCount == 0 && !view.lastCaseWrong && view.lastCaseCorrect) {
                        view.setValidator(view.getValidator() - 1);
                        view.lastCaseWrong = true;
                        view.lastCaseCorrect = false;
                        Log.e("validator", "Wrong " + view.validatorCount + "");

                    } else if (view.validatorCount == 1 && !view.lastCaseCorrect) {
                        Log.e("validator", "Correct " + view.validatorCount + " validor" + view.getValidator());
                        view.setValidator(view.getValidator() + 1);
                        Log.e(TAG, "afterTextChanged:view.getValidator() : " + view.getValidator());
                        view.lastCaseWrong = false;
                        view.lastCaseCorrect = true;
                    }
                    textAttrChanged.onChange();
//                    }
                    view.notFirstTime = true;
                }
            });
        }
    }

    @BindingAdapter("app:isValidate")
    public static void setError(CustomEditText view, int value) {
        Log.e(TAG, "setError: " + value);
        if (view.isCome && Validate.isEmpty(view.getEditText().getText().toString())) {
            view.setError(ResourceManager.getString(R.string.fieldRequired));
        }
        view.isCome = true;
        view.setValidator(value);

    }

    @InverseBindingAdapter(attribute = "isValidate")
    public static int getError(CustomEditText errorInputLayout) {
        return errorInputLayout.validator;
    }

    @BindingAdapter("app:text")
    public static void getText(CustomEditText view, String text) {
        view.getEditText().setText(text);
    }

    @InverseBindingAdapter(attribute = "text")
    public static String setText(CustomEditText customEditText) {
        return customEditText.getEditText().getText().toString();
    }

    @BindingAdapter(value = "textAttrChanged")
    public static void setTextListner(final CustomEditText view, final InverseBindingListener textAttrChanged) {
        if (textAttrChanged != null) {
            view.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    textAttrChanged.onChange();
                }
            });


        }
    }


    public void setValidator(int validator) {
        this.validator = validator;
    }

    public int getValidator() {
        Log.e(TAG, "getValidator: " + validator);
        return validator;
    }
}