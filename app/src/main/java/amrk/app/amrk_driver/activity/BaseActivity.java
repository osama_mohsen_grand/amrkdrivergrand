package amrk.app.amrk_driver.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import amrk.app.amrk_driver.PassingObject;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.base.ParentActivity;
import amrk.app.amrk_driver.customViews.actionbar.BackActionBarView;
import amrk.app.amrk_driver.databinding.ActivityBaseBinding;
import amrk.app.amrk_driver.pages.chat.view.ChatFragment;
import amrk.app.amrk_driver.pages.notifications.NotificationsFragment;
import amrk.app.amrk_driver.pages.splash.SplashFragment;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.helper.MovementHelper;
import amrk.app.amrk_driver.utils.resources.ResourceManager;

public class BaseActivity extends ParentActivity {
    private static final String TAG = "BaseActivity";
    ActivityBaseBinding activityBaseBinding;
    public BackActionBarView backActionBarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeLanguage();
        IApplicationComponent component = ((MyApplication) getApplicationContext()).getApplicationComponent();
        component.inject(this);
        activityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);
        backActionBarView = new BackActionBarView(this);
        getNotification();
        if (!notification_checked) {
            if (getIntent().hasExtra(Constants.PAGE)) {
                String fragmentName = getIntent().getStringExtra(Constants.PAGE);
                if (fragmentName != null) {
                    try {
                        Fragment fragment = (Fragment) Class.forName(fragmentName).newInstance();
                        MovementHelper.replaceFragmentTag(this, getBundle(fragment), fragmentName, "");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            } else
                MovementHelper.replaceFragment(this, new SplashFragment(), "");
        }
    }

    private void setTitleName(@Nullable String title) {
        if (title != null) {
            backActionBarView.setTitle(title);
        } else {
            if (getIntent().hasExtra(Constants.NAME_BAR)) {
                backActionBarView.setTitle(getIntent().getStringExtra(Constants.NAME_BAR));
            }
        }
        activityBaseBinding.llBaseActionBarContainer.addView(backActionBarView);

    }

    public void getNotification() {
        if (getIntent() != null && getIntent().getBooleanExtra("is_notification", false)) {
            if (getIntent().getSerializableExtra("type") != null) {
                notification_checked = true;
                String typeNotifications = getIntent().getStringExtra("type");
                String orderId = getIntent().getStringExtra("attribute_id");
                Bundle bundle = new Bundle();
                if (typeNotifications.equals("5")) {// chat
                    setTitleName(ResourceManager.getString(R.string.chat));
                    ChatFragment fragment = new ChatFragment();
                    bundle.putString(Constants.BUNDLE, new Gson().toJson(new PassingObject(Integer.parseInt(orderId))));
                    fragment.setArguments(bundle);
                    MovementHelper.replaceFragmentTag(this, fragment, fragment.getClass().getName(), "");
                } else {
                    setTitleName(ResourceManager.getString(R.string.menuNotifications));
                    NotificationsFragment fragment = new NotificationsFragment();
                    MovementHelper.replaceFragmentTag(this, fragment, fragment.getClass().getName(), "");
                }
            }
        }
    }

    private Fragment getBundle(Fragment fragment) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.BUNDLE, getIntent().getStringExtra(Constants.BUNDLE));
        fragment.setArguments(bundle);
        if (getIntent().hasExtra(Constants.NAME_BAR)) {
            setTitleName(null);
        }
        return fragment;
    }

    @Override
    public void onBackPressed() {
        try {
            if (dialogLoader != null && dialogLoader.isShowing()) {
                dialogLoader.dismiss();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        super.onBackPressed();
    }

}