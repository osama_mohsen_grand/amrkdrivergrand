package amrk.app.amrk_driver.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;

import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.base.ParentActivity;
import amrk.app.amrk_driver.customViews.actionbar.HomeActionBarView;
import amrk.app.amrk_driver.customViews.menu.NavigationDrawerView;
import amrk.app.amrk_driver.databinding.ActivityMainBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.home.HomeFragment;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.ImmediateUpdateActivity;
import amrk.app.amrk_driver.utils.helper.MovementHelper;
import amrk.app.amrk_driver.utils.services.TrackingService;
import amrk.app.amrk_driver.utils.session.UserHelper;

import static amrk.app.amrk_driver.utils.ImmediateUpdateActivity.UPDATE_REQUEST_CODE;

public class MainActivity extends ParentActivity {
    public HomeActionBarView homeActionBarView = null;
    ActivityMainBinding activityMainBinding;
    public NavigationDrawerView navigationDrawerView;
    @Inject
    MutableLiveData<Mutable> liveData;
    ImmediateUpdateActivity immediateUpdateActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeLanguage();
        setContentView(R.layout.activity_main);
        IApplicationComponent component = ((MyApplication) getApplicationContext()).getApplicationComponent();
        component.inject(this);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        homeActionBarView = new HomeActionBarView(this);
        navigationDrawerView = new NavigationDrawerView(this);
        activityMainBinding.llBaseContainer.addView(navigationDrawerView);
        navigationDrawerView.layoutNavigationDrawerBinding.llBaseActionBarContainer.addView(homeActionBarView, 0);
        homeActionBarView.setNavigation(navigationDrawerView);
        homeActionBarView.connectDrawer(navigationDrawerView.layoutNavigationDrawerBinding.dlMainNavigationMenu, true);
        navigationDrawerView.setActionBar(homeActionBarView);
        homeActionBarView.setTitle(getString(R.string.menuHome));
        homeActionBarView.changeDriverStatus(UserHelper.getInstance(this).getUserData().getOnline());
        new Handler().postDelayed(() -> MovementHelper.replaceFragment(this, new HomeFragment(), ""), 1000);
        immediateUpdateActivity = new ImmediateUpdateActivity(this);
        setEvents();

    }

    public void startTrackerService() {
        if (!isMyServiceRunning()) {
            Intent intent = new Intent(getApplicationContext(), TrackingService.class);
            intent.setAction(Constants.ACTION_START_LOCATION_SERVICE);
            startService(intent);
        }
    }

    public void stopTrackerService() {
        if (isMyServiceRunning()) {
            Intent intent = new Intent(getApplicationContext(), TrackingService.class);
            intent.setAction(Constants.ACTION_STOP_LOCATION_SERVICE);
            startService(intent);
        }
        //Notify the user that tracking has been enabled//
    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (TrackingService.class.getName().equals(service.service.getClassName())) {
                if (service.foreground)
                    return true;
            }
        }
        return false;
    }

    private void setEvents() {
//        navigationDrawerView.liveData.observe( this, new Observer<Object>() {
//            @SuppressLint("WrongConstant")
//            public void onChanged(@Nullable Object object) {
//                liveData.setValue(new Mutable());
//                Mutable mutable = (Mutable) object;
//                if(!mutable.message.equals(Constants.LOGIN_FIRST)) {
//                    homeActionBarView.setTitle(mutable.message);
//                    navigationDrawerView.layoutNavigationDrawerBinding.dlMainNavigationMenu.closeDrawer(Gravity.START);
//                }else {
//                    toastMessage(ResourceManager.getString(R.string.please_login_first), R.drawable.ic_info_white, R.color.colorPrimary);
//                }
//            }
//        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Picasso.get().load(UserHelper.getInstance(this).getUserData().getImage()).placeholder(R.drawable.logo).into(navigationDrawerView.layoutNavigationDrawerBinding.vvNavigationDrawerImage);
        updateAuto();
    }

    private void updateAuto() {
        immediateUpdateActivity.getAppUpdateManager().getAppUpdateInfo().addOnSuccessListener(it -> {
            if (it.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                try {
                    immediateUpdateActivity.getAppUpdateManager().startUpdateFlowForResult(it, AppUpdateType.IMMEDIATE, this, UPDATE_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setMessage(getString(R.string.exit_app))
                .setCancelable(false)
                .setNeutralButton(getString(R.string.cancel), (dialog, which) -> dialog.cancel())
                .setPositiveButton(getString(R.string.yes), (dialog, id) -> {
                    super.onBackPressed();
                    dialog.cancel();
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
