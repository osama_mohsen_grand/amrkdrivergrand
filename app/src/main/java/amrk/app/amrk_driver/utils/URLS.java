package amrk.app.amrk_driver.utils;

public class URLS {
    public final static String BASE_URL = "https://amrrek.net/api/";
//    public final static String BASE_URL = "https://amrk1.my-staff.net/api/";

    public final static String HOME = "captin/collected-money-today";
    public final static String LOGIN_PHONE = "captin/phone-check/";
    public final static String LOGIN_PASSWORD = "captin/login";
    public static final String FORGET_PASSWORD = "captin/code-send/";
    public final static String GET_COUNTRIES_CODE = "app/countries-codes";
    public final static String BOARD = "app/app-explanation/2/0";
    public final static String PRIVACY = "app/app-explanation/2/1";
    public final static String COUNTRIES = "app/countries";
    public final static String REGISTER = "captin/register";
    public final static String UPDATE_PROFILE = "captin/update-profile";
    public final static String CONFIRM_CODE = "captin/code-check";
    public final static String CITIES = "app/cities?country_id=";
    public final static String CARS_NATIONALS = "app/cars-nationals?type=2";
    public static final String TRIP_HISTORY = "captin/trip_history/";
    public final static String ABOUT = "app/about-us?type=2";
    public final static String TERMS = "app/terms?type=2";
    public final static String BANK_ACCOUNTS = "user-uber/bank_accounts";
    public final static String ADD_BANK_ACCOUNT = "captin/add_bank_transfer";
    public static final String BANKING_TRANSFER = "captin/banking_transfers";
    public final static String NOTIFICATIONS = "app/get-notifications?type=2";
    public static final String CREDITS = "captin/get-credits";
    public final static String GET_USER_DOCUMENTS = "captin/driver-documents";
    public final static String GET_DRIVER_SUBSCRIPTIONS = "app/get-subscription?type=2";
    public static final String UPDATE_LOCATION = "captin/update_location";
    public final static String CAR_LEVELS = "captin/get-my-car-levels";
    public final static String UPDATE_CARS_LEVELS = "captin/update-my-car-levels";
    public static final String CHANGE_STATUS = "captin/change_status";
    public final static String CONTACT = "app/contact-us";
    public static final String CHAT = "user-uber/chat_history?is_captin=1&trip_id=";
    public static final String SEND_MESSAGE = "user-uber/add_message";
    public static final String COLLECT_MONEY = "captin/collect_money";
    public static final String RATE_TRIP = "captin/rate_trip";
    public static final String GET_CANCEL_REASONS = "user-uber/cancelling_reasons?is_captin=1";
    public static final String PUT_RESET = "captin/put-in-wallet?money=";
    public static final String CHECK_PAYMENT = "app/payment";
    public static final String UPDATE_BUSY = "captin/update-busy";
}
