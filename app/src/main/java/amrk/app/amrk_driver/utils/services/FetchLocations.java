package amrk.app.amrk_driver.utils.services;


import java.util.ArrayList;

import amrk.app.amrk_driver.pages.home.models.EndTripLocationsRequest;

public interface FetchLocations {
    public void locations(ArrayList<EndTripLocationsRequest> locations);
}
