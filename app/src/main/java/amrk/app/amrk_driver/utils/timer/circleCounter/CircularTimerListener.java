package amrk.app.amrk_driver.utils.timer.circleCounter;

public interface CircularTimerListener {
    String updateDataOnTick(long remainingTimeInMs);
    void onTimerFinished();
}
