package amrk.app.amrk_driver.utils.locations;

import android.location.Location;

public interface FetchLocationListener {
    public void location(Location location);
}
