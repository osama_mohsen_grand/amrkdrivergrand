package amrk.app.amrk_driver.utils.services;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Objects;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.helper.AppHelper;
import amrk.app.amrk_driver.utils.locations.MapConfig;
import amrk.app.amrk_driver.utils.resources.ResourceManager;
import amrk.app.amrk_driver.utils.session.UserHelper;

public class TrackingService extends Service implements RoutingListener {
    double latitude, longitude;
    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Drivers");
    GeoFire geoFire = new GeoFire(ref);
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    double lastLat = Double.parseDouble(UserHelper.getInstance(MyApplication.getInstance()).getTripLastLat());
    double lastLng = Double.parseDouble(UserHelper.getInstance(MyApplication.getInstance()).getTripLastLng());
    int lastDistance = UserHelper.getInstance(MyApplication.getInstance()).getTripLastDistance();

    DatabaseReference tripsRef = null, tripsRefDriver = null;
    Long tsLong;
    String timestamp = "";
    LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(@NonNull LocationResult locationResult) {
            super.onLocationResult(locationResult);
            locationResult.getLastLocation();
            latitude = locationResult.getLastLocation().getLatitude();
            longitude = locationResult.getLastLocation().getLongitude();
            if (UserHelper.getInstance(MyApplication.getInstance()).getTripId() != 0) {
                latitude = locationResult.getLastLocation().getLatitude();
                longitude = locationResult.getLastLocation().getLongitude();
                float speed = locationResult.getLastLocation().getSpeed();
//                if (tripsRef == null && tripsRefDriver == null) {
//                    tripsRef = FirebaseDatabase.getInstance().getReference("Trips")
//                            .child(String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getTripId()));
//                    tripsRefDriver = FirebaseDatabase.getInstance().getReference("Trips")
//                            .child(String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getTripId()))
//                            .child(String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getUserData().getId()));
//                }
//                tsLong = System.currentTimeMillis() / 1000;
//                timestamp = tsLong.toString();
//                tripsRef.child(timestamp).child("lat").setValue(latitude);
//                tripsRef.child(timestamp).child("lng").setValue(longitude);
//                tripsRef.child(timestamp).child("speed").setValue(speed);
//                // for lastKnown location of amrk_driver
//                try {
//                    tripsRefDriver.child("lat").setValue(latitude);
//                    tripsRefDriver.child("lng").setValue(longitude);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                findRoutes();
            } else {
                if (UserHelper.getInstance(MyApplication.getInstance()).getUserData() != null)
                    geoFire.setLocation(mAuth.getCurrentUser().getUid(), new GeoLocation(latitude, longitude), (key, error) -> {
//                            Log.e("onLocationResult", "onLocationResult: "+error.getMessage() );
                    });
            }
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not implemen");
    }

    private void startLocationService() {
        String channelId = "location_notification_channel";
        Intent resultIntent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Location service")
                        .setContentText("Running")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri).
                        setPriority(NotificationCompat.PRIORITY_MAX)
                        .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0, notificationBuilder.build());

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(3000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.getFusedLocationProviderClient(this)
                .requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        startForeground(Constants.LOCATION_SERVICE_ID, notificationBuilder.build());
    }

    private void stopLocationService() {
        if (UserHelper.getInstance(MyApplication.getInstance()).getUserData() != null && UserHelper.getInstance(MyApplication.getInstance()).getTripId() == 0) {
            geoFire.removeLocation(String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getUserData().getId()));
            LocationServices.getFusedLocationProviderClient(this)
                    .removeLocationUpdates(locationCallback);
        } else {
            if (UserHelper.getInstance(MyApplication.getInstance()).getUserData() != null && UserHelper.getInstance(MyApplication.getInstance()).getTripStatus() == 0) {
//                if (tripsRef == null) {
//                    tripsRef = FirebaseDatabase.getInstance().getReference("Trips")
//                            .child(String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getTripId()));
//                }
//                tripsRef.removeValue();
                UserHelper.getInstance(MyApplication.getInstance()).addTripStatus(0);
                UserHelper.getInstance(MyApplication.getInstance()).addLastTripInfo(0, "0.0", "0.0"); // reset  values
                LocationServices.getFusedLocationProviderClient(this)
                        .removeLocationUpdates(locationCallback);

            }
        }
        stopForeground(true);
        stopSelf();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equals(Constants.ACTION_START_LOCATION_SERVICE)) {
                    loginToFirebase();
                } else if (action.equals(Constants.ACTION_STOP_LOCATION_SERVICE)) {
                    stopLocationService();
                }
            }
        }
        return super.onStartCommand(intent, flags, startId);

    }

    private void loginToFirebase() {
        String email = UserHelper.getInstance(MyApplication.getInstance()).getUserData().getEmail();
        String password = "12345678";

        FirebaseAuth.getInstance().signInWithEmailAndPassword(
                email, password).addOnCompleteListener(task -> {
            //If the user has been authenticated...//
            if (task.isSuccessful()) {
                //...then call requestLocationUpdates//
                startLocationService();
            } else {
                //If sign in fails, then log the error//
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        startLocationService();
                    }
                });
            }
        });
    }

    private void findRoutes() {
        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .waypoints(new LatLng(latitude, longitude), lastLat != 0.0 ? new LatLng(lastLat, lastLng) : new LatLng(latitude, longitude))
                .key(getResources().getString(R.string.google_map))  //also define your api key here.
                .build();
        routing.execute();
    }

    @Override
    public void onRoutingFailure(RouteException e) {

    }

    @Override
    public void onRoutingStart() {
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {

        //add route(s) to the map using polyline
        for (int i = 0; i < route.size(); i++) {
            if (i == shortestRouteIndex) {
                UserHelper.getInstance(MyApplication.getInstance()).addLastTripInfo((lastDistance + route.get(i).getDistanceValue()), String.valueOf(latitude), String.valueOf(longitude));
            }
        }
    }

    @Override
    public void onRoutingCancelled() {

    }

}
