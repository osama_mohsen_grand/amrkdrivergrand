package amrk.app.amrk_driver.utils;

public class Constants {
    public static final String GET_COUNTRIES_CODE = "GET_COUNTRIES_CODE";
    public static final String COUNTRIES = "COUNTRIES";
    public static final String SELECT_COUNTRY = "SELECT_COUNTRY";
    public static final String TERMS_URL = "https://amrrek.net/terms/";
    public static final String POLICY_URL = "https://amrrek.net/privacy/";

    public final static String PHONE_VERIFIED = "PHONE_VERIFIED";
    public final static String ERROR_NOT_FOUND = "not_found";
    public final static String CONFIRM_CODE = "CONFIRM_CODE";
    public final static String NOT_MATCH_PASSWORD = "NOT_MATCH_PASSWORD";

    public final static String CITIES = "CITIES";
    public final static String EMAIL = "email";
    public final static String FIELD = "FIELD";
    public final static String CARS_NATIONALS = "CARS_NATIONALS";
    public static final String CONTACT = "CONTACT";

    public final static String UPDATE_PROFILE = "update_profile";

    public static final String LAT = "LAT";
    public static final String LNG = "LNG";
    public static final String CITY = "CITY";

    public final static String IMAGE = "image";
    public final static String WARNING = "WARNING";
    public final static String TRIP_HISTORY = "TRIP_HISTORY";

    public final static String LOGOUT = "logout";
    public final static String TRIP_TYPE = "TRIP_TYPE";
    public final static String NOT_VERIFIED = "NOT_VERIFIED";
    public final static String BACKGROUND_API = "BACKGROUND_API";

    public final static String BANKING_TRANSFER = "BANKING_TRANSFER";
    public final static String BANKING_RECEIVABLE = "BANKING_RECEIVABLE";
    public final static String ADD_BALANCE = "ADD_BALANCE";
    public final static String RENEW_SUBSCRIPTIONS = "RENEW_SUBSCRIPTIONS";
    public final static String DATE_SUBSCRIPTIONS = "DATE_SUBSCRIPTIONS";
    public final static String ERROR_TOAST = "error_toast";

    public final static String ERROR = "error";
    public final static String SHOW_PROGRESS = "showProgress";
    public final static String HIDE_PROGRESS = "hideProgress";
    public final static String SERVER_ERROR = "serverError";
    public final static String BANKING_ACCOUNTS = "BANKING_ACCOUNTS";
    public final static String ADD_BANK_TRANSFER = "ADD_BANK_TRANSFER";
    public final static String FAILURE_CONNECTION = "failure_connection";
    public final static String GET_USER_DOCUMENTS = "GET_USER_DOCUMENTS";

    public final static String LANGUAGE = "language";
    public static final String DEFAULT_LANGUAGE = "ar";
    public static final String SHARE_BAR = "SHARE_BAR";
    public static final String PAGE = "page";
    public final static String NAME_BAR = "name_bar";
    public static final String BUNDLE = "bundle";
    public static final String CREDITS = "CREDITS";
    public static final String UPDATE_LOCATION = "UPDATE_LOCATION";
    public static final String CAR_LEVELS = "CAR_LEVELS";
    public static final String UPDATE_CARS_LEVELS = "UPDATE_CARS_LEVELS";
    public static final String GOOGLE_API = "GOOGLE_API";
    public static final String CHANGE_STATUS = "CHANGE_STATUS";
    public static final String CANCEL_REASONS = "CANCEL_REASONS";
    public static final String SHOW_CALL_DIALOG = "SHOW_CALL_DIALOG";
    public static final String CALL_CLIENT = "CALL_CLIENT";
    public static final String HIDE_CALL_DIALOG = "HIDE_CALL_DIALOG";
    public static final int LOCATION_SERVICE_ID = 175;
    public static final String ACTION_START_LOCATION_SERVICE = "startLocationService";
    public static final String ACTION_STOP_LOCATION_SERVICE = "stopLocationService";

    public static final String front_car_image = "front_car_image";
    public static final int front_car_image_code = 380;

    public static final String back_car_image = "back_car_image";
    public static final int back_car_code = 381;

    public static final String insurance_image = "insurance_image";
    public static final int insurance_image_code = 382;

    public static final String license_image = "license_image";
    public static final int license_image_code = 383;

    public static final String civil_image = "civil_image";
    public static final int civil_image_code = 384;
    public static final String identity_image = "id_image";
    public static final int identity_image_code = 385;

    //REQUESTS
    public final static int POST_REQUEST = 200;
    public final static int GET_REQUEST = 201;
    public final static int DELETE_REQUEST = 202;
    public final static int CHECK_CONFIRM_NAV_REGISTER = 0;
    public final static int CHECK_CONFIRM_NAV_FORGET = 1;

    //RESPONSES
    public static final int RESPONSE_SUCCESS = 200;
    public final static int RESULT_CODE = 203;
    public static final int DOC_REQUIRED = 0;
    public static final int RESPONSE_JWT_EXPIRE = 403;
    public static final int RESPONSE_405 = 405;
    public static final int RESPONSE_404 = 404;
    public final static String HOME = "HOME";
    public final static int FILE_TYPE_IMAGE = 378;
    public final static int FILE_TYPE_DOCUMENT = 379;

    public static final String TERMS = "contact_us";
    public static final String PRIVACY = "PRIVACY";
    public static final String SEND_MESSAGE = "SEND_MESSAGE";
    public static final String ABOUT = "about";
    public static final String MAP_WITH_START_ADDRESS = "MAP_WITH_START_ADDRESS";
    public static final String FULL_ADDRESS = "FULL_ADDRESS";
    public static final String LOGIN = "login";
    public static final String PICK_MONEY = "PICK_MONEY";
    public static final String FORGET_PASSWORD = "forget_password";
    public static final String COLLECT_MONEY = "COLLECT_MONEY";
    public static final String PUT_RESET_MONEY = "PUT_RESET_MONEY";
    public static final String UPDATE_BUSY = "UPDATE_BUSY";
    public static final String RATE_TRIP = "RATE_TRIP";
    public static final String CANCEL_TRIP = "CANCEL_TRIP";
    public static final String REGISTER = "register";
    public static final String CURRENT_LOCATION = "CURRENT_LOCATION";
    public static final String PICK_UP_LOCATION = "PICK_UP_LOCATION";
    public static final String SEND_PAYMENT = "SEND_PAYMENT";
    public static final String PAYMENT_METHOD = "PAYMENT_METHOD";
    public static final String MY_FATOORA_METHODS = "MY_FATOORA_METHODS";
    public static final String CHECK_PAYMENT = "CHECK_PAYMENT";
    public  static boolean DATA_CHANGE = false;
    public static final String SEARCH_LOCATION = "SEARCH_LOCATION";
    public static final String PICKED_SUCCESSFULLY = "PICKED_SUCCESSFULLY";
    public final static int AUTOCOMPLETE_REQUEST_CODE = 203;
    public static final String NOTIFICATIONS = "notifications";
    public static final String CHAT = "chat";
    public static final String MENU_HOME = "menu_home";
    public static final String NEXT = "next";
    public static final String BOARD = "board";
    public static final String MENu = "menu";
    public static final int TRIP_WAITING = 0;
    public static final int TRIP_ACCEPTED = 1;
    public static final int TRIP_START = 2;
    public static final int TRIP_ARRIVE = 5;
    public static final int TRIP_FINISH = 3;
    public static final int TRIP_CANCEL = 4;
    public static final int UNPAID = 0;

    //PUSHER
//    public static final String PUSHER_TOKEN_URL="https://amrk1.my-staff.net/api/captin/generate-beams-token";
    public static final String PUSHER_TOKEN_URL=URLS.BASE_URL.concat("captin/generate-beams-token");

    public static final String tripEventName="App\\Events\\SendTripEvent";
    public static final String tripBeamName="drivers-";
    public static final String tripChannelName="trip-";

    //Chat
    public static final String chatEventName="App\\Events\\SendMessageEvent";
    public static final String chatChannelName="message-";
    public static final String PUSHER_LIVE_DATA ="PUSHER_CHAT";

}

