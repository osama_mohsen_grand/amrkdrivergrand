package amrk.app.amrk_driver.utils.services;

import com.google.gson.annotations.SerializedName;

public class LastDriverLocation{
	@SerializedName("lng")
	private double lng;

	@SerializedName("lat")
	private double lat;

	public double getLng(){
		return lng;
	}

	public double getLat(){
		return lat;
	}

}