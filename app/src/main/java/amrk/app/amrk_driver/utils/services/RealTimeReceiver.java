package amrk.app.amrk_driver.utils.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;

import amrk.app.amrk_driver.pages.chat.model.Chat;

public class RealTimeReceiver extends BroadcastReceiver {
    public static MessageReceiverListener messageReceiverListene;
    public static NewTripReceiverListener tripReceiverListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            try {
                if (intent.getAction().equals("app.te.receiver")) {
                    if (intent.getSerializableExtra("uber_chat") != null) {
                        String details = String.valueOf(intent.getSerializableExtra("uber_chat"));
                        Chat messagesItem = new Gson().fromJson(details, Chat.class);
                        messageReceiverListene.onMessageChanged(messagesItem);
                    }
                    if (intent.getSerializableExtra("trip_details") != null) {
                        String details = String.valueOf(intent.getSerializableExtra("trip_details"));
                        Log.e("onReceive", "onReceive:Trip " + details);
//                        TripDataFromNotifications messagesItem = new Gson().fromJson(details, TripDataFromNotifications.class);
                        tripReceiverListener.onTripChanged(details);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else
            Log.i("onReceive", "onReceive: null");
    }

    public interface MessageReceiverListener {
        void onMessageChanged(Chat messagesItem);
    }

    public interface NewTripReceiverListener {
        void onTripChanged(String dataFromNotification);
    }
}
