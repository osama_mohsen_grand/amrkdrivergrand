package amrk.app.amrk_driver.utils.helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.model.LatLng;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.pushnotifications.BeamsCallback;
import com.pusher.pushnotifications.PushNotifications;
import com.pusher.pushnotifications.PusherCallbackError;
import com.pusher.pushnotifications.auth.AuthData;
import com.pusher.pushnotifications.auth.BeamsTokenProvider;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.pages.home.models.TripDataFromNotifications;
import amrk.app.amrk_driver.utils.session.LanguagesHelper;

/**
 * Created by osama on 03/01/2018.
 */

public class AppHelper {
    public static void setBackgroundNoData(Context context, ScrollView view) {
        //ToDO setBackgroundNoData Image drawable
        view.setBackground(ContextCompat.getDrawable(context, R.drawable.ccp_down_arrow));
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) MyApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static DatePickerDialog initCalender(Context context, boolean startFromNow, DatePickerDialog.OnDateSetListener datePickerDialog) {
        Calendar calendar = Calendar.getInstance();
        int day = 6, month = 6, year = 1990;
        if (startFromNow) {
            day = calendar.get(Calendar.DAY_OF_MONTH);
            month = calendar.get(Calendar.MONTH);
            year = calendar.get(Calendar.YEAR);
        }
        DatePickerDialog dPickerDialog = new DatePickerDialog(context, R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Calendar, datePickerDialog, year, month, day);
        if (startFromNow)
            dPickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        dPickerDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

//                dPickerDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setPadding(5,5,5,5);
//                dPickerDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setPadding(5,5,5,5);
            }
        });
        return dPickerDialog;
    }

    public static Bitmap resizeIcon(Drawable drawable) {
        int height = 50;
        int width = 50;
        BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
        Bitmap b = bitmapDrawable.getBitmap();
        return Bitmap.createScaledBitmap(b, width, height, false);
    }

    public static void restartApp(Activity activity) {
        Intent i = activity.getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(activity.getBaseContext().getPackageName());
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.finish();
        activity.startActivity(i);
    }


    public static int getScreenWidth(Context context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

    public static int getScreenHeight(Context context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.heightPixels;
    }


    public static String dateConvert(int year, int month, int day) {
        int month_select = month + 1;
        String month_final = String.valueOf(month_select), day_final = String.valueOf(day);
        if (month_select < 10) {

            month_final = "0" + month_final;
        }
        if (day < 10) {
            day_final = "0" + day_final;
        }
        String date_show = String.valueOf(year) + "-" + String.valueOf(month_final) + "-" + String.valueOf(day_final);
        return date_show;
    }

    public static void closeApp(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    public static void shareApp(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String shareSub = activity.getString(R.string.app_name);
        String shareBody = getPlayStoreLink(activity);
        intent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
        intent.putExtra(Intent.EXTRA_TEXT, shareBody);
//        activity.startActivity(Intent.createChooser(intent, activity.getString(R.string.share)));
        activity.startActivity(Intent.createChooser(intent, "share"));
    }

    public static String getPlayStoreLink(Context context) {
        final String appPackageName = context.getPackageName();
        return "https://play.google.com/store/apps/details?id=" + appPackageName;
    }

    public static void shareCustom(Activity activity, String title, String message) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        final String appPackageName = MyApplication.getInstance().getPackageName();
        message += "\n\nhttps://play.google.com/store/apps/details?id=" + appPackageName;
        intent.putExtra(Intent.EXTRA_SUBJECT, title);
        intent.putExtra(Intent.EXTRA_TEXT, message);
//        activity.startActivity(Intent.createChooser(intent, activity.getString(R.string.share)));
        activity.startActivity(Intent.createChooser(intent, "share"));

    }

    public static void openWhats(Activity activity, String number) {
        Uri uri = Uri.parse("smsto:" + number);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        activity.startActivity(Intent.createChooser(i, ""));
    }

    public static void openDialNumber(Context context, String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        context.startActivity(intent);
    }

    public static void openBrowser(Context context, String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);
    }


    public static void openEmail(Context context, String email) {

        String mailto = "mailto:" + email + "?" +
//                "?cc=" + "sales@2grand.net" +
                "&subject=" + context.getString(R.string.app_name) +
                "&body=";

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse(mailto));

        try {
            context.startActivity(emailIntent);
        } catch (ActivityNotFoundException e) {
            //TODO: Handle case where no email app is available
            Toast.makeText(context, "No Server Mail Application Found", Toast.LENGTH_SHORT).show();
        }


    }

    @SuppressLint("WrongConstant")
    public static void initVerticalRV(RecyclerView recyclerView, Context context, int spanCount) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(30);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(new GridLayoutManager(context, spanCount, LinearLayoutManager.VERTICAL, false));
    }

    @SuppressLint("WrongConstant")
    public static void initHorizontalRV(RecyclerView recyclerView, Context context, int spanCount) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(30);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(new GridLayoutManager(context, spanCount, LinearLayoutManager.HORIZONTAL, false));
    }

    public static String numberToDecimal(int number) {
        String number2Decimal = String.valueOf(number);
        if (number < 10) {
            number2Decimal = "0" + number2Decimal;
        }
        return number2Decimal;
    }


    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static Bitmap resizeVectorIcon(Drawable drawable, int width, int height) {
        try {
            Bitmap bitmap;
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, width, height);
            drawable.draw(canvas);
            return bitmap;
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    public static void startAndroidGoogleMap(Context context, LatLng source) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr=" + source.latitude + "," + source.longitude));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    public static void startAndroidGoogleMap(Context context, LatLng source, TripDataFromNotifications dataFromNotifications) {
        LatLng startLatLng = null, midLatLng = null, endLatLng = null;
        startLatLng = new LatLng(Double.parseDouble(dataFromNotifications.getTripPaths().get(0).getLat()), Double.parseDouble(dataFromNotifications.getTripPaths().get(0).getLng()));
        if (dataFromNotifications.getTripPaths().size() == 2)
            endLatLng = new LatLng(Double.parseDouble(dataFromNotifications.getTripPaths().get(1).getLat()), Double.parseDouble(dataFromNotifications.getTripPaths().get(1).getLng()));
        else if (dataFromNotifications.getTripPaths().size() == 3) {
            midLatLng = new LatLng(Double.parseDouble(dataFromNotifications.getTripPaths().get(1).getLat()), Double.parseDouble(dataFromNotifications.getTripPaths().get(1).getLng()));
            endLatLng = new LatLng(Double.parseDouble(dataFromNotifications.getTripPaths().get(2).getLat()), Double.parseDouble(dataFromNotifications.getTripPaths().get(2).getLng()));
        }
        Uri gmmIntentUri;
        if (midLatLng == null)
            gmmIntentUri = Uri.parse("https://www.google.com/maps/dir/?api=1&origin=" + source.latitude + "," + source.longitude + "&destination=" + Objects.requireNonNull(endLatLng).latitude + "," + endLatLng.longitude + "&travelmode=driving");
        else
            gmmIntentUri = Uri.parse("https://www.google.com/maps/dir/?api=1&origin=" + source.latitude + "," + source.longitude + "&destination=" + endLatLng.latitude + "," + endLatLng.longitude + "&waypoints=" + midLatLng.latitude + "," + midLatLng.longitude + "&travelmode=driving");
        Intent intent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        intent.setPackage("com.google.android.apps.maps");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            try {
                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                context.startActivity(unrestrictedIntent);
            } catch (ActivityNotFoundException innerEx) {
                Log.e("startAndroidGoogleMap", "startAndroidGoogleMap: " + innerEx.getMessage());
            }
        }

    }

    public static void connectPusher(String eventName, String beamName,String channelName) {
        PusherOptions options = new PusherOptions();
        options.setCluster("eu");
        Pusher pusher = new Pusher("75b53060b1f415501d21", options);
        pusher.connect();
        Channel channel = pusher.subscribe(channelName);
        Log.e("connectPusher", "connectPusher: "+channel );
        channel.bind(eventName, event -> Log.e("onEvent", "onEvent: " + event.getData()));
        PushNotifications.start(MyApplication.getInstance(), "4b9224fd-ac88-4e17-86bf-65c347bc0fbd");
        PushNotifications.addDeviceInterest("drivers");
        BeamsTokenProvider tokenProvider = new BeamsTokenProvider(
                "https://amrk1.my-staff.net/api/captin/generate-beams-token",
                () -> {
                    // Headers and URL query params your auth endpoint needs to
                    // request a Beams Token for a given user
                    HashMap<String, String> headers = new HashMap<>();
                    // for example:
                    headers.put("jwt", LanguagesHelper.getJwt());
                    HashMap<String, String> queryParams = new HashMap<>();
                    return new AuthData(
                            headers,
                            queryParams
                    );
                }
        );

        PushNotifications.setUserId(beamName, tokenProvider, new BeamsCallback<Void, PusherCallbackError>() {
            @Override
            public void onSuccess(@NonNull Void... values) {
                Log.e("PusherBeams", "Successfully authenticated with Pusher Beams");
            }

            @Override
            public void onFailure(PusherCallbackError error) {
                Log.e("PusherBeams", "Pusher Beams authentication failed: " + error.getMessage());
            }
        });
    }

}
