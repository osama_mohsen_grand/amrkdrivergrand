package amrk.app.amrk_driver.utils.services;

public class LastLocationModel {
    private double lat;
    private double lng;
    private double speed;

    public LastLocationModel() {
    }

    public LastLocationModel(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
