package amrk.app.amrk_driver.utils.locations;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.HashMap;

public class MapConfig {
    ArrayList<Marker> markers = new ArrayList<>();
    Context context;
    GoogleMap mMap;
    public HashMap<Marker, Object> markersId = new HashMap<>();

    public MapConfig(Context context, GoogleMap map) {
        this.context = context;
        this.mMap = map;
    }

    public void setSettings() {
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setTrafficEnabled(true);
        mMap.setBuildingsEnabled(true);
        mMap.setIndoorEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
    }

    public void clear() {
        markersId.clear();
        markers.clear();
        getGoogleMap().clear();
    }

    public GoogleMap getGoogleMap() {
        return mMap;
    }


    public static void getAddress(double lat, double lng, Context context, MapAddressInterface mapAddressInterface) {
        MapAddress mapAddress = new MapAddress(((Activity) context), lat, lng);
        mapAddress.getAddress((address, city) -> {
            if (mapAddressInterface != null)
                mapAddressInterface.fetchFullAddress(address, city);
        });

    }

}
