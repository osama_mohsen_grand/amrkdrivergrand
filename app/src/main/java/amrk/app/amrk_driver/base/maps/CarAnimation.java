package amrk.app.amrk_driver.base.maps;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

import amrk.app.amrk_driver.base.MyApplication;

public class CarAnimation {
    GoogleMap googleMap;
    LatLng lastLocation;
    LatLng shadowTgt;
    double bearing;
    boolean loaded = false;

    public CarAnimation(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    //start new bearing
    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }

    public void animateMarker(final LatLng endPosition, final Marker marker, List<LatLng> latLngs) {
        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(1000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(animation -> {
                try {
                    float v = animation.getAnimatedFraction();
                    float bearing = getBearing(startPosition, endPosition);
                    LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                    marker.setPosition(newPosition);
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (LatLng latLng : latLngs) {
                        builder.include(latLng);
                    }
                    int w = MyApplication.getInstance().getResources().getDisplayMetrics().heightPixels;
                    int padding = (int) (w * .10); // offset from edges of the map in pixels
                    LatLngBounds bounds = builder.build();
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                    googleMap.setOnMapLoadedCallback(() -> {
                        if (latLngs.size() > 1)
                            googleMap.animateCamera(cu);
                        else
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                    .target(newPosition)
                                    .zoom(17.5f)
                                    .bearing(googleMap.getCameraPosition().bearing)
                                    .build()));
                        loaded = true;
                    });
//                    if (loaded)

                    marker.setRotation(bearing);
                } catch (Exception ex) {
                    //I don't care atm..
                    ex.printStackTrace();
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                }
            });
            valueAnimator.start();
        }
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }
//
//    //end new bearing
//    private void changeCameraView(LatLng newPos, LatLng oldPos) {
//        // ignore very small position deviations (prevents wild swinging)
//        double d = SphericalUtil.computeDistanceBetween(oldPos, newPos);
//        if (d < 10) {
//            return;
//        }
//        // compute our own bearing (do not use location bearing)
//        bearing = SphericalUtil.computeHeading(oldPos, newPos);
//        //-----------------------------------------------
//        // Next section really only needs to be done once
//        // Compute distance of pixels on screen using some desirable "offset"
//        Projection p = googleMap.getProjection();
//        Point bottomRightPoint = p.toScreenLocation(p.getVisibleRegion().nearRight);
//        Point center = new Point(bottomRightPoint.x / 2, bottomRightPoint.y / 2);
//        Point offset = new Point(center.x, (center.y + 300));
//
//        LatLng centerLoc = p.fromScreenLocation(center);
//        LatLng offsetNewLoc = p.fromScreenLocation(offset);
//
//        // this computed value only changes on zoom
//        double offsetDistance = SphericalUtil.computeDistanceBetween(centerLoc, offsetNewLoc);
//        //-----------------------------------------------
//
//
//        // Compute shadow target position from current position (see diagram)
//        shadowTgt = SphericalUtil.computeOffset(newPos, offsetDistance, bearing);
//        Log.e("changeCameraView", "changeCameraView: "+shadowTgt );
//        // update circles
//        if (centerCircle != null) {
//            centerCircle.setCenter(shadowTgt);
//        } else {
//            centerCircle = mMap.addCircle(new CircleOptions().strokeColor(Color.BLUE).center(shadowTgt).radius(50));
//        }
//        if (carCircle != null) {
//            carCircle.setCenter(newPos);
//        } else {
//            carCircle = mMap.addCircle(new CircleOptions().strokeColor(Color.GREEN).center(newPos).radius(50));
//        }
//
//
//        // update camera
//
//        CameraPosition.Builder b = CameraPosition.builder();
//        b.zoom(15.0F);
//        b.bearing((float) (bearing));
//        b.target(shadowTgt);
//        CameraUpdate cu = CameraUpdateFactory.newCameraPosition(b.build());
//        mMap.animateCamera(cu);
//
//        // save location as last for next update
//        lastLocation = newPos;
//    }
}
