package amrk.app.amrk_driver.base.maps;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Looper;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.pages.home.viewModels.HomeViewModel;
import amrk.app.amrk_driver.utils.helper.AppHelper;
import amrk.app.amrk_driver.utils.resources.ResourceManager;

public class MapHelper {
    public double driverLat, driverLng;
    public MarkerOptions markerOptionsDriver;
    public Marker driverMarker;
    public HomeViewModel homeViewModels;
    public GoogleMap mMap;
    Context context;
    CarAnimation carAnimation;
    public ArrayList<LatLng> latLngs = new ArrayList<>();

    public MapHelper(HomeViewModel homeViewModels, GoogleMap mMap, Context context) {
        this.homeViewModels = homeViewModels;
        this.mMap = mMap;
        this.context = context;
        carAnimation = new CarAnimation(mMap);

    }

    LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(@NotNull LocationResult locationResult) {
            carAnimation.animateMarker(new LatLng(locationResult.getLastLocation().getLatitude(), locationResult.getLastLocation().getLongitude()), driverMarker, latLngs);
        }
    };

    public void requestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setTrafficEnabled(true);
        mMap.setBuildingsEnabled(false);
        mMap.setIndoorEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        // for first time open app
        latLngs.clear();
        getLastKnownLocation();
    }


    public void addMarker(LatLng position) {
        driverLat = position.latitude;
        driverLng = position.longitude;
        if (latLngs.size() > 0)
            latLngs.set(0, position);
        markerOptionsDriver = new MarkerOptions();
        markerOptionsDriver.draggable(true);
//        if (homeViewModels.getTripDataFromNotifications() != null && homeViewModels.getTripDataFromNotifications().getStatus() != Constants.TRIP_WAITING)
        markerOptionsDriver.icon(BitmapDescriptorFactory.fromBitmap(AppHelper.resizeVectorIcon(ResourceManager.getDrawable(R.drawable.ic_car), ResourceManager.getDrawable(R.drawable.ic_car).getMinimumWidth(), ResourceManager.getDrawable(R.drawable.ic_car).getMinimumHeight())));
//        else
//            markerOptionsDriver.icon(BitmapDescriptorFactory.fromBitmap(AppHelper.resizeVectorIcon(ResourceManager.getDrawable(R.drawable.ic_current_location), ResourceManager.getDrawable(R.drawable.ic_current_location).getMinimumHeight(), ResourceManager.getDrawable(R.drawable.ic_current_location).getMinimumHeight())));
        markerOptionsDriver.position(position);
        markerOptionsDriver.anchor(0.5f, 0.5f);
        if (driverMarker == null) {
            driverMarker = mMap.addMarker(markerOptionsDriver);
        }
        carAnimation.animateMarker(position, driverMarker, latLngs);
    }

    private void animateCamera(LatLng position) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(position).zoom(20).bearing(mMap.getCameraPosition().bearing).tilt(40).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//        driverLat = position.latitude;
//        driverLng = position.longitude;
//        addMarker(position);

    }

    public void getLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationServices.getFusedLocationProviderClient(context)
                .requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(MyApplication.getInstance());
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener((Activity) context, location -> {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        latLngs.add(new LatLng(location.getLatitude(), location.getLongitude()));
                        driverLat = location.getLatitude();
                        driverLng = location.getLongitude();
                        addMarker(new LatLng(location.getLatitude(), location.getLongitude()));
                        homeViewModels.updateLocation(location.getLatitude(), location.getLongitude());
                    }
                });

    }

}
