package amrk.app.amrk_driver.base;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.Observable;
import androidx.databinding.PropertyChangeRegistry;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import amrk.app.amrk_driver.BR;
import amrk.app.amrk_driver.PassingObject;
import amrk.app.amrk_driver.pages.auth.models.UserData;
import amrk.app.amrk_driver.pages.auth.models.home.HomeDriverData;
import amrk.app.amrk_driver.utils.resources.ResourceManager;
import amrk.app.amrk_driver.utils.session.LanguagesHelper;
import amrk.app.amrk_driver.utils.session.UserHelper;


public class BaseViewModel extends ViewModel implements Observable {
    private MutableLiveData<Object> mutableLiveData = new MutableLiveData<>();
    String message;
    PropertyChangeRegistry mCallBacks;
    private PassingObject passingObject = new PassingObject();
    String countryCurrency;
    public UserData userData = UserHelper.getInstance(MyApplication.getInstance()).getUserData();
    private HomeDriverData homeDriverData = UserHelper.getInstance(MyApplication.getInstance()).getUserHomeData();
    public String lang = LanguagesHelper.getCurrentLanguage();
    public String today;
    int searchProgressVisible = View.GONE;

    public BaseViewModel() {
        mCallBacks = new PropertyChangeRegistry();
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE", new Locale(LanguagesHelper.getCurrentLanguage()));
        Date d = new Date();
        today = sdf.format(d);

    }

    public MutableLiveData<Object> getLiveData() {
        return mutableLiveData == null ? mutableLiveData = new MutableLiveData<>() : mutableLiveData;
    }

    @Bindable
    public String getMessage() {
        return message;
    }

    @Bindable
    public void setMessage(String message) {
        notifyChange(BR.message);
        this.message = message;
    }

    public void goBack(Context context) {
        ((Activity) context).finish();
    }

    public String getString(int stringRes) {
        return ResourceManager.getString(stringRes);
    }

    public Drawable getDrawable(int drawable) {
        return ResourceManager.getDrawable(drawable);
    }

    @Bindable
    public int getSearchProgressVisible() {
        return searchProgressVisible;
    }

    @Bindable
    public void setSearchProgressVisible(int searchProgressVisible) {
        notifyChange(BR.searchProgressVisible);
        this.searchProgressVisible = searchProgressVisible;
    }

    public String[] getStringArray(int resArray) {
        return MyApplication.getInstance().getResources().getStringArray(resArray);
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        mCallBacks.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        mCallBacks.remove(callback);
    }

    @Bindable
    public PassingObject getPassingObject() {
        return passingObject;
    }

    @Bindable
    public void setPassingObject(PassingObject passingObject) {
        notifyChange(BR.passingObject);
        this.passingObject = passingObject;
    }

    @Bindable
    public HomeDriverData getHomeDriverData() {
        return homeDriverData;
    }

    @Bindable
    public void setHomeDriverData(HomeDriverData homeDriverData) {
        this.homeDriverData = homeDriverData;
    }

    public String getCountryCurrency() {
        return UserHelper.getInstance(MyApplication.getInstance()).getCountryCurrency();
    }

    public void notifyChange() {
        mCallBacks.notifyChange(this, 0);
    }

    public void notifyChange(int propertyId) {
        mCallBacks.notifyChange(this, propertyId);
    }

}
