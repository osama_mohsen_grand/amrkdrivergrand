package amrk.app.amrk_driver.base;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;
import com.pusher.pushnotifications.fcm.MessagingService;

import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Objects;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.activity.BaseActivity;
import amrk.app.amrk_driver.activity.MainActivity;
import amrk.app.amrk_driver.utils.session.UserHelper;


public class MyFirebaseMessagingService extends MessagingService {

    @Override
    public void onNewToken(@NotNull String s) {
        super.onNewToken(s);
        Log.e("onNewToken", "onNewToken: " + s);
    }

    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
        Log.e("onMessageReceived", "onMessageReceived: " + remoteMessage.getData());
//        if (remoteMessage.getData().get("trip_details") != null) {
//            showNotification(remoteMessage.getData());
//            Intent intent = new Intent();
//            intent.putExtra("trip_details", remoteMessage.getData().get("trip_details"));
//            intent.setAction("app.te.receiver");
//            sendBroadcast(intent);
//        }
//        if (remoteMessage.getData().get("uber_chat") != null) {
//            showNotification(remoteMessage.getData());
//            Intent intent = new Intent();
//            intent.putExtra("uber_chat", remoteMessage.getData().get("uber_chat"));
//            intent.setAction("app.te.receiver");
//            sendBroadcast(intent);
//        } else {
        showNotification(remoteMessage.getData());
//        }
    }

    private void showNotification(Map<String, String> notification) {
        Intent intent;
        if (Objects.requireNonNull(notification.get("type")).equals("1")) {
            if (!TextUtils.isEmpty(notification.get("attribute_id")))
                UserHelper.getInstance(MyApplication.getInstance()).addTripId(Integer.parseInt(notification.get("attribute_id")));
            intent = new Intent(this, MainActivity.class);
        } else
            intent = new Intent(this, BaseActivity.class);
        intent.putExtra("is_notification", true);
        intent.putExtra("type", notification.get("type"));
        intent.putExtra("attribute_id", notification.get("attribute_id"));
        // Set the Activity to start in a new, empty task
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        String channelId = "channelId";
        try {
            Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + MyApplication.getInstance().getPackageName() + "/" + R.raw.notify_default);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), defaultSoundUri);
            r.play();
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this, channelId)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(notification.get("title"))
                            .setContentText(notification.get("body"))
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(channelId,
                        "Channel human readable title",
                        NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);
            }
            notificationManager.notify(0, notificationBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}