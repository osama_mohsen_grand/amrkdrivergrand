package amrk.app.amrk_driver.base;

import javax.inject.Singleton;

import amrk.app.amrk_driver.utils.locations.MapAddressActivity;
import dagger.Component;
import amrk.app.amrk_driver.activity.BaseActivity;
import amrk.app.amrk_driver.activity.MainActivity;
import amrk.app.amrk_driver.connection.ConnectionModule;
import amrk.app.amrk_driver.pages.auth.changePassword.ChangePasswordFragment;
import amrk.app.amrk_driver.pages.auth.confirmCode.ConfirmCodeFragment;
import amrk.app.amrk_driver.pages.auth.forgetPassword.ForgetPasswordFragment;
import amrk.app.amrk_driver.pages.auth.login.LoginFragment;
import amrk.app.amrk_driver.pages.auth.register.RegisterFragment;
import amrk.app.amrk_driver.pages.balance.AddCreditFragment;
import amrk.app.amrk_driver.pages.balance.MyBankingTransferFragment;
import amrk.app.amrk_driver.pages.balance.PayBankingTransferFragment;
import amrk.app.amrk_driver.pages.balance.PayReceivablesFragment;
import amrk.app.amrk_driver.pages.balance.PaymentFragment;
import amrk.app.amrk_driver.pages.chat.view.ChatFragment;
import amrk.app.amrk_driver.pages.collectMoney.MainCollectFragment;
import amrk.app.amrk_driver.pages.collectMoney.PickUpCollectFragment;
import amrk.app.amrk_driver.pages.collectMoney.PutMoneyWalletFragment;
import amrk.app.amrk_driver.pages.countries.UserDetectLocation;
import amrk.app.amrk_driver.pages.history.HistoryFragment;
import amrk.app.amrk_driver.pages.home.HomeFragment;
import amrk.app.amrk_driver.pages.myFatora.MyFatooraMethodFragment;
import amrk.app.amrk_driver.pages.notifications.NotificationsFragment;
import amrk.app.amrk_driver.pages.onBoard.OnBoardFragment;
import amrk.app.amrk_driver.pages.profile.ProfileFragment;
import amrk.app.amrk_driver.pages.settings.AboutAppFragment;
import amrk.app.amrk_driver.pages.settings.CarCategoriesFragment;
import amrk.app.amrk_driver.pages.settings.ContactUsFragment;
import amrk.app.amrk_driver.pages.settings.DocumentsFragment;
import amrk.app.amrk_driver.pages.settings.LangFragment;
import amrk.app.amrk_driver.pages.settings.TermsFragment;
import amrk.app.amrk_driver.pages.splash.SplashFragment;
import amrk.app.amrk_driver.pages.subscriptions.RenewSubscriptionsFragment;
import amrk.app.amrk_driver.pages.subscriptions.SubscriptionsDateFragment;

//Component refer to an interface or waiter for make an coffee cup to me
@Singleton
@Component(modules = {ConnectionModule.class, LiveData.class})
public interface IApplicationComponent {
    void inject(MainActivity mainActivity);

    void inject(BaseActivity tmpActivity);

    void inject(MapAddressActivity mapAddressActivity);

    void inject(SplashFragment splashFragment);

    void inject(OnBoardFragment onBoardFragment);

    void inject(UserDetectLocation countriesFragment);

    void inject(LoginFragment loginFragment);

    void inject(ForgetPasswordFragment forgetPasswordFragment);

    void inject(ConfirmCodeFragment confirmCodeFragment);

    void inject(ChangePasswordFragment changePasswordFragment);

    void inject(RegisterFragment registerFragment);

    void inject(HomeFragment homeFragment);

    void inject(NotificationsFragment notificationsFragment);

    void inject(ProfileFragment profileFragment);

    void inject(AboutAppFragment aboutAppFragment);

    void inject(ContactUsFragment contactUsFragment);

    void inject(TermsFragment termsFragment);

    void inject(DocumentsFragment documentsFragment);

    void inject(ChatFragment chatFragment);

    void inject(CarCategoriesFragment carCategoriesFragment);

    void inject(MainCollectFragment mainCollectFragment);

    void inject(PutMoneyWalletFragment putMoneyWalletFragment);

    void inject(PickUpCollectFragment pickUpCollectFragment);

    void inject(AddCreditFragment addCreditFragment);

    void inject(MyBankingTransferFragment myBankingTransferFragment);

    void inject(PayBankingTransferFragment payBankingTransferFragment);

    void inject(PaymentFragment paymentFragment);

    void inject(PayReceivablesFragment payReceivablesFragment);

    void inject(HistoryFragment historyFragment);

    void inject(LangFragment langFragment);

    void inject(SubscriptionsDateFragment subscriptionsDateFragment);

    void inject(RenewSubscriptionsFragment renewSubscriptionsFragment);

    void inject(MyFatooraMethodFragment myFatooraMethodFragment);

    @Component.Builder
    interface Builder {
        IApplicationComponent build();
    }
}
