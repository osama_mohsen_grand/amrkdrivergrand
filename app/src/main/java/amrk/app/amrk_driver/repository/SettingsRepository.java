package amrk.app.amrk_driver.repository;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.connection.ConnectionHelper;
import amrk.app.amrk_driver.connection.FileObject;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.model.base.StatusMessage;
import amrk.app.amrk_driver.pages.auth.models.RegisterRequest;
import amrk.app.amrk_driver.pages.auth.models.UsersResponse;
import amrk.app.amrk_driver.pages.balance.models.AddCreditRequest;
import amrk.app.amrk_driver.pages.balance.models.CreditsResponse;
import amrk.app.amrk_driver.pages.balance.models.bankingTransfer.BankingTransferResponse;
import amrk.app.amrk_driver.pages.balance.models.bankingTransfer.MyBankingResponse;
import amrk.app.amrk_driver.pages.balance.models.bankingTransfer.UploadBankTransferRequest;
import amrk.app.amrk_driver.pages.notifications.models.NotificationsResponse;
import amrk.app.amrk_driver.pages.settings.models.AboutResponse;
import amrk.app.amrk_driver.pages.settings.models.ContactRequest;
import amrk.app.amrk_driver.pages.settings.models.carCategories.CarCategoriesResponse;
import amrk.app.amrk_driver.pages.settings.models.carCategories.UpdateCarsRequest;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.URLS;
import amrk.app.amrk_driver.utils.session.UserHelper;
import io.reactivex.disposables.Disposable;

@Singleton
public class SettingsRepository extends BaseRepository {

    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    @Inject
    public SettingsRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getAbout() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.ABOUT, new Object(), AboutResponse.class,
                Constants.ABOUT, true);
    }

    public Disposable getTerms() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.TERMS, new Object(), AboutResponse.class,
                Constants.TERMS, true);
    }

    public Disposable privacyPolicy() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.PRIVACY, new Object(), AboutResponse.class,
                Constants.TERMS, true);
    }

    public Disposable getNotifications() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.NOTIFICATIONS, new Object(), NotificationsResponse.class,
                Constants.NOTIFICATIONS, true);
    }

    public Disposable getDriverBalance() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CREDITS, new Object(), CreditsResponse.class,
                Constants.CREDITS, true);
    }

    public Disposable addCredit(AddCreditRequest addCreditRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.UPDATE_PROFILE, addCreditRequest, UsersResponse.class,
                Constants.UPDATE_PROFILE, false);
    }

    public Disposable uploadTransferDocument(int bankId, ArrayList<FileObject> fileObjects) {
        return connectionHelper.requestApi(URLS.ADD_BANK_ACCOUNT, new UploadBankTransferRequest(bankId), fileObjects, StatusMessage.class,
                Constants.ADD_BANK_TRANSFER, false);
    }

    public Disposable getBankAccounts() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.BANK_ACCOUNTS, new Object(), BankingTransferResponse.class,
                Constants.BANKING_ACCOUNTS, true);
    }

    public Disposable getBankingTransfers() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.BANKING_TRANSFER, new Object(), MyBankingResponse.class,
                Constants.BANKING_TRANSFER, true);
    }

    public Disposable getCarLevels() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CAR_LEVELS, new Object(), CarCategoriesResponse.class,
                Constants.CAR_LEVELS, true);
    }

    public Disposable updateCars(List<Integer> selectedCars) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.UPDATE_CARS_LEVELS, new UpdateCarsRequest(selectedCars), CarCategoriesResponse.class,
                Constants.UPDATE_CARS_LEVELS, true);
    }

    public Disposable sendContact(ContactRequest contactRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CONTACT, contactRequest, StatusMessage.class,
                Constants.CONTACT, false);
    }
    public Disposable updateLang(String lang) {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setLang(lang);
        registerRequest.setCountry_id(String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getUserData().getCountryId()));
        return connectionHelper.requestApiBackground(Constants.POST_REQUEST, URLS.UPDATE_PROFILE, registerRequest);
    }
}