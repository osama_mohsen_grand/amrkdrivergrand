package amrk.app.amrk_driver.repository;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;
import javax.inject.Singleton;

import amrk.app.amrk_driver.connection.ConnectionHelper;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.history.models.HistoryResponse;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.URLS;
import io.reactivex.disposables.Disposable;

@Singleton
public class HistoryRepository extends BaseRepository {

    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    @Inject
    public HistoryRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getHistory(int type, int reportType, int page, boolean showProgress) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.TRIP_HISTORY + type + "/" + reportType + "?page=" + page, new Object(), HistoryResponse.class,
                Constants.TRIP_HISTORY, showProgress);
    }

}