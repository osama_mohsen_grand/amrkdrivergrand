package amrk.app.amrk_driver.repository;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;
import javax.inject.Singleton;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.maps.models.DistanceTimeResponse;
import amrk.app.amrk_driver.connection.ConnectionHelper;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.model.base.StatusMessage;
import amrk.app.amrk_driver.pages.auth.models.RegisterRequest;
import amrk.app.amrk_driver.pages.auth.models.UsersResponse;
import amrk.app.amrk_driver.pages.chat.model.RateRequest;
import amrk.app.amrk_driver.pages.collectMoney.models.CollectMoneyRequest;
import amrk.app.amrk_driver.pages.home.models.ChangeStatusResponse;
import amrk.app.amrk_driver.pages.home.models.StatusRequest;
import amrk.app.amrk_driver.pages.home.models.cancelTripReasons.CancelReasonsResponse;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.URLS;
import amrk.app.amrk_driver.utils.resources.ResourceManager;
import amrk.app.amrk_driver.utils.services.LastLocationModel;
import io.reactivex.disposables.Disposable;

@Singleton
public class HomeRepository extends BaseRepository {

    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    @Inject
    public HomeRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable updateLocation(LastLocationModel lastLocationModel) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.UPDATE_LOCATION, lastLocationModel, StatusMessage.class,
                Constants.UPDATE_LOCATION, false);
    }

    public Disposable changeStatus(StatusRequest statusRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CHANGE_STATUS, statusRequest, ChangeStatusResponse.class,
                Constants.CHANGE_STATUS, false);
    }
 public Disposable lastTrip(StatusRequest statusRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CHANGE_STATUS, statusRequest, ChangeStatusResponse.class,
                Constants.CHANGE_STATUS, true);
    }

    public Disposable cancelTrip(StatusRequest statusRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CHANGE_STATUS, statusRequest, ChangeStatusResponse.class,
                Constants.CANCEL_TRIP, true);
    }

    public Disposable getLessTime(String destination, String start) {
        String requestUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + start + "&destinations=" + destination + "&key=" + ResourceManager.getString(R.string.google_map);
        return connectionHelper.requestGoogleApi(Constants.GET_REQUEST, requestUrl, new Object(), DistanceTimeResponse.class,
                Constants.GOOGLE_API, false);
    }

    public Disposable cancelReasons() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_CANCEL_REASONS, new Object(), CancelReasonsResponse.class,
                Constants.CANCEL_REASONS, false);
    }

    public Disposable collectMoney(CollectMoneyRequest collectMoneyRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.COLLECT_MONEY, collectMoneyRequest, StatusMessage.class,
                Constants.COLLECT_MONEY, true);
    }

    public Disposable rateTrip(RateRequest rateRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.RATE_TRIP, rateRequest, StatusMessage.class,
                Constants.RATE_TRIP, true);
    }

    public Disposable changeDriverOnlineStatus(int online) {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setOnline(String.valueOf(online));
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.UPDATE_PROFILE, registerRequest, UsersResponse.class,
                Constants.UPDATE_PROFILE, false);
    }

    public Disposable putReset(double tripTotal, int id) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.PUT_RESET + tripTotal + "&trip_id=" + id, new Object(), StatusMessage.class,
                Constants.PUT_RESET_MONEY, false);
    }

    public Disposable updateCaptainBusy() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.UPDATE_BUSY, new Object(), StatusMessage.class,
                Constants.UPDATE_BUSY, false);
    }
}