package amrk.app.amrk_driver.repository;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_driver.connection.ConnectionHelper;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.onBoard.models.BoardResponse;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.URLS;
import io.reactivex.disposables.Disposable;


public class BaseRepository {
    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getBoard() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.BOARD , new Object(), BoardResponse.class,
                Constants.BOARD, true);
    }
}
