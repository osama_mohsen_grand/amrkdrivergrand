package amrk.app.amrk_driver.repository;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import amrk.app.amrk_driver.connection.ConnectionHelper;
import amrk.app.amrk_driver.connection.FileObject;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.model.base.StatusMessage;
import amrk.app.amrk_driver.pages.auth.models.ConfirmCodeRequest;
import amrk.app.amrk_driver.pages.auth.models.ForgetPasswordRequest;
import amrk.app.amrk_driver.pages.auth.models.LoginRequest;
import amrk.app.amrk_driver.pages.auth.models.RegisterRequest;
import amrk.app.amrk_driver.pages.auth.models.UsersResponse;
import amrk.app.amrk_driver.pages.auth.models.carNational.CarsNationalResponse;
import amrk.app.amrk_driver.pages.auth.models.home.HomeDriverResponse;
import amrk.app.amrk_driver.pages.countries.models.CountriesCodesResponse;
import amrk.app.amrk_driver.pages.countries.models.CountriesResponse;
import amrk.app.amrk_driver.pages.countries.models.UpdateCountryRequest;
import amrk.app.amrk_driver.pages.myFatora.models.SendPaymentRequest;
import amrk.app.amrk_driver.pages.settings.models.UserDocumentsResponse;
import amrk.app.amrk_driver.pages.subscriptions.models.SubscriptionsResponse;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.URLS;
import io.reactivex.disposables.Disposable;

@Singleton
public class AuthRepository extends BaseRepository {

    @Inject
    public ConnectionHelper connectionHelper;

    protected MutableLiveData<Mutable> liveData;

    @Inject
    public AuthRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getCountriesCodes() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_COUNTRIES_CODE, new Object(), CountriesCodesResponse.class,
                Constants.GET_COUNTRIES_CODE, false);
    }

    public Disposable getHome() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.HOME, new Object(), HomeDriverResponse.class,
                Constants.MENU_HOME, false);
    }


    public Disposable getCountries() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.COUNTRIES, new Object(), CountriesResponse.class,
                Constants.COUNTRIES, true);
    }

    public Disposable getCities(int countryId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CITIES + countryId, new Object(), CountriesResponse.class,
                Constants.CITIES, true);
    }

    public Disposable updateCountry(int countryId) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.UPDATE_PROFILE, new UpdateCountryRequest(countryId), UsersResponse.class,
                Constants.UPDATE_PROFILE, true);
    }

    public Disposable loginPhone(LoginRequest request) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.LOGIN_PHONE, request, StatusMessage.class,
                Constants.PHONE_VERIFIED, true);
    }

    public Disposable loginPassword(LoginRequest request) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.LOGIN_PASSWORD, request, UsersResponse.class,
                Constants.LOGIN, false);
    }


    public Disposable register(RegisterRequest request, List<FileObject> fileObjects) {
        return connectionHelper.requestApi(URLS.REGISTER, request, fileObjects, UsersResponse.class,
                Constants.REGISTER, false);
    }

    public Disposable updateProfile(RegisterRequest request, ArrayList<FileObject> fileObjects) {
        if (fileObjects == null) {
            return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.UPDATE_PROFILE, request, UsersResponse.class,
                    Constants.UPDATE_PROFILE, false);
        } else {
            return connectionHelper.requestApi(URLS.UPDATE_PROFILE, request, fileObjects, UsersResponse.class,
                    Constants.UPDATE_PROFILE, false);
        }

    }

    public Disposable forgetPassword(ForgetPasswordRequest request) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.FORGET_PASSWORD, request, StatusMessage.class,
                Constants.FORGET_PASSWORD, false);
    }

    public Disposable confirmCode(ConfirmCodeRequest request) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CONFIRM_CODE, request, UsersResponse.class,
                Constants.CONFIRM_CODE, false);
    }

    public Disposable getCarsNationals() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CARS_NATIONALS, new Object(), CarsNationalResponse.class,
                Constants.CARS_NATIONALS, true);
    }

    public Disposable getUserDocuments() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_USER_DOCUMENTS, new Object(), UserDocumentsResponse.class,
                Constants.GET_USER_DOCUMENTS, true);
    }

    public Disposable getDriverSubscriptions() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_DRIVER_SUBSCRIPTIONS, new Object(), SubscriptionsResponse.class,
                Constants.RENEW_SUBSCRIPTIONS, true);
    }

    public Disposable checkPayment(SendPaymentRequest sendPaymentRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CHECK_PAYMENT, sendPaymentRequest, UsersResponse.class,
                Constants.CHECK_PAYMENT, true);
    }

}