package amrk.app.amrk_driver.repository;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;
import javax.inject.Singleton;

import amrk.app.amrk_driver.connection.ConnectionHelper;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.chat.model.ChatRequest;
import amrk.app.amrk_driver.pages.chat.model.ChatResponse;
import amrk.app.amrk_driver.pages.chat.model.ChatSendResponse;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.URLS;
import io.reactivex.disposables.Disposable;

@Singleton
public class ChatRepository extends BaseRepository {

    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    @Inject
    public ChatRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getChat(int orderId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CHAT + orderId, new Object(), ChatResponse.class,
                Constants.CHAT, true);
    }

    public Disposable sendChat(ChatRequest request) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.SEND_MESSAGE, request, ChatSendResponse.class,
                Constants.SEND_MESSAGE, true);
    }
}