package amrk.app.amrk_driver.connection;

public interface ConnectionListenerInterFace {
    void onRequestResponse(Object response);

}
