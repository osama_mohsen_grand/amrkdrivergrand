package amrk.app.amrk_driver.pages.chat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_driver.model.base.StatusMessage;


public class ChatResponse extends StatusMessage {

    @SerializedName("data")
    private List<Chat> data;

    public void setData(List<Chat> data) {
        this.data = data;
    }

    public List<Chat> getData() {
        return data;
    }
}
