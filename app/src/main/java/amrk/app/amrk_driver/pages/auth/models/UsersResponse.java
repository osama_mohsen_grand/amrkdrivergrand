package amrk.app.amrk_driver.pages.auth.models;

import com.google.gson.annotations.SerializedName;
import amrk.app.amrk_driver.model.base.StatusMessage;

public class UsersResponse extends StatusMessage {
    @SerializedName("data")
    private UserData data;

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }
}