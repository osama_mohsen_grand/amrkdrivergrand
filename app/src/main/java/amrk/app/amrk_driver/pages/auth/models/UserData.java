package amrk.app.amrk_driver.pages.auth.models;

import com.google.gson.annotations.SerializedName;

public class UserData {

    @SerializedName("wallet_flag")
    private String walletFlag;

    @SerializedName("national_id")
    private String nationalId;

    @SerializedName("jwt")
    private String jwt;

    @SerializedName("l_name")
    private String lName;
    @SerializedName("currency")
    private String currency;
    @SerializedName("files_completed")
    private int files_completed;
    @SerializedName("collectedMoneyToday")
    private double collectedMoneyToday;

    @SerializedName("front_car_image")
    private String frontCarImage;

    @SerializedName("promo_code")
    private String promoCode;

    @SerializedName("national_id_type")
    private String nationalIdType;

    @SerializedName("points")
    private int points;

    @SerializedName("no_of_trips")
    private String noOfTrips;

    @SerializedName("user_code")
    private String userCode;

    @SerializedName("car_level")
    private int carLevel;

    @SerializedName("license_image")
    private String licenseImage;

    @SerializedName("rate")
    private String rate;

    @SerializedName("busy")
    private String busy;

    @SerializedName("bank_name")
    private String bankName;

    @SerializedName("car_num")
    private String carNum;

    @SerializedName("id")
    private int id;

    @SerializedName("insurance_image")
    private String insuranceImage;

    @SerializedName("email")
    private String email;

    @SerializedName("lat")
    private String lat;

    @SerializedName("suspend")
    private String suspend;

    @SerializedName("civil_image")
    private String civilImage;
    @SerializedName("image")
    private String image;

    @SerializedName("wallet")
    private String wallet;

    @SerializedName("lng")
    private String lng;

    @SerializedName("back_car_image")
    private String backCarImage;

    @SerializedName("verified")
    private String verified;

    @SerializedName("active")
    private int active;

    @SerializedName("car_color")
    private String carColor;

    @SerializedName("bank_account_num")
    private String bankAccountNum;

    @SerializedName("accept")
    private int accept;

    @SerializedName("token")
    private String token;

    @SerializedName("bank_account_name")
    private String bankAccountName;

    @SerializedName("car_text")
    private String carText;

    @SerializedName("phone")
    private String phone;

    @SerializedName("f_name")
    private String name;

    @SerializedName("name")
    private String userName;

    @SerializedName("online")
    private int online;

    @SerializedName("color_name")
    private String colorName;

    @SerializedName("country_id")
    private int countryId;

    @SerializedName("city_id")
    private int cityId;
    @SerializedName("subscription_type")
    private int subscription_type;

    public String getWalletFlag() {
        return walletFlag;
    }

    public String getNationalId() {
        return nationalId;
    }

    public String getJwt() {
        return jwt;
    }

    public String getlName() {
        return lName;
    }

    public String getFrontCarImage() {
        return frontCarImage;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public String getNationalIdType() {
        return nationalIdType;
    }

    public int getPoints() {
        return points;
    }

    public String getNoOfTrips() {
        return noOfTrips;
    }

    public String getUserCode() {
        return userCode;
    }

    public int getCarLevel() {
        return carLevel;
    }

    public String getLicenseImage() {
        return licenseImage;
    }

    public String getRate() {
        return rate;
    }

    public String getBusy() {
        return busy;
    }

    public String getBankName() {
        return bankName;
    }

    public String getCarNum() {
        return carNum;
    }

    public int getId() {
        return id;
    }

    public String getInsuranceImage() {
        return insuranceImage;
    }

    public String getEmail() {
        return email;
    }

    public String getLat() {
        return lat;
    }

    public String getSuspend() {
        return suspend;
    }

    public String getCivilImage() {
        return civilImage;
    }

    public String getImage() {
        return image;
    }

    public String getWallet() {
        return wallet;
    }

    public String getLng() {
        return lng;
    }

    public String getBackCarImage() {
        return backCarImage;
    }

    public String getVerified() {
        return verified;
    }

    public int getActive() {
        return active;
    }

    public String getCarColor() {
        return carColor;
    }

    public String getBankAccountNum() {
        return bankAccountNum;
    }

    public int getAccept() {
        return accept;
    }

    public String getToken() {
        return token;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public String getCarText() {
        return carText;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public int getOnline() {
        return online;
    }

    public void setOnline(int online) {
        this.online = online;
    }

    public String getColorName() {
        return colorName;
    }

    public int getCountryId() {
        return countryId;
    }

    public int getCityId() {
        return cityId;
    }

    public String getUserName() {
        return userName;
    }

    public String getCurrency() {
        return currency;
    }

    public int getFiles_completed() {
        return files_completed;
    }

    public double getCollectedMoneyToday() {
        return collectedMoneyToday;
    }

    public int getSubscription_type() {
        return subscription_type;
    }

    @Override
    public String toString() {
        return "UserData{" +
                "walletFlag='" + walletFlag + '\'' +
                ", nationalId='" + nationalId + '\'' +
                ", jwt='" + jwt + '\'' +
                ", lName='" + lName + '\'' +
                ", currency='" + currency + '\'' +
                ", frontCarImage='" + frontCarImage + '\'' +
                ", promoCode='" + promoCode + '\'' +
                ", nationalIdType='" + nationalIdType + '\'' +
                ", points=" + points +
                ", noOfTrips='" + noOfTrips + '\'' +
                ", userCode='" + userCode + '\'' +
                ", carLevel=" + carLevel +
                ", licenseImage='" + licenseImage + '\'' +
                ", rate='" + rate + '\'' +
                ", busy='" + busy + '\'' +
                ", bankName='" + bankName + '\'' +
                ", carNum='" + carNum + '\'' +
                ", id=" + id +
                ", insuranceImage='" + insuranceImage + '\'' +
                ", email='" + email + '\'' +
                ", lat='" + lat + '\'' +
                ", suspend='" + suspend + '\'' +
                ", civilImage='" + civilImage + '\'' +
                ", image='" + image + '\'' +
                ", wallet='" + wallet + '\'' +
                ", lng='" + lng + '\'' +
                ", backCarImage='" + backCarImage + '\'' +
                ", verified='" + verified + '\'' +
                ", active=" + active +
                ", carColor='" + carColor + '\'' +
                ", bankAccountNum='" + bankAccountNum + '\'' +
                ", accept=" + accept +
                ", token='" + token + '\'' +
                ", bankAccountName='" + bankAccountName + '\'' +
                ", carText='" + carText + '\'' +
                ", phone='" + phone + '\'' +
                ", name='" + name + '\'' +
                ", userName='" + userName + '\'' +
                ", online=" + online +
                ", colorName='" + colorName + '\'' +
                ", countryId=" + countryId +
                ", cityId=" + cityId +
                '}';
    }
}