package amrk.app.amrk_driver.pages.balance;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import javax.inject.Inject;
import amrk.app.amrk_driver.BR;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.base.ParentActivity;
import amrk.app.amrk_driver.connection.FileObject;
import amrk.app.amrk_driver.databinding.FragmentPayBankTransferBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.model.base.StatusMessage;
import amrk.app.amrk_driver.pages.balance.models.bankingTransfer.BankingTransferResponse;
import amrk.app.amrk_driver.pages.balance.viewModels.BalanceViewModel;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.upload.FileOperations;

public class PayBankingTransferFragment extends BaseFragment {
    FragmentPayBankTransferBinding payBankTransferBinding;
    @Inject
    BalanceViewModel viewModel;


    public PayBankingTransferFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        payBankTransferBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_pay_bank_transfer, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        payBankTransferBinding.setMyBankingViewModel(viewModel);
        viewModel.bankAccounts();
        liveDataListeners();
        return payBankTransferBinding.getRoot();
    }


    private void liveDataListeners() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");

            if (Constants.BANKING_ACCOUNTS.equals(((Mutable) o).message)) {
                viewModel.getPayBankingAdapter().updateData(((BankingTransferResponse) ((Mutable) o).object).getData());
                viewModel.notifyChange(BR.payBankingAdapter);
            } else if (Constants.WARNING.equals(((Mutable) o).message)) {
                ((ParentActivity) context).toastError(getString(R.string.bank_account_warning));
            } else if (Constants.IMAGE.equals(((Mutable) o).message)) {
                pickImageDialogSelect(Constants.FILE_TYPE_IMAGE);
            } else if (Constants.ADD_BANK_TRANSFER.equals(((Mutable) o).message)) {
                toastMessage(((StatusMessage) mutable.object).mMessage);
                finishActivity();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            viewModel.getFileObjectList().clear();
            FileObject fileObject = FileOperations.getFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            viewModel.getFileObjectList().add(fileObject);
            payBankTransferBinding.text6.setText(getString(R.string.payment_file_selected));
        }
    }
}
