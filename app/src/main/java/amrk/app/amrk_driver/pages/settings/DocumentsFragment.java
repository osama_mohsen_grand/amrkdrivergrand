package amrk.app.amrk_driver.pages.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.connection.FileObject;
import amrk.app.amrk_driver.databinding.FragmentDocumentBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.auth.models.UsersResponse;
import amrk.app.amrk_driver.pages.settings.models.UserDocumentsResponse;
import amrk.app.amrk_driver.pages.settings.viewModels.DocumentsViewModel;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.upload.FileOperations;

public class DocumentsFragment extends BaseFragment {

    private Context context;
    FragmentDocumentBinding binding;
    @Inject
    DocumentsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_document, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        viewModel.userDocuments();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (((Mutable) o).message) {
                case Constants.GET_USER_DOCUMENTS:
                    viewModel.setUserDocuments(((UserDocumentsResponse) (mutable).object).getData());
                    break;
                case Constants.front_car_image:
                    pickImageDialogSelect(Constants.front_car_image_code);
                    break;
                case Constants.back_car_image:
                    pickImageDialogSelect(Constants.back_car_code);
                    break;
                case Constants.insurance_image:
                    pickImageDialogSelect(Constants.insurance_image_code);
                    break;
                case Constants.license_image:
                    pickImageDialogSelect(Constants.license_image_code);
                    break;
                case Constants.civil_image:
                    pickImageDialogSelect(Constants.civil_image_code);
                    break;
                case Constants.UPDATE_PROFILE:
                    toastMessage(((UsersResponse) (mutable).object).mMessage);
                    finishActivity();
                    break;
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        FileObject fileObject = null;
        if (requestCode == Constants.front_car_image_code) {
            fileObject = FileOperations.getFileObject(getActivity(), data, Constants.front_car_image, Constants.FILE_TYPE_IMAGE);
            binding.tvDoc.setText(getResources().getString(R.string.front_car_add));
        } else if (requestCode == Constants.back_car_code) {
            fileObject = FileOperations.getFileObject(getActivity(), data, Constants.back_car_image, Constants.FILE_TYPE_IMAGE);
            binding.tvDocCarBackImage.setText(getResources().getString(R.string.back_car_add));
        } else if (requestCode == Constants.insurance_image_code) {
            fileObject = FileOperations.getFileObject(getActivity(), data, Constants.insurance_image, Constants.FILE_TYPE_IMAGE);
            binding.tvDocInsuranceImage.setText(getResources().getString(R.string.insurance_car_add));
        } else if (requestCode == Constants.license_image_code) {
            fileObject = FileOperations.getFileObject(getActivity(), data, Constants.license_image, Constants.FILE_TYPE_IMAGE);
            binding.tvDocLicenseImage.setText(getResources().getString(R.string.license_car_add));
        } else if (requestCode == Constants.civil_image_code) {
            fileObject = FileOperations.getFileObject(getActivity(), data, Constants.civil_image, Constants.FILE_TYPE_IMAGE);
            binding.tvDocLicenseCivilImage.setText(getResources().getString(R.string.civil_car_add));
        }
        viewModel.getFileObjects().add(fileObject);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }


}
