package amrk.app.amrk_driver.pages.splash;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.pushnotifications.BeamsCallback;
import com.pusher.pushnotifications.PushNotifications;
import com.pusher.pushnotifications.PusherCallbackError;
import com.pusher.pushnotifications.auth.AuthData;
import com.pusher.pushnotifications.auth.BeamsTokenProvider;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import javax.inject.Inject;

import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.databinding.FragmentSplashBinding;
import amrk.app.amrk_driver.pages.auth.login.LoginFragment;
import amrk.app.amrk_driver.pages.auth.models.home.HomeDriverResponse;
import amrk.app.amrk_driver.pages.onBoard.OnBoardFragment;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.helper.MovementHelper;
import amrk.app.amrk_driver.utils.session.LanguagesHelper;
import amrk.app.amrk_driver.utils.session.UserHelper;

public class SplashFragment extends BaseFragment {
    FragmentSplashBinding fragmentSplashBinding;
    @Inject
    SplashViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSplashBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        fragmentSplashBinding.setViewmodel(viewModel);
        viewModel.runSplash();
        setEvent();
        return fragmentSplashBinding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (((Mutable) o).message.equals(Constants.MENU_HOME)) {
                UserHelper.getInstance(requireActivity()).userHomeData(((HomeDriverResponse) mutable.object).getData());
                MovementHelper.startActivityMain(requireActivity());
            } else if (((Mutable) o).message.equals(Constants.BACKGROUND_API)) {
//                viewModel.getCountryCodes();
            } else if (mutable.message.equals(Constants.GET_COUNTRIES_CODE)) {
                if (UserHelper.getInstance(MyApplication.getInstance()).getIsFirst()) {
                    LanguagesHelper.setLanguage(LanguagesHelper.getCurrentLanguage().equals(Resources.getSystem().getConfiguration().locale.getLanguage()) ? Resources.getSystem().getConfiguration().locale.getLanguage() : LanguagesHelper.getCurrentLanguage());
                    MovementHelper.startActivityBase(requireActivity(), OnBoardFragment.class.getName(), null, null);
                } else {
                    MovementHelper.startActivityBase(requireActivity(), LoginFragment.class.getName(), null, null);
                }
//                UserHelper.getInstance(context).addCountryCodes(((CountriesCodesResponse) (mutable).object).getData().toString().replace("[", "").replace("]", "").replace(" ", ""));
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
    }


}
