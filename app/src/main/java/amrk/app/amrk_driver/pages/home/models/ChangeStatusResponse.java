package amrk.app.amrk_driver.pages.home.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_driver.model.base.StatusMessage;

public class ChangeStatusResponse extends StatusMessage {
    @SerializedName("data")
    private TripDataFromNotifications tripDataFromNotifications;

    public TripDataFromNotifications getTripDataFromNotifications() {
        return tripDataFromNotifications;
    }
}
