package amrk.app.amrk_driver.pages.chat.viewmodel;


import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.pages.chat.model.Chat;

public class ItemChatViewModel extends BaseViewModel {
    Chat chat;

    public ItemChatViewModel(Chat chat) {
        this.chat = chat;
    }

    @Bindable
    public Chat getChat() {
        return chat;
    }

    @BindingAdapter("android:layoutDirection")
    public static void chatAdminDirection(ConstraintLayout constraintLayout, int senderType) {
        if (senderType == 1) {
            constraintLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else
            constraintLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
    }
}
