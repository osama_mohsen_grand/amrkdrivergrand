package amrk.app.amrk_driver.pages.settings.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.databinding.ItemCarCategoriesBinding;
import amrk.app.amrk_driver.pages.settings.models.carCategories.CarLevels;
import amrk.app.amrk_driver.pages.settings.viewModels.ItemCarCategoryViewModel;


public class CarLevelsAdapter extends RecyclerView.Adapter<CarLevelsAdapter.ViewHolder> {
    public List<CarLevels> carLevelsList;
    Context context;

    public CarLevelsAdapter() {
        carLevelsList = new ArrayList<>();
    }

    public List<CarLevels> getCarLevelsList() {
        return carLevelsList;
    }

    @Override
    public @NotNull ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_car_categories,
                parent, false);
        context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder holder, int position) {
        CarLevels dataModel = carLevelsList.get(position);
        ItemCarCategoryViewModel homeItemViewModels = new ItemCarCategoryViewModel(dataModel);
        holder.setViewModel(homeItemViewModels);
    }


    @Override
    public int getItemCount() {
        return this.carLevelsList.size();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@NotNull List<CarLevels> data) {
        this.carLevelsList.clear();

        this.carLevelsList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemCarCategoriesBinding itemBinding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemBinding == null) {
                itemBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemBinding != null) {
                itemBinding.unbind();
            }
        }

        void setViewModel(ItemCarCategoryViewModel itemViewModels) {
            if (itemBinding != null) {
                itemBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
