package amrk.app.amrk_driver.pages.chat.view;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.pushnotifications.BeamsCallback;
import com.pusher.pushnotifications.PushNotifications;
import com.pusher.pushnotifications.PusherCallbackError;
import com.pusher.pushnotifications.auth.AuthData;
import com.pusher.pushnotifications.auth.BeamsTokenProvider;

import java.util.HashMap;

import javax.inject.Inject;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.databinding.FragmentChatBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.chat.model.Chat;
import amrk.app.amrk_driver.pages.chat.model.ChatResponse;
import amrk.app.amrk_driver.pages.chat.model.ChatSendResponse;
import amrk.app.amrk_driver.pages.chat.viewmodel.ChatViewModel;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.services.RealTimeReceiver;
import amrk.app.amrk_driver.utils.session.LanguagesHelper;

public class ChatFragment extends BaseFragment implements RealTimeReceiver.MessageReceiverListener {
    FragmentChatBinding binding;
    @Inject
    ChatViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        viewModel.chat();
        setEvent();
        connectPusher(Constants.chatEventName, Constants.tripBeamName.concat(viewModel.userData.getJwt()), Constants.chatChannelName.concat(viewModel.userData.getJwt()));
        return binding.getRoot();
    }


    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (mutable.message.equals(Constants.CHAT)) {
                viewModel.adapter.update(((ChatResponse) mutable.object).getData());
                if (viewModel.adapter.getChatList().size() > 0)
                    new Handler(Looper.getMainLooper()).postDelayed(() -> binding.rcChat.smoothScrollToPosition(viewModel.adapter.getChatList().size() - 1), 200);
            } else if (((Mutable) o).message.equals(Constants.SEND_MESSAGE)) {
                ChatSendResponse chatSendResponse = (ChatSendResponse) ((Mutable) o).object;
                viewModel.adapter.getChatList().add(chatSendResponse.getChatData());
                viewModel.fileObjectList.clear();
                binding.message.setText("");
                binding.message.setHint(getResources().getString(R.string.chat_hint));
                binding.rcChat.scrollToPosition(viewModel.adapter.getItemCount() - 1);
                viewModel.adapter.notifyItemChanged(viewModel.adapter.getItemCount() - 1);
            } else if (((Mutable) o).message.equals(Constants.PUSHER_LIVE_DATA)) {
                binding.rcChat.scrollToPosition(viewModel.adapter.getChatList().size() - 1);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
        MyApplication.getInstance().setMessageReceiverListener(this);
    }

    public void connectPusher(String eventName, String beamName, String channelName) {
        PusherOptions options = new PusherOptions();
        options.setCluster("eu");
        Pusher pusher = new Pusher("75b53060b1f415501d21", options);
        pusher.connect();
        Channel channel = pusher.subscribe(channelName);
        channel.bind(eventName, event -> {
            ChatSendResponse chatSendResponse = new Gson().fromJson(event.getData(), ChatSendResponse.class);
            viewModel.adapter.getChatList().add(chatSendResponse.getMessagePusher());
            viewModel.adapter.notifyItemInserted(viewModel.adapter.getChatList().size() - 1);
            viewModel.liveData.postValue(new Mutable(Constants.PUSHER_LIVE_DATA));
        });
        PushNotifications.start(MyApplication.getInstance(), "4b9224fd-ac88-4e17-86bf-65c347bc0fbd");
        PushNotifications.addDeviceInterest("drivers");
        BeamsTokenProvider tokenProvider = new BeamsTokenProvider(Constants.PUSHER_TOKEN_URL, () -> {
            HashMap<String, String> headers = new HashMap<>();
            headers.put("jwt", LanguagesHelper.getJwt());
            HashMap<String, String> queryParams = new HashMap<>();
            return new AuthData(
                    headers,
                    queryParams
            );
        });
        PushNotifications.setUserId(beamName, tokenProvider, new BeamsCallback<Void, PusherCallbackError>() {
            @Override
            public void onSuccess(@NonNull Void... values) {
                Log.e("PusherBeams", "Successfully authenticated with Pusher Beams");
            }

            @Override
            public void onFailure(PusherCallbackError error) {

                Log.e("PusherBeams", "Pusher Beams authentication failed: " + error.getMessage());
            }
        });

    }

    @Override
    public void onMessageChanged(Chat messagesItem) {
        if (messagesItem != null) {
            viewModel.adapter.getChatList().add(messagesItem);
            viewModel.adapter.notifyItemInserted(viewModel.adapter.getChatList().size() - 1);
            binding.rcChat.scrollToPosition(viewModel.adapter.getChatList().size() - 1);
        }
    }
}
