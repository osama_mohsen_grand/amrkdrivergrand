package amrk.app.amrk_driver.pages.countries.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_driver.model.base.StatusMessage;


public class CountriesCodesResponse extends StatusMessage {

    @SerializedName("data")
    private List<String> data;

    public List<String> getData() {
        return data;
    }

}