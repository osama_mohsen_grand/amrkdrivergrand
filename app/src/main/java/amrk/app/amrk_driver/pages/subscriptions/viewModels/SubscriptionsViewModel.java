package amrk.app.amrk_driver.pages.subscriptions.viewModels;


import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_driver.BR;
import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.subscriptions.models.SubscriptionsData;
import amrk.app.amrk_driver.repository.AuthRepository;
import amrk.app.amrk_driver.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class SubscriptionsViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    AuthRepository repository;
    SubscriptionsData subscriptionsData;

    @Inject
    public SubscriptionsViewModel(AuthRepository repository) {
        subscriptionsData = new SubscriptionsData();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void getDriverSubscriptions() {
        compositeDisposable.add(repository.getDriverSubscriptions());
    }

    public void renewSubscriptions() {
        liveData.setValue(new Mutable(Constants.PAYMENT_METHOD));
    }

    @Bindable
    public SubscriptionsData getSubscriptionsData() {
        return subscriptionsData;
    }

    @Bindable
    public void setSubscriptionsData(SubscriptionsData subscriptionsData) {
        notifyChange(BR.subscriptionsData);
        this.subscriptionsData = subscriptionsData;
    }

    public AuthRepository getRepository() {
        return repository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }
}
