package amrk.app.amrk_driver.pages.chat.model;

import com.google.gson.annotations.SerializedName;

public class Chat {

    @SerializedName("trip_id")
    private int tripId;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("receiver_id")
    private int receiverId;
    @SerializedName("sender_type")
    private int sender_type;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("id")
    private int id;

    @SerializedName("message")
    private String message;

    @SerializedName("sender_id")
    private int senderId;
    @SerializedName("sender_image")
    private String sender_image;
    @SerializedName("receiver_image")
    private String receiver_image;
    @SerializedName("image")
    private String image;

    public int getSender_type() {
        return sender_type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSender_image() {
        return sender_image;
    }

    public void setSender_image(String sender_image) {
        this.sender_image = sender_image;
    }

    public String getReceiver_image() {
        return receiver_image;
    }

    public void setReceiver_image(String receiver_image) {
        this.receiver_image = receiver_image;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public int getTripId() {
        return tripId;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    public int getReceiverId() {
        return receiverId;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public int getSenderId() {
        return senderId;
    }

}
