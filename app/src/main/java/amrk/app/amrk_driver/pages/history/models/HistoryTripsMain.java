package amrk.app.amrk_driver.pages.history.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HistoryTripsMain {
    @SerializedName("current_page")
    int currentPage;
    @SerializedName("next_page_url")
    String nextPageUrl;
    @SerializedName("data")
    private List<HistoryData> data;

    public int getCurrentPage() {
        return currentPage;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public List<HistoryData> getData() {
        return data;
    }
}
