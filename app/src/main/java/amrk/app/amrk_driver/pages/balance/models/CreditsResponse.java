package amrk.app.amrk_driver.pages.balance.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_driver.model.base.StatusMessage;

public class CreditsResponse extends StatusMessage {

    @SerializedName("data")
    private CreditData data;

    public CreditData getData() {
        return data;
    }
}