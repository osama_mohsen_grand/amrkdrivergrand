package amrk.app.amrk_driver.pages.balance.itemViewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.pages.balance.models.bankingTransfer.MyBankingData;


public class MyBankingTransferItemViewModels extends BaseViewModel {
    MyBankingData myBankingData;

    public MyBankingTransferItemViewModels(MyBankingData myBankingData) {
        this.myBankingData = myBankingData;

    }

    @Bindable
    public MyBankingData getMyBankingData() {
        return myBankingData;
    }

}
