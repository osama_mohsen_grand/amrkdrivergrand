package amrk.app.amrk_driver.pages.countries;


import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.countries.adapters.CountriesAdapter;
import amrk.app.amrk_driver.repository.AuthRepository;
import amrk.app.amrk_driver.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class CountriesViewModel extends BaseViewModel {
    MutableLiveData<Mutable> liveData;
     CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    AuthRepository repository;
    CountriesAdapter countriesAdapter;

    @Inject
    public CountriesViewModel(AuthRepository repository) {
        countriesAdapter = new CountriesAdapter();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void getCountries() {
        compositeDisposable.add(repository.getCountries());
    }

    public CountriesAdapter getCountriesAdapter() {
        return countriesAdapter;
    }

    public void toLogin() {
//        if (getPassingObject() != null)
//            compositeDisposable.add(repository.updateCountry(getCountriesAdapter().getCountriesList().get(getCountriesAdapter().selectedCountry).getId()));
//        else
            liveData.setValue(new Mutable(Constants.LOGIN));
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }
}
