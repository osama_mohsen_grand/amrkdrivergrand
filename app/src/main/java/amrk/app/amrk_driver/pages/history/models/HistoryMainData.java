package amrk.app.amrk_driver.pages.history.models;

import com.google.gson.annotations.SerializedName;

public class HistoryMainData {
    @SerializedName("total_trips")
    private String total_trips;
    @SerializedName("total_profits")
    private String total_profits;
    @SerializedName("trips")
    private HistoryTripsMain data;

    public HistoryTripsMain getData() {
        return data;
    }

    public String getTotal_trips() {
        return total_trips;
    }

    public String getTotal_profits() {
        return total_profits;
    }
}
