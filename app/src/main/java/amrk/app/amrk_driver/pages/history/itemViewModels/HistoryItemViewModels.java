package amrk.app.amrk_driver.pages.history.itemViewModels;

import android.util.Log;

import androidx.databinding.Bindable;

import java.text.DecimalFormat;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.pages.history.models.HistoryData;


public class HistoryItemViewModels extends BaseViewModel {
    HistoryData historyData;
    public String start, mid, end;

    public HistoryItemViewModels(HistoryData historyData) {
        if (historyData.getTripPathsItem().size() > 1) {
            start = historyData.getTripPathsItem().get(0).getAddress();
        }
        if (historyData.getTripPathsItem().size() >= 2) {
            if (historyData.getTripPathsItem().size() == 2)
                end = historyData.getTripPathsItem().get(1).getAddress();
            if (historyData.getTripPathsItem().size() == 3) {
                mid = historyData.getTripPathsItem().get(1).getAddress();
                end = historyData.getTripPathsItem().get(2).getAddress();
            }
        }
        this.historyData = historyData;
    }

    @Bindable
    public HistoryData getHistoryData() {
        DecimalFormat formatter = new DecimalFormat("#,###,##.##");
        String yourFormattedString = formatter.format(Double.parseDouble(historyData.getTripTotal()));
        Log.e("getHistoryData", "getHistoryData: " + yourFormattedString);
        historyData.setTripTotal(yourFormattedString);
        return historyData;
    }

}
