package amrk.app.amrk_driver.pages.home.models;

import com.google.gson.annotations.SerializedName;

public class EndTripLocationsRequest {
    @SerializedName("lat")
    private double lat;
    @SerializedName("lng")
    private double lng;
    @SerializedName("speed")
    private float speed;
    @SerializedName("time")
    private String time;

    public EndTripLocationsRequest(double lat, double lng, float speed, String time) {
        this.lat = lat;
        this.lng = lng;
        this.speed = speed;
        this.time = time;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
