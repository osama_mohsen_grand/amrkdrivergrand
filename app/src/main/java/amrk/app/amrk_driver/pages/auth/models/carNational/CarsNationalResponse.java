package amrk.app.amrk_driver.pages.auth.models.carNational;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_driver.model.base.StatusMessage;

public class CarsNationalResponse extends StatusMessage {

	@SerializedName("data")
	private CarsNationals data;

	public CarsNationals getData(){
		return data;
	}

}