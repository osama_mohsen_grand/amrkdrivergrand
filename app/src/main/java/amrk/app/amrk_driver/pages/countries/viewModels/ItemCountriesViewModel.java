package amrk.app.amrk_driver.pages.countries.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.pages.countries.models.Countries;
import amrk.app.amrk_driver.utils.Constants;

public class ItemCountriesViewModel extends BaseViewModel {
    public Countries countries;

    public ItemCountriesViewModel(Countries countries) {
        this.countries = countries;
    }

    @Bindable
    public Countries getCountries() {
        return countries;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.SELECT_COUNTRY);
    }


}
