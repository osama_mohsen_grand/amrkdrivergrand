package amrk.app.amrk_driver.pages.collectMoney.models;

import com.google.gson.annotations.SerializedName;

public class CollectMoneyRequest  {
    @SerializedName("trip_id")
    private int tripId;
    @SerializedName("money")
    private String money;

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}
