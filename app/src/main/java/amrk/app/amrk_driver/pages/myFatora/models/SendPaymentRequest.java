package amrk.app.amrk_driver.pages.myFatora.models;

import androidx.databinding.ObservableField;

import com.google.gson.annotations.SerializedName;

public class SendPaymentRequest {
    //For UI
    private String cardHolderName;
    private String cardNumber;
    private String cardExpireDate;
    private String cardCvv;
    private boolean save;
    public transient ObservableField<String> cardHolderNameError = new ObservableField<>();
    public transient ObservableField<String> cardNumberError = new ObservableField<>();
    public transient ObservableField<String> cardExpireDateError = new ObservableField<>();
    public transient ObservableField<String> cardCvvError = new ObservableField<>();
    //For CHECK PAYMENT IN DB
    @SerializedName("amount")
    private double amount;
    @SerializedName("transaction_id")
    private int transactionId;
    @SerializedName("status")
    private int status;
    @SerializedName("type")
    private int type;
    @SerializedName("cc_token")
    private String cc_token;
    @SerializedName("card_num")
    private String card_num;

    public boolean isValid() {
//        boolean valid = true;
//        if (!Validate.isValid(cardHolderName, Constants.FIELD)) {
//            cardHolderNameError.set(Validate.error);
//            valid = false;
//        }
//        if (!Validate.isValid(cardNumber, Constants.CARD_NUMBER)) {
//            cardNumberError.set(Validate.error);
//            valid = false;
//        }
//        if (!Validate.isValid(cardExpireDate, Constants.CARD_EXPIRE)) {
//            cardExpireDateError.set(Validate.error);
//            valid = false;
//        }
//        if (!Validate.isValid(cardCvv, Constants.CARD_CVV)) {
//            cardCvvError.set(Validate.error);
//            valid = false;
//        }
//        return valid;
        return true;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        cardHolderNameError.set(null);
        this.cardHolderName = cardHolderName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        cardNumberError.set(null);
        this.cardNumber = cardNumber;
    }

    public String getCardExpireDate() {
        return cardExpireDate;
    }

    public void setCardExpireDate(String cardExpireDate) {
        cardExpireDateError.set(null);
        this.cardExpireDate = cardExpireDate;
    }

    public String getCardCvv() {
        return cardCvv;
    }

    public void setCardCvv(String cardCvv) {
        cardCvvError.set(null);
        this.cardCvv = cardCvv;
    }

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCc_token() {
        return cc_token;
    }

    public void setCc_token(String cc_token) {
        this.cc_token = cc_token;
    }

    public String getCard_num() {
        return card_num;
    }

    public void setCard_num(String card_num) {
        this.card_num = card_num;
    }
}
