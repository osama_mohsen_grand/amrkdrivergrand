package amrk.app.amrk_driver.pages.settings;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_driver.BR;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.databinding.FragmentCarCategoriesBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.settings.models.carCategories.CarCategoriesResponse;
import amrk.app.amrk_driver.pages.settings.viewModels.SettingsViewModel;
import amrk.app.amrk_driver.utils.Constants;

public class CarCategoriesFragment extends BaseFragment {
    private Context context;
    FragmentCarCategoriesBinding binding;
    @Inject
    SettingsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_car_categories, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        viewModel.getCarLevels();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {


        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.CAR_LEVELS:
                    viewModel.getCarLevelsAdapter().updateData(((CarCategoriesResponse) mutable.object).getData());
                    viewModel.notifyChange(BR.carLevelsAdapter);
                    break;
                case Constants.UPDATE_CARS_LEVELS:
                    toastMessage(((CarCategoriesResponse) mutable.object).mMessage);
                    finishActivity();
                    break;
                case Constants.WARNING:
                    toastErrorMessage(getString(R.string.car_warning));
                    break;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }


}
