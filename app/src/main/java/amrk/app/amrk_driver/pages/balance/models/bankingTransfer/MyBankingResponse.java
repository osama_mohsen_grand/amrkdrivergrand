package amrk.app.amrk_driver.pages.balance.models.bankingTransfer;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_driver.model.base.StatusMessage;

public class MyBankingResponse extends StatusMessage {


    @SerializedName("data")
    private List<MyBankingData> data;

    public List<MyBankingData> getData() {
        return data;
    }

}