package amrk.app.amrk_driver.pages.settings.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_driver.model.base.StatusMessage;

public class UserDocumentsResponse extends StatusMessage {
    @SerializedName("data")
    private UserDocuments data;

    public UserDocuments getData() {
        return data;
    }

}