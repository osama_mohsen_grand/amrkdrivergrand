package amrk.app.amrk_driver.pages.collectMoney;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_driver.PassingObject;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.databinding.FragmentMainCollectBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.collectMoney.viewModels.CollectViewModels;
import amrk.app.amrk_driver.pages.home.models.TripDataFromNotifications;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.helper.MovementHelper;


public class MainCollectFragment extends BaseFragment {
    FragmentMainCollectBinding mainCollectBinding;
    @Inject
    CollectViewModels collectViewModels;
    Context context;

    public MainCollectFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainCollectBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_collect, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        mainCollectBinding.setCollectViewModel(collectViewModels);
        liveDataListeners();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            collectViewModels.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            collectViewModels.setTripDataFromNotifications(new Gson().fromJson(String.valueOf(collectViewModels.getPassingObject().getObjectClass()), TripDataFromNotifications.class));
        }
        return mainCollectBinding.getRoot();
    }


    private void liveDataListeners() {
        collectViewModels.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (((Mutable) o).message.equals(Constants.PICK_MONEY))
                MovementHelper.startActivityForResultWithBundle(context, new PassingObject(collectViewModels.getTripDataFromNotifications()), null, PickUpCollectFragment.class.getName(), null);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        collectViewModels.getHomeRepository().setLiveData(collectViewModels.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            MovementHelper.finishWithResult(new PassingObject(collectViewModels.getTripDataFromNotifications()), context);
        } else {
            Log.e("onActivityResult", "onActivityResult: error");
        }
        super.onActivityResult(requestCode, resultCode, data);

    }
}
