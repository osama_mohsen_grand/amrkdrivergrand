package amrk.app.amrk_driver.pages.chat.model;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

public class RateRequest {
    @SerializedName("trip_id")
    private int trip_id;
    @SerializedName("comment")
    private String comment;
    @SerializedName("rate")
    private String rate;

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public int getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(int trip_id) {
        this.trip_id = trip_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isRateValid() {
        Log.e("isRateValid", "isRateValid: "+trip_id+rate );
        return (trip_id != 0 && rate != null);
    }

}
