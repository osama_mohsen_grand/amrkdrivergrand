package amrk.app.amrk_driver.pages.history.models;

import com.google.gson.annotations.SerializedName;

public class TripPathsItem {

    @SerializedName("address")
    private String address;

    @SerializedName("lng")
    private String lng;

    @SerializedName("id")
    private int id;

    @SerializedName("lat")
    private String lat;

    @SerializedName("status")
    private String status;

    public String getAddress() {
        return address;
    }

    public String getLng() {
        return lng;
    }

    public int getId() {
        return id;
    }

    public String getLat() {
        return lat;
    }

    public String getStatus() {
        return status;
    }
}