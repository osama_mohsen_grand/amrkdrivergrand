package amrk.app.amrk_driver.pages.balance.itemViewModels;

import androidx.databinding.Bindable;
 import androidx.lifecycle.MutableLiveData;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.pages.notifications.models.NotificationsData;


public class AddCreditItemViewModels extends BaseViewModel {
    private NotificationsData notificationsData;
    private MutableLiveData<NotificationsData> itemsOperationsLiveListener;

    public AddCreditItemViewModels(NotificationsData notificationsData) {
        this.notificationsData = notificationsData;
        this.itemsOperationsLiveListener = new MutableLiveData<>();
    }


    public MutableLiveData<NotificationsData> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener;
    }


    @Bindable
    public NotificationsData getNotificationsData() {
        return notificationsData;
    }


    public void itemAction() {
        notifyChange();
        itemsOperationsLiveListener.setValue(notificationsData);
    }

}
