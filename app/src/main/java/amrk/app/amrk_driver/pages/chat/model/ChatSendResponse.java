package amrk.app.amrk_driver.pages.chat.model;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_driver.model.base.StatusMessage;


public class ChatSendResponse extends StatusMessage {
    @SerializedName("data")
    private Chat chatData;
    @SerializedName("message")
    private Chat messagePusher;

    public Chat getChatData() {
        return chatData;
    }

    public Chat getMessagePusher() {
        return messagePusher;
    }
}
