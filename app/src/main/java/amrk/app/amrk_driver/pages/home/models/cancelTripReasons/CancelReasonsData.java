package amrk.app.amrk_driver.pages.home.models.cancelTripReasons;

import com.google.gson.annotations.SerializedName;

public class CancelReasonsData {

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("is_captin")
	private int isCaptin;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("reason")
	private String reason;


	@SerializedName("deleted_at")
	private Object deletedAt;

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getIsCaptin(){
		return isCaptin;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public String getReason() {
		return reason;
	}

	public Object getDeletedAt(){
		return deletedAt;
	}
}