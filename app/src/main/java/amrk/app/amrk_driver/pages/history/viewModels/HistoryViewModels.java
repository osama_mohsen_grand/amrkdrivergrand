package amrk.app.amrk_driver.pages.history.viewModels;


import android.view.View;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_driver.BR;
import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.history.adapters.HistoryAdapter;
import amrk.app.amrk_driver.pages.history.models.HistoryMainData;
import amrk.app.amrk_driver.pages.history.models.HistoryTripsMain;
import amrk.app.amrk_driver.repository.HistoryRepository;
import amrk.app.amrk_driver.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;


public class HistoryViewModels extends BaseViewModel {
    private HistoryAdapter historyAdapter;
    private int type = 0, reportType = 0;
    @Inject
    HistoryRepository repository;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    HistoryMainData historyMainData;
    HistoryTripsMain historyTripsMain;

    @Inject
    public HistoryViewModels(HistoryRepository repository) {
        historyMainData = new HistoryMainData();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void history(int page, boolean showProgress) {
        compositeDisposable.add(repository.getHistory(getType(), getReportType(), page, showProgress));
    }

    public void getAllTrips() {
        setType(0);
    }

    public void getCashTrips() {
        setType(1);
    }

    public void getCreditTrips() {
        setType(2);
    }

    public void changeTripType() {
        liveData.setValue(new Mutable(Constants.TRIP_TYPE));
    }

    @Bindable
    public int getType() {
        return type;
    }

    @Bindable
    public void setType(int type) {
        notifyChange(BR.type);
        this.type = type;
        history(1, true);
    }

    @Bindable
    public int getReportType() {
        return reportType;
    }

    @Bindable
    public void setReportType(int reportType) {
        notifyChange(BR.reportType);
        this.reportType = reportType;
        history(1, true);
    }

    @Bindable
    public HistoryMainData getHistoryMainData() {
        return historyMainData;
    }

    @Bindable
    public void setHistoryMainData(HistoryMainData historyMainData) {
        setHistoryTripsMain(historyMainData.getData());
        notifyChange(BR.historyMainData);
        this.historyMainData = historyMainData;
    }

    @Bindable
    public HistoryTripsMain getHistoryTripsMain() {
        return this.historyTripsMain == null ? this.historyTripsMain = new HistoryTripsMain() : this.historyTripsMain;
    }

    public void setHistoryTripsMain(HistoryTripsMain historyTripsMain) {
        if (getHistoryAdapter().historyDataList.size() > 0) {
            getHistoryAdapter().loadMore(historyTripsMain.getData());
        } else {
            getHistoryAdapter().updateData(historyTripsMain.getData());
            notifyChange(BR.historyAdapter);
        }
        setSearchProgressVisible(View.GONE);
        this.historyTripsMain = historyTripsMain;
    }

    @Bindable
    public HistoryAdapter getHistoryAdapter() {
        return this.historyAdapter == null ? this.historyAdapter = new HistoryAdapter() : this.historyAdapter;
    }

    public HistoryRepository getRepository() {
        return repository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }
}
