package amrk.app.amrk_driver.pages.notifications.itemViewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.pages.notifications.models.NotificationsData;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.session.UserHelper;


public class NotificationsItemViewModels extends BaseViewModel {
    NotificationsData notificationsData;

    public NotificationsItemViewModels(NotificationsData notificationsData) {
        this.notificationsData = notificationsData;
    }

    @Bindable
    public NotificationsData getNotificationsData() {
        return notificationsData;
    }

    //0=>without, 1=>trip
    public void itemAction() {
        if (UserHelper.getInstance(MyApplication.getInstance()).getTripId() == 0) {
            if (notificationsData.getOrder_type() == 1) {
                UserHelper.getInstance(MyApplication.getInstance()).addTripId(notificationsData.getOrder_id());
                getLiveData().setValue(Constants.TRIP_TYPE);
            }
        }
    }

}
