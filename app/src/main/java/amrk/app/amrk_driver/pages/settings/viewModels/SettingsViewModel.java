package amrk.app.amrk_driver.pages.settings.viewModels;

import android.widget.RadioGroup;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.settings.adapters.CarLevelsAdapter;
import amrk.app.amrk_driver.pages.settings.models.AboutData;
import amrk.app.amrk_driver.pages.settings.models.ContactRequest;
import amrk.app.amrk_driver.repository.SettingsRepository;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class SettingsViewModel extends BaseViewModel {
    private AboutData aboutData;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    SettingsRepository repository;
    CarLevelsAdapter carLevelsAdapter;
    ContactRequest contactRequest;

    @Inject
    public SettingsViewModel(SettingsRepository repository) {
        aboutData = new AboutData();
        contactRequest = new ContactRequest();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void getAbout() {
        compositeDisposable.add(repository.getAbout());
    }

    public void getCarLevels() {
        compositeDisposable.add(repository.getCarLevels());
    }

    public void sendContact() {
        getContactRequest().setType(2);
        if (getContactRequest().isValid()) {
            setMessage(Constants.SHOW_PROGRESS);
            compositeDisposable.add(repository.sendContact(getContactRequest()));
        }
    }

    public void updateCars() {
        List<Integer> selectedCars = new ArrayList<>();
        for (int i = 0; i < getCarLevelsAdapter().getCarLevelsList().size(); i++) {
            if (getCarLevelsAdapter().getCarLevelsList().get(i).getIsSelected() == 1)
                selectedCars.add(getCarLevelsAdapter().getCarLevelsList().get(i).getId());
        }
        if (selectedCars.size() > 0)
            compositeDisposable.add(repository.updateCars(selectedCars));
        else
            liveData.setValue(new Mutable(Constants.WARNING));
    }

    public void getTerms() {
        if (getPassingObject().getObject().equals(Constants.TERMS))
            compositeDisposable.add(repository.getTerms());
        else
            compositeDisposable.add(repository.privacyPolicy());
    }

    public void onLangChange(RadioGroup radioGroup, int id) {
        if (id == R.id.arabic) {
            lang = "ar";
        } else
            lang = "en";
    }

    public void changeLang() {
        liveData.setValue(new Mutable(Constants.LANGUAGE));
        if (UserHelper.getInstance(MyApplication.getInstance()).getUserData() != null)
            compositeDisposable.add(repository.updateLang(lang));
    }


    public SettingsRepository getRepository() {
        return repository;
    }

    @Bindable
    public AboutData getAboutData() {
        return aboutData;
    }

    @Bindable
    public void setAboutData(AboutData aboutData) {
        notifyChange(BR.aboutData);
        this.aboutData = aboutData;
    }

    @Bindable
    public CarLevelsAdapter getCarLevelsAdapter() {
        return this.carLevelsAdapter == null ? this.carLevelsAdapter = new CarLevelsAdapter() : this.carLevelsAdapter;
    }

    public ContactRequest getContactRequest() {
        return contactRequest;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }
}
