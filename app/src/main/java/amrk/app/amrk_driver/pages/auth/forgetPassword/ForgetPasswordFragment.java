package amrk.app.amrk_driver.pages.auth.forgetPassword;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_driver.PassingObject;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.databinding.FragmentForgetPasswordBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.model.base.StatusMessage;
import amrk.app.amrk_driver.pages.auth.confirmCode.ConfirmCodeFragment;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.helper.MovementHelper;

public class ForgetPasswordFragment extends BaseFragment {
    private Context context;
     FragmentForgetPasswordBinding binding;
    @Inject
    ForgetPasswordViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forget_password, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
//        binding.ccp.setCustomMasterCountries(UserHelper.getInstance(context).getCountryCodes());
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
//        viewModel.cpp = binding.ccp.getDefaultCountryCode();
//
//        binding.ccp.setOnCountryChangeListener(() -> {
//            if (viewModel.getRequest().getPhone().contains(viewModel.cpp)) {
//                viewModel.getRequest().setPhone(viewModel.getRequest().getPhone().replace(viewModel.cpp, ""));
//            }
//            viewModel.cpp = binding.ccp.getSelectedCountryCode();
//        });

        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (((Mutable) o).message.equals(Constants.FORGET_PASSWORD)) {
                toastMessage(((StatusMessage) mutable.object).mMessage);
                MovementHelper.startActivityWithBundle(context, new PassingObject(Constants.CHECK_CONFIRM_NAV_FORGET, viewModel.getRequest().getPhone()), null, ConfirmCodeFragment.class.getName(),null);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
