package amrk.app.amrk_driver.pages.balance.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.databinding.PayBankingTransferItemBinding;
import amrk.app.amrk_driver.pages.balance.itemViewModels.PayBankingTransferItemViewModels;
import amrk.app.amrk_driver.pages.balance.models.bankingTransfer.TransferData;
import amrk.app.amrk_driver.utils.helper.MovementHelper;
import amrk.app.amrk_driver.utils.resources.ResourceManager;


public class MyPayBankingAdapter extends RecyclerView.Adapter<MyPayBankingAdapter.ViewHolder> {
    public List<TransferData> transferData;
    Context context;
    public int lastAccountId;
    public int position;

    public MyPayBankingAdapter() {
        transferData = new ArrayList<>();
    }


    @Override
    public @NotNull ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pay_banking_transfer_item,
                parent, false);
        context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder holder, int position) {
        TransferData dataModel = transferData.get(position);
        PayBankingTransferItemViewModels homeItemViewModels = new PayBankingTransferItemViewModels(dataModel);
        homeItemViewModels.getLiveData().observe(((LifecycleOwner) MovementHelper.unwrap(context)), o -> {
            notifyItemChanged(this.position);
            this.position = position;
            lastAccountId = dataModel.getId();
            notifyItemChanged(position);
        });
        if (lastAccountId == dataModel.getId())
            holder.itemBinding.textBankName.setTextColor(ResourceManager.getColor(R.color.colorPrimaryDark));
        else
            holder.itemBinding.textBankName.setTextColor(ResourceManager.getColor(R.color.black));
        holder.setViewModel(homeItemViewModels);
    }

    @Override
    public int getItemCount() {
        return this.transferData.size();
    }

    //
    @Override
    public void onViewAttachedToWindow(@NotNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@NotNull List<TransferData> data) {
        this.transferData.clear();

        this.transferData.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        PayBankingTransferItemBinding itemBinding;

        //
        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemBinding == null) {
                itemBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemBinding != null) {
                itemBinding.unbind();
            }
        }

        void setViewModel(PayBankingTransferItemViewModels itemViewModels) {
            if (itemBinding != null) {
                itemBinding.setMyBankingItemViewModels(itemViewModels);
            }
        }
    }
}
