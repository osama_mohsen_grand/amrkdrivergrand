package amrk.app.amrk_driver.pages.history.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.databinding.HistoryItemBinding;
import amrk.app.amrk_driver.pages.history.itemViewModels.HistoryItemViewModels;
import amrk.app.amrk_driver.pages.history.models.HistoryData;


public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    public List<HistoryData> historyDataList;
    Context context;

    public HistoryAdapter() {
        historyDataList = new ArrayList<>();
    }


    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_item,
                parent, false);
        context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        HistoryData dataModel = historyDataList.get(position);
        HistoryItemViewModels homeItemViewModels = new HistoryItemViewModels(dataModel);
        holder.setViewModel(homeItemViewModels);
    }

    @Override
    public int getItemCount() {
        return this.historyDataList.size();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@NotNull List<HistoryData> data) {
        this.historyDataList.clear();

        this.historyDataList.addAll(data);
        notifyDataSetChanged();
    }
    public void loadMore(@NotNull List<HistoryData> dataList) {
        int start = historyDataList.size();
        historyDataList.addAll(dataList);
        notifyItemRangeInserted(start, dataList.size());
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        HistoryItemBinding itemBinding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemBinding == null) {
                itemBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemBinding != null) {
                itemBinding.unbind();
            }
        }

        void setViewModel(HistoryItemViewModels itemViewModels) {
            if (itemBinding != null) {
                itemBinding.setHistoryItemViewModels(itemViewModels);
            }
        }
    }
}
