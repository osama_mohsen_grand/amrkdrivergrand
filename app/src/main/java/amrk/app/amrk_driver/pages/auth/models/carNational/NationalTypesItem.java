package amrk.app.amrk_driver.pages.auth.models.carNational;

import com.google.gson.annotations.SerializedName;

public class NationalTypesItem{

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;
	@SerializedName("num")
	private int num;

	public int getNum() {
		return num;
	}

	public String getName(){
		return name;
	}

	public int getId(){
		return id;
	}
}