package amrk.app.amrk_driver.pages.balance.viewModels;

import android.text.TextUtils;
import android.widget.RadioGroup;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import javax.inject.Inject;

import amrk.app.amrk_driver.BR;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.connection.FileObject;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.balance.adapters.MyBankingTransferAdapter;
import amrk.app.amrk_driver.pages.balance.adapters.MyPayBankingAdapter;
import amrk.app.amrk_driver.pages.balance.models.AddCreditRequest;
import amrk.app.amrk_driver.pages.balance.models.CreditData;
import amrk.app.amrk_driver.repository.SettingsRepository;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.resources.ResourceManager;
import io.reactivex.disposables.CompositeDisposable;


public class BalanceViewModel extends BaseViewModel {
    private MyPayBankingAdapter payBankingAdapter;
    private MyBankingTransferAdapter transferAdapter;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    SettingsRepository repository;
    public MutableLiveData<Mutable> liveData;
    CreditData creditData;
    int receivableType;
    AddCreditRequest addCreditRequest;
    boolean amountEnabled = true;
    ArrayList<FileObject> fileObjectList;

    @Inject
    public BalanceViewModel(SettingsRepository repository) {
        fileObjectList = new ArrayList<>();
        addCreditRequest = new AddCreditRequest();
        creditData = new CreditData();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void balance() {
        compositeDisposable.add(repository.getDriverBalance());
    }

    public void bankAccounts() {
        compositeDisposable.add(repository.getBankAccounts());
    }

    public void bankingTransfers() {
        compositeDisposable.add(repository.getBankingTransfers());
    }

    public void addCredit() {
        if (!TextUtils.isEmpty(getAddCreditRequest().getWallet())) {
//            setMessage(Constants.SHOW_PROGRESS);
//            compositeDisposable.add(repository.addCredit(getAddCreditRequest()));
            liveData.setValue(new Mutable(Constants.PAYMENT_METHOD));
        }
    }

    public void uploadTransferDocument() {
        if (getPayBankingAdapter().lastAccountId != 0 && getFileObjectList().size() > 0) {
            setMessage(Constants.SHOW_PROGRESS);
            compositeDisposable.add(repository.uploadTransferDocument(getPayBankingAdapter().lastAccountId, getFileObjectList()));
        } else
            liveData.setValue(new Mutable(Constants.WARNING));
    }

    public void toBankTransfer() {
        liveData.setValue(new Mutable(Constants.BANKING_TRANSFER));
    }

    public void toPayment() {
        liveData.setValue(new Mutable(Constants.BANKING_RECEIVABLE));
    }

    public void toAddBalance() {
        liveData.setValue(new Mutable(Constants.ADD_BALANCE));
    }

    public void toRenewSubscriptions() {
        liveData.setValue(new Mutable(Constants.RENEW_SUBSCRIPTIONS));
    }

    public void toDateSubscriptions() {
        liveData.setValue(new Mutable(Constants.DATE_SUBSCRIPTIONS));
    }

    public void toTransferAction() {
        if (getReceivableType() == 0)
            liveData.setValue(new Mutable(Constants.BANKING_TRANSFER));
        else
            liveData.setValue(new Mutable(Constants.PAYMENT_METHOD));
    }

    public void toTransfer() {
        setReceivableType(0);
    }

    public void toVisa() {
        setReceivableType(1);
    }

    public void selectImage() {
        liveData.setValue(new Mutable(Constants.IMAGE));
    }

    public void amountChange(RadioGroup radioGroup, int id) {
        if (R.id.money1 == id)
            getAddCreditRequest().setWallet(ResourceManager.getString(R.string.money1));
        else if (R.id.money2 == id)
            getAddCreditRequest().setWallet(ResourceManager.getString(R.string.money2));
        else if (R.id.money3 == id)
            getAddCreditRequest().setWallet(ResourceManager.getString(R.string.money3));
        else if (R.id.money4 == id)
            getAddCreditRequest().setWallet(ResourceManager.getString(R.string.money4));
        else if (R.id.money5 == id) {
            getAddCreditRequest().setWallet(null);
        }
        setAmountEnabled(R.id.money5 == id);
    }

    public AddCreditRequest getAddCreditRequest() {
        return addCreditRequest;
    }

    @Bindable
    public boolean isAmountEnabled() {
        return amountEnabled;
    }

    @Bindable
    public void setAmountEnabled(boolean amountEnabled) {
        notifyChange(BR.amountEnabled);
        this.amountEnabled = amountEnabled;
    }

    @Bindable
    public CreditData getCreditData() {
        return creditData;
    }

    @Bindable
    public void setCreditData(CreditData creditData) {
        notifyChange(BR.creditData);
        this.creditData = creditData;
    }

    @Bindable
    public int getReceivableType() {
        return receivableType;
    }

    @Bindable
    public void setReceivableType(int receivableType) {
        notifyChange(BR.receivableType);
        this.receivableType = receivableType;
    }

    @Bindable
    public MyPayBankingAdapter getPayBankingAdapter() {
        return this.payBankingAdapter == null ? this.payBankingAdapter = new MyPayBankingAdapter() : this.payBankingAdapter;
    }

    @Bindable
    public MyBankingTransferAdapter getTransferAdapter() {
        return this.transferAdapter == null ? this.transferAdapter = new MyBankingTransferAdapter() : this.transferAdapter;
    }

    public ArrayList<FileObject> getFileObjectList() {
        return fileObjectList;
    }

    public SettingsRepository getRepository() {
        return repository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }
}
