package amrk.app.amrk_driver.pages.auth.models.carNational;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarsNationals {

    @SerializedName("national_types")
    private List<NationalTypesItem> nationalTypes;

    @SerializedName("car_types")
    private List<CarTypesItem> carTypes;
    @SerializedName("subscriptions_types")
    private List<CarTypesItem> subTypes;

    public List<NationalTypesItem> getNationalTypes() {
        return nationalTypes;
    }

    public List<CarTypesItem> getCarTypes() {
        return carTypes;
    }

    public List<CarTypesItem> getSubTypes() {
        return subTypes;
    }
}