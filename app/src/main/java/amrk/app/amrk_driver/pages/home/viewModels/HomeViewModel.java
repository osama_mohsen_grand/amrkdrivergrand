package amrk.app.amrk_driver.pages.home.viewModels;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.View;
import android.widget.RatingBar;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_driver.BR;
import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.chat.model.RateRequest;
import amrk.app.amrk_driver.pages.home.adapters.CancelReasonsAdapter;
import amrk.app.amrk_driver.pages.home.models.EndTripLocationsRequest;
import amrk.app.amrk_driver.pages.home.models.StatusRequest;
import amrk.app.amrk_driver.pages.home.models.TripDataFromNotifications;
import amrk.app.amrk_driver.repository.HomeRepository;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.services.FetchLocations;
import amrk.app.amrk_driver.utils.services.LastLocationModel;
import amrk.app.amrk_driver.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class HomeViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    HomeRepository homeRepository;
    TripDataFromNotifications tripDataFromNotifications;
    StatusRequest statusRequest;
    CancelReasonsAdapter reasonsAdapter;
    public DatabaseReference tripsRef = null;
    ValueEventListener valueEventListener;
    RateRequest rateRequest;
    int finishVisible = View.GONE;

    @Inject
    public HomeViewModel(HomeRepository homeRepository) {
        rateRequest = new RateRequest();
        statusRequest = new StatusRequest();
        tripDataFromNotifications = new TripDataFromNotifications();
        this.homeRepository = homeRepository;
        this.liveData = new MutableLiveData<>();
        homeRepository.setLiveData(liveData);
        tripsRef = FirebaseDatabase.getInstance().getReference("Trips")
                .child(String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getTripId()));

    }

    public void updateLocation(double latitude, double longitude) {
        LastLocationModel lastLocationModel = new LastLocationModel();
        lastLocationModel.setLat(latitude);
        lastLocationModel.setLng(longitude);
        compositeDisposable.add(homeRepository.updateLocation(lastLocationModel));
    }

    public void timeBetweenDriverAndUserStartLocation(LatLng driverCurrentLocation) {
        String destination = String.valueOf(getTripDataFromNotifications().getStartLat()).concat(",").concat(String.valueOf(getTripDataFromNotifications().getStartLng()));
        String start = String.valueOf(driverCurrentLocation.latitude).concat(",").concat(String.valueOf(driverCurrentLocation.longitude));
        compositeDisposable.add(homeRepository.getLessTime(destination, start));
    }

    public void changeStatus(int status) {
        getStatusRequest().setStatus(status);
        getStatusRequest().setTrip_id(UserHelper.getInstance(MyApplication.getInstance()).getTripId());
        if (status == Constants.TRIP_CANCEL) {
            getStatusRequest().setCancel_id(getReasonsAdapter().lastSelection);
//            cancelTrip();
        }
        setMessage(Constants.SHOW_PROGRESS);
        compositeDisposable.add(homeRepository.changeStatus(getStatusRequest()));
    }

    public void getLastTrip(int status) {
        getStatusRequest().setStatus(status);
        getStatusRequest().setTrip_id(UserHelper.getInstance(MyApplication.getInstance()).getTripId());
        compositeDisposable.add(homeRepository.lastTrip(getStatusRequest()));
    }

    public void cancelTrip() {
        setMessage(Constants.SHOW_PROGRESS);
        compositeDisposable.add(homeRepository.changeStatus(getStatusRequest()));
    }

    public void updateCaptainBusy() {
        compositeDisposable.add(homeRepository.updateCaptainBusy());
    }

    public void rateTrip() {
        getRateRequest().setTrip_id(UserHelper.getInstance(MyApplication.getInstance()).getTripId());
        if (getRateRequest().isRateValid()) {
            compositeDisposable.add(homeRepository.rateTrip(getRateRequest()));
        } else {
            liveData.setValue(new Mutable(Constants.WARNING));
        }
    }

    public void changeDriverOnlineStatus(int online) {
        compositeDisposable.add(homeRepository.changeDriverOnlineStatus(online));
    }

    public void cancelTripReasons() {
        setMessage(Constants.SHOW_PROGRESS);
        compositeDisposable.add(homeRepository.cancelReasons());
    }

    public void showMapWithStartAddress() {
        liveData.setValue(new Mutable(Constants.MAP_WITH_START_ADDRESS));
    }

    public void showMapWithFullAddress() {
        liveData.setValue(new Mutable(Constants.FULL_ADDRESS));
    }

    public void finishTrip() {
        setMessage(Constants.SHOW_PROGRESS);
//        fetchTripLocations(locations -> {
        String distance = String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getTripLastDistance() / 1000);
        statusRequest.setDistance(distance);
        changeStatus(Constants.TRIP_FINISH);
//        });

    }

    private void fetchTripLocations(FetchLocations fetchLocations) {
        ArrayList<EndTripLocationsRequest> locations = new ArrayList<>();
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                locations.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    try {
                        if (!Objects.requireNonNull(ds.getKey()).equals(String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getUserData().getId()))) {
                            try {
                                Double latitude = ds.child("lat").getValue(Double.class);
                                Double longitude = ds.child("lng").getValue(Double.class);
                                Float speed = ds.child("speed").getValue(Float.class);
                                locations.add(new EndTripLocationsRequest(latitude, longitude, speed, ds.getKey()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (IndexOutOfBoundsException e) {
                        Log.d("Index EXception", e.getMessage());
                    }
                }
                tripsRef.removeEventListener(valueEventListener);
                fetchLocations.locations(locations);
            }

            @Override
            public void onCancelled(@NotNull DatabaseError databaseError) {
                Log.d("TAG", "" + databaseError.getMessage());
            }
        };
        tripsRef.orderByKey().addValueEventListener(valueEventListener);
    }

    public void checkTimeForCancel() {
        Log.e("checkTimeForCancel", "checkTimeForCancel: " + findDifference(getTripDataFromNotifications().getUpdatedAt()));
    }

    public void reCenterToCurrentLocation() {
        liveData.setValue(new Mutable(Constants.CURRENT_LOCATION));
    }

    @SuppressLint("SimpleDateFormat")
    public long findDifference(String end_date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:mm");
        try {
            Date start_date = new Date();
            Date d1 = sdf.parse(sdf.format(start_date));
            Date d2 = sdf.parse(end_date);
            long difference_In_Time = Objects.requireNonNull(d1).getTime() - Objects.requireNonNull(d2).getTime();
            long difference_In_Minutes
                    = (difference_In_Time
                    / (1000 * 60))
                    % 60;
            return difference_In_Minutes;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void showCallDialog() {
        liveData.setValue(new Mutable(Constants.SHOW_CALL_DIALOG));
    }

    public void callClient() {
        liveData.setValue(new Mutable(Constants.CALL_CLIENT));
    }

    public void hideCallDialog() {
        liveData.setValue(new Mutable(Constants.HIDE_CALL_DIALOG));
    }

    public void toChat() {
        liveData.setValue(new Mutable(Constants.CHAT));
    }

    public StatusRequest getStatusRequest() {
        return statusRequest;
    }

    public RateRequest getRateRequest() {
        return rateRequest;
    }

    public void setRateRequest(RateRequest rateRequest) {
        this.rateRequest = rateRequest;
    }

    public void onRateChange(RatingBar ratingBar, float rating, boolean fromUser) {
        getRateRequest().setRate(String.valueOf(rating));
    }

    @Bindable
    public TripDataFromNotifications getTripDataFromNotifications() {
        return tripDataFromNotifications;
    }

    @Bindable
    public void setTripDataFromNotifications(TripDataFromNotifications tripDataFromNotifications) {
        notifyChange(BR.tripDataFromNotifications);
        this.tripDataFromNotifications = tripDataFromNotifications;
    }

    @Bindable
    public int getFinishVisible() {
        return finishVisible;
    }

    @Bindable
    public void setFinishVisible(int finishVisible) {
        notifyChange(BR.finishVisible);
        this.finishVisible = finishVisible;
    }

    @Bindable
    public CancelReasonsAdapter getReasonsAdapter() {
        return this.reasonsAdapter == null ? this.reasonsAdapter = new CancelReasonsAdapter() : this.reasonsAdapter;
    }

    public HomeRepository getHomeRepository() {
        return homeRepository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }


}
