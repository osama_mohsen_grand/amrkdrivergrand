package amrk.app.amrk_driver.pages.subscriptions;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import javax.inject.Inject;
import amrk.app.amrk_driver.PassingObject;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.databinding.FragmentDateSubscriptionBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.myFatora.MyFatooraMethodFragment;
import amrk.app.amrk_driver.pages.subscriptions.models.SubscriptionsResponse;
import amrk.app.amrk_driver.pages.subscriptions.viewModels.SubscriptionsViewModel;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.helper.MovementHelper;


public class SubscriptionsDateFragment extends BaseFragment {
    @Inject
    SubscriptionsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentDateSubscriptionBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_date_subscription, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        viewModel.getDriverSubscriptions();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (((Mutable) o).message.equals(Constants.RENEW_SUBSCRIPTIONS)) {
                viewModel.setSubscriptionsData(((SubscriptionsResponse) mutable.object).getData());
            } else if (Constants.PAYMENT_METHOD.equals(((Mutable) o).message)) {
                MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(1, viewModel.getSubscriptionsData().getTotal()), getString(R.string.pay), MyFatooraMethodFragment.class.getName(), null);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null)
            MovementHelper.startActivityMain(requireActivity());
    }

}
