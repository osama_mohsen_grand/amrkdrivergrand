package amrk.app.amrk_driver.pages.notifications.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_driver.model.base.StatusMessage;


public class NotificationsResponse extends StatusMessage {
    @SerializedName("data")
    private List<NotificationsData> data;
    public void setData(List<NotificationsData> data) {
        this.data = data;
    }

    public List<NotificationsData> getData() {
        return data;
    }

}