package amrk.app.amrk_driver.pages.balance.models.bankingTransfer;

import com.google.gson.annotations.SerializedName;

public class TransferData {

	@SerializedName("image")
	private String image;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("bank_name")
	private String bankName;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("account_no")
	private String accountNo;

	@SerializedName("deleted_at")
	private Object deletedAt;

	public String getImage(){
		return image;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getBankName(){
		return bankName;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public String getAccountNo(){
		return accountNo;
	}

	public Object getDeletedAt(){
		return deletedAt;
	}
}