package amrk.app.amrk_driver.pages.countries.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.databinding.ItemCountriesBinding;
import amrk.app.amrk_driver.pages.countries.models.Countries;
import amrk.app.amrk_driver.pages.countries.viewModels.ItemCountriesViewModel;


public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.ViewHolder> {
    List<Countries> countriesList;
    private Context context;
    public int selectedCountry = 0;

    public CountriesAdapter() {
        this.countriesList = new ArrayList<>();
    }

    public List<Countries> getCountriesList() {
        return countriesList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_countries,
                parent, false);
        this.context = parent.getContext();
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Countries dataModel = countriesList.get(position);
        ItemCountriesViewModel itemMenuViewModel = new ItemCountriesViewModel(dataModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> {
            selectedCountry = position;
            notifyDataSetChanged();
        });
        if (selectedCountry == position) {
            holder.itemMenuBinding.itemRadio.setImageResource(R.drawable.ic_filled_radio);
        } else
            holder.itemMenuBinding.itemRadio.setImageResource(R.drawable.ic_empty_radio);
        holder.setViewModel(itemMenuViewModel);
    }


    public void update(List<Countries> dataList) {
        this.countriesList.clear();
        countriesList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return countriesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemCountriesBinding itemMenuBinding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemCountriesViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemCountriesViewModel(itemViewModels);
            }
        }
    }
}
