package amrk.app.amrk_driver.pages.settings.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_driver.ResponseHeader;
import amrk.app.amrk_driver.base.BaseViewModel;

public class ItemDocumentViewModel extends BaseViewModel {
    public ResponseHeader menuModel;

    public ItemDocumentViewModel(ResponseHeader menuModel) {
        this.menuModel = menuModel;
    }

    @Bindable
    public ResponseHeader getMenuModel() {
        return menuModel;
    }

    public void itemAction() {
        //TODO Item Action with liveData
//        getLiveData().setValue(Constants.MENu);
    }

}
