package amrk.app.amrk_driver.pages.auth.register;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.skydoves.colorpickerview.ColorPickerDialog;
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import amrk.app.amrk_driver.BR;
import amrk.app.amrk_driver.PassingObject;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.base.ParentActivity;
import amrk.app.amrk_driver.connection.FileObject;
import amrk.app.amrk_driver.databinding.FragmentRegisterBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.auth.confirmCode.ConfirmCodeFragment;
import amrk.app.amrk_driver.pages.auth.models.UsersResponse;
import amrk.app.amrk_driver.pages.auth.models.carNational.CarTypesItem;
import amrk.app.amrk_driver.pages.auth.models.carNational.CarsNationalResponse;
import amrk.app.amrk_driver.pages.auth.models.carNational.NationalTypesItem;
import amrk.app.amrk_driver.pages.countries.models.Countries;
import amrk.app.amrk_driver.pages.countries.models.CountriesResponse;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.PopUp.PopUpMenuHelper;
import amrk.app.amrk_driver.utils.helper.MovementHelper;
import amrk.app.amrk_driver.utils.session.LanguagesHelper;
import amrk.app.amrk_driver.utils.upload.FileOperations;

public class RegisterFragment extends BaseFragment {
    private Context context;
    private FragmentRegisterBinding binding;
    @Inject
    RegisterViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
//        binding.ccp.setCustomMasterCountries(UserHelper.getInstance(context).getCountryCodes());
        viewModel.carTypes();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
//        viewModel.cpp = binding.ccp.getDefaultCountryCode();
//        binding.ccp.setOnCountryChangeListener(() -> {
//            if (viewModel.getRequest().getPhone().contains(viewModel.cpp)) {
//                viewModel.getRequest().setPhone(viewModel.getRequest().getPhone().replace(viewModel.cpp, ""));
//            }
//            viewModel.cpp = binding.ccp.getSelectedCountryCode();
//        });

        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (((Mutable) o).message) {
                case Constants.IMAGE:
                    pickImageDialogSelect(Constants.FILE_TYPE_IMAGE);
                    break;
                case Constants.REGISTER:
                    toastMessage(((UsersResponse) ((Mutable) o).object).mMessage);
                    viewModel.goBack(context);
                    MovementHelper.startActivityWithBundle(context, new PassingObject(Constants.CHECK_CONFIRM_NAV_REGISTER, viewModel.getRequest().getPhone()), null, ConfirmCodeFragment.class.getName(), null);
                    break;
                case Constants.COUNTRIES:
                    viewModel.setCountriesList(((CountriesResponse) ((Mutable) o).object).getCountriesList());
                    showCountries(viewModel.getCountriesList());
                    break;
                case Constants.CITIES:
                    viewModel.setCitiesList(((CountriesResponse) ((Mutable) o).object).getCountriesList());
                    showCities(viewModel.getCitiesList());
                    break;
                case Constants.CARS_NATIONALS:
                    viewModel.setCarsNationals(((CarsNationalResponse) ((Mutable) o).object).getData());
                    break;
                case Constants.WARNING:
                    ((ParentActivity) context).toastError(getString(R.string.empty_warning));
                    break;
                case Constants.SELECT_COUNTRY:
                    ((ParentActivity) context).toastError(getString(R.string.choose_country));
                    break;
                case Constants.PICK_UP_LOCATION:
                    MovementHelper.startMapActivityForResultWithBundle(context, new PassingObject(1, getResources().getString(R.string.choose_location)));
                    break;

            }
        });
        binding.inputRegisterCountry.getEditText().setOnClickListener(v -> {
            if (viewModel.getCountriesList().size() > 0) {
                showCountries(viewModel.getCountriesList());
            } else {
                viewModel.getCountries();
            }
        });
        binding.inputRegisterCity.getEditText().setOnClickListener(v -> {
            if (viewModel.getCitiesList().size() > 0) {
                showCities(viewModel.getCitiesList());
            } else {
                viewModel.getCities();
            }
        });
        binding.inputRegisterIdentityType.getEditText().setOnClickListener(v -> {
            if (viewModel.getCarsNationals().getNationalTypes() != null && viewModel.getCarsNationals().getNationalTypes().size() > 0) {
                showNationalsTypes(viewModel.getCarsNationals().getNationalTypes());
            }
        });
        binding.inputRegisterCarType.getEditText().setOnClickListener(v -> {
            if (viewModel.getCarsNationals().getCarTypes() != null && viewModel.getCarsNationals().getCarTypes().size() > 0) {
                showCarTypes(viewModel.getCarsNationals().getCarTypes());
            }
        });
        binding.inputRegisterType.getEditText().setOnClickListener(v -> {
            if (viewModel.getCarsNationals().getSubTypes() != null && viewModel.getCarsNationals().getSubTypes().size() > 0) {
                registerType(viewModel.getCarsNationals().getSubTypes());
            }
        });
        binding.inputRegisterCarFrontImage.getEditText().setOnClickListener(v -> pickImageDialogSelect(Constants.front_car_image_code));
        binding.inputRegisterCarBackImage.getEditText().setOnClickListener(v -> pickImageDialogSelect(Constants.back_car_code));
        binding.inputRegisterInsuranceImage.getEditText().setOnClickListener(v -> pickImageDialogSelect(Constants.insurance_image_code));
        binding.inputRegisterLicenseImage.getEditText().setOnClickListener(v -> pickImageDialogSelect(Constants.license_image_code));
        binding.inputRegisterLicenseCivilImage.getEditText().setOnClickListener(v -> pickImageDialogSelect(Constants.civil_image_code));
        binding.inputRegisterIdentityImage.getEditText().setOnClickListener(v -> pickImageDialogSelect(Constants.identity_image_code));
        binding.inputRegisterCarColor.getEditText().setOnClickListener(v -> colorDialogPicker());
    }

    private void registerType(List<CarTypesItem> subTypes) {
        PopUpMenuHelper.showSubTypePopUp(context, binding.inputRegisterType.getEditText(), subTypes).setOnMenuItemClickListener(item -> {
            binding.inputRegisterType.getEditText().setText(subTypes.get(item.getItemId()).getName());
            viewModel.getRequest().setSubscription_type(String.valueOf(subTypes.get(item.getItemId()).getId()));
            return false;
        });
    }

    private void colorDialogPicker() {
        ColorPickerDialog.Builder colorPickerView = new ColorPickerDialog.Builder(getActivity())
                .setTitle(getResources().getString(R.string.choose_color_hint))
                .setPreferenceName("test")
                .setPositiveButton(getResources().getString(R.string.save), (ColorEnvelopeListener) (envelope, fromUser) -> {
                    binding.inputRegisterCarColor.getEditText().setText("#".concat(envelope.getHexCode()));
                    viewModel.getRequest().setCar_color(binding.inputRegisterCarColor.getEditText().getText().toString());
                });
        colorPickerView.show();
    }

    private void showCountries(List<Countries> countriesList) {
        PopUpMenuHelper.showCountriesPopUp(context, binding.inputRegisterCountry.getEditText(), countriesList).setOnMenuItemClickListener(item -> {
            binding.inputRegisterCountry.getEditText().setText(countriesList.get(item.getItemId()).getName());
            viewModel.getRequest().setCountry_id(String.valueOf(countriesList.get(item.getItemId()).getId()));
            return false;
        });
    }

    private void showCities(List<Countries> countriesList) {
        PopUpMenuHelper.showCountriesPopUp(context, binding.inputRegisterCity.getEditText(), countriesList).setOnMenuItemClickListener(item -> {
            binding.inputRegisterCity.getEditText().setText(countriesList.get(item.getItemId()).getName());
            viewModel.getRequest().setCity_id(String.valueOf(countriesList.get(item.getItemId()).getId()));
            return false;
        });
    }

    private void showNationalsTypes(List<NationalTypesItem> nationalTypesItems) {
        PopUpMenuHelper.showNationalsPopUp(context, binding.inputRegisterIdentityType.getEditText(), nationalTypesItems).setOnMenuItemClickListener(item -> {
            binding.inputRegisterIdentityType.getEditText().setText(nationalTypesItems.get(item.getItemId()).getName());
            viewModel.getRequest().setNational_id_type(String.valueOf(nationalTypesItems.get(item.getItemId()).getId()));
            viewModel.getRequest().setIdentityNum(nationalTypesItems.get(item.getItemId()).getNum());
            return false;
        });
    }

    private void showCarTypes(List<CarTypesItem> carTypesItems) {
        PopUpMenuHelper.showCarTypePopUp(context, binding.inputRegisterCarType.getEditText(), carTypesItems).setOnMenuItemClickListener(item -> {
            binding.inputRegisterCarType.getEditText().setText(carTypesItems.get(item.getItemId()).getName());
            viewModel.getRequest().setCar_level(String.valueOf(carTypesItems.get(item.getItemId()).getId()));
            return false;
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        FileObject fileObject = null;
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            fileObject = FileOperations.getFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            binding.imgRegister.setImageURI(Uri.fromFile(new File(fileObject.getFilePath())));
        } else if (requestCode == Constants.front_car_image_code) {
            fileObject = FileOperations.getFileObject(getActivity(), data, Constants.front_car_image, Constants.FILE_TYPE_IMAGE);
            binding.inputRegisterCarFrontImage.getEditText().setText(getResources().getString(R.string.front_car_add));
            viewModel.getRequest().setFrontImage("Selected");
        } else if (requestCode == Constants.back_car_code) {
            fileObject = FileOperations.getFileObject(getActivity(), data, Constants.back_car_image, Constants.FILE_TYPE_IMAGE);
            binding.inputRegisterCarBackImage.getEditText().setText(getResources().getString(R.string.back_car_add));
            viewModel.getRequest().setBackImage("Selected");
        } else if (requestCode == Constants.insurance_image_code) {
            fileObject = FileOperations.getFileObject(getActivity(), data, Constants.insurance_image, Constants.FILE_TYPE_IMAGE);
            binding.inputRegisterInsuranceImage.getEditText().setText(getResources().getString(R.string.insurance_car_add));
            viewModel.getRequest().setInsuranceImage("Selected");
        } else if (requestCode == Constants.license_image_code) {
            fileObject = FileOperations.getFileObject(getActivity(), data, Constants.license_image, Constants.FILE_TYPE_IMAGE);
            binding.inputRegisterLicenseImage.getEditText().setText(getResources().getString(R.string.license_car_add));
            viewModel.getRequest().setLicenseImage("Selected");
        } else if (requestCode == Constants.civil_image_code) {
            fileObject = FileOperations.getFileObject(getActivity(), data, Constants.civil_image, Constants.FILE_TYPE_IMAGE);
            binding.inputRegisterLicenseCivilImage.getEditText().setText(getResources().getString(R.string.civil_car_add));
            viewModel.getRequest().setCivilImage("Selected");
        } else if (requestCode == Constants.identity_image_code) {
            fileObject = FileOperations.getFileObject(getActivity(), data, Constants.identity_image, Constants.FILE_TYPE_IMAGE);
            binding.inputRegisterIdentityImage.getEditText().setText(getResources().getString(R.string.identity_number_image_add));
            viewModel.getRequest().setIdentityImage("Selected");
        } else if (requestCode == Constants.RESULT_CODE) {
            viewModel.request.setCityName(data.getStringExtra(Constants.CITY));
            viewModel.notifyChange(BR.request);
        }
        if (fileObject != null)
            viewModel.getFileObject().add(fileObject);
        super.onActivityResult(requestCode, resultCode, data);
    }

}
