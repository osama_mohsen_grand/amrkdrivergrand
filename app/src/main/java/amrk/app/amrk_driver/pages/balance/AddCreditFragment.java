package amrk.app.amrk_driver.pages.balance;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import javax.inject.Inject;

import amrk.app.amrk_driver.PassingObject;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.databinding.FragmentAddCreditBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.auth.models.UsersResponse;
import amrk.app.amrk_driver.pages.balance.viewModels.BalanceViewModel;
import amrk.app.amrk_driver.pages.myFatora.MyFatooraMethodFragment;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.helper.MovementHelper;
import amrk.app.amrk_driver.utils.session.UserHelper;


public class AddCreditFragment extends BaseFragment {
    FragmentAddCreditBinding addCreditBinding;
    @Inject
    BalanceViewModel viewModel;

    public AddCreditFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        addCreditBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_credit, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        addCreditBinding.setAddCreditViewModel(viewModel);
        liveDataListeners();

        return addCreditBinding.getRoot();

    }

    private void liveDataListeners() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (Constants.UPDATE_PROFILE.equals(((Mutable) o).message)) {
                toastMessage(((UsersResponse) mutable.object).mMessage);
                UserHelper.getInstance(context).userLogin(((UsersResponse) mutable.object).getData());
                finishActivity();
            } else if (Constants.PAYMENT_METHOD.equals(((Mutable) o).message)) {
                MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(0, viewModel.getAddCreditRequest().getWallet()), getString(R.string.pay), MyFatooraMethodFragment.class.getName(), null);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data != null) {
            finishActivity();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
