package amrk.app.amrk_driver.pages.subscriptions;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.databinding.FragmentNewSubscriptionsBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.subscriptions.models.SubscriptionsResponse;
import amrk.app.amrk_driver.pages.subscriptions.viewModels.SubscriptionsViewModel;
import amrk.app.amrk_driver.utils.Constants;


public class RenewSubscriptionsFragment extends BaseFragment {

    private Context context;
    @Inject
    SubscriptionsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentNewSubscriptionsBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_subscriptions, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        viewModel.getDriverSubscriptions();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (((Mutable) o).message.equals(Constants.RENEW_SUBSCRIPTIONS)) {
                viewModel.setSubscriptionsData(((SubscriptionsResponse) mutable.object).getData());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }


}
