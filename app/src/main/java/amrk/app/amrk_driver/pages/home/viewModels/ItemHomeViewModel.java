package amrk.app.amrk_driver.pages.home.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_driver.ResponseHeader;
import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.utils.Constants;

public class ItemHomeViewModel extends BaseViewModel {
    public ResponseHeader menuModel;

    public ItemHomeViewModel(ResponseHeader menuModel) {
        this.menuModel = menuModel;
    }

    @Bindable
    public ResponseHeader getMenuModel() {
        return menuModel;
    }

    public void itemAction() {
        //TODO Item Action with liveData
        getLiveData().setValue(Constants.MENu);
    }

}
