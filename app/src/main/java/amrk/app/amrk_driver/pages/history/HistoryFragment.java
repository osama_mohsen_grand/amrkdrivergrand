package amrk.app.amrk_driver.pages.history;


import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.databinding.FragmentHistoryBinding;
import amrk.app.amrk_driver.pages.history.models.HistoryResponse;
import amrk.app.amrk_driver.pages.history.viewModels.HistoryViewModels;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.PopUp.PopUp;
import amrk.app.amrk_driver.utils.PopUp.PopUpMenuHelper;


public class HistoryFragment extends BaseFragment {
    FragmentHistoryBinding binding;
    @Inject
    HistoryViewModels viewModel;


    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_history, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setHistoryViewModel(viewModel);
        viewModel.history(1, true);
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), o -> {
            handleActions(o);
            if (Constants.TRIP_HISTORY.equals(o.message)) {
                viewModel.getHistoryAdapter().historyDataList.clear();
                viewModel.setHistoryMainData(((HistoryResponse) o.object).getData());
            } else if (Constants.TRIP_TYPE.equals(o.message)) {
                showHistoryReportType();
            }
        });
        binding.rcTrips.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (viewModel.getSearchProgressVisible() == View.GONE && !TextUtils.isEmpty(viewModel.getHistoryTripsMain().getNextPageUrl())) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == viewModel.getHistoryAdapter().historyDataList.size() - 1) {
                        viewModel.setSearchProgressVisible(View.VISIBLE);
                        viewModel.history((viewModel.getHistoryTripsMain().getCurrentPage() + 1), false);
                    }
                }
            }
        });
    }

    private void showHistoryReportType() {
        Resources resources = context.getResources();
        List<PopUp> itemList = new ArrayList<>();
        itemList.add(new PopUp(resources.getString(R.string.weekly), 0));
        itemList.add(new PopUp(resources.getString(R.string.monthly), 1));
        itemList.add(new PopUp(resources.getString(R.string.yearly), 2));
        PopUpMenuHelper.showPopUp(context, binding.lastRidesCard, itemList).setOnMenuItemClickListener(item -> {
            binding.lastRidesItem.setText(itemList.get(item.getItemId()).getName());
            viewModel.setReportType(itemList.get(item.getItemId()).getId());
            return false;
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }
}
