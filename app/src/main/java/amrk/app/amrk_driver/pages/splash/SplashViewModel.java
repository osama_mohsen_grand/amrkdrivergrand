package amrk.app.amrk_driver.pages.splash;

import android.os.Handler;
import android.widget.RadioGroup;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;

import com.smarteist.autoimageslider.SliderView;

import javax.inject.Inject;

import amrk.app.amrk_driver.BR;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.pages.onBoard.OnBoardAdapter;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.repository.AuthRepository;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.session.LanguagesHelper;
import amrk.app.amrk_driver.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class SplashViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    OnBoardAdapter onBoardAdapter;
    @Inject
    AuthRepository repository;
    public ObservableBoolean isLangShow = new ObservableBoolean();

    @Inject
    public SplashViewModel(AuthRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void runSplash() {
        new Handler().postDelayed(() -> {
            if (UserHelper.getInstance(MyApplication.getInstance()).getUserData() != null) {
                home();
            } else if (!UserHelper.getInstance(MyApplication.getInstance()).getIsFirst()) {
                liveData.setValue(new Mutable(Constants.GET_COUNTRIES_CODE));
            } else {
                isLangShow.set(true);
            }
        }, 2000);
    }

    public void onLangChange(RadioGroup radioGroup, int id) {
        if (id == R.id.arabic) {
            lang = "ar";
        } else
            lang = "en";

    }

    public void getCountryCodes() {
        compositeDisposable.add(repository.getCountriesCodes());
    }

    public void home() {
        compositeDisposable.add(repository.getHome());
    }

    public void getSlider() {
        compositeDisposable.add(repository.getBoard());
    }

    public void toLogin() {
        liveData.setValue(new Mutable(Constants.LOGIN));
    }

    public void changeLang() {
        LanguagesHelper.setLanguage(lang);
        liveData.setValue(new Mutable(Constants.GET_COUNTRIES_CODE));
    }

    public void setupSlider(SliderView sliderView) {
        sliderView.setSliderAdapter(onBoardAdapter);
        notifyChange(BR.onBoardAdapter);
    }

    @Bindable
    public OnBoardAdapter getOnBoardAdapter() {
        return this.onBoardAdapter == null ? this.onBoardAdapter = new OnBoardAdapter() : this.onBoardAdapter;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public AuthRepository getRepository() {
        return repository;
    }

    public void toNext() {
        liveData.setValue(new Mutable(Constants.NEXT));
    }
}
