package amrk.app.amrk_driver.pages.settings.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_driver.model.base.StatusMessage;

public class AboutResponse extends StatusMessage {
    @SerializedName("data")
    @Expose
    private AboutData aboutData;

    public AboutData getAboutData() {
        return aboutData;
    }
}