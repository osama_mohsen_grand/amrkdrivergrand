package amrk.app.amrk_driver.pages.balance;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import javax.inject.Inject;

import amrk.app.amrk_driver.PassingObject;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.databinding.FragmentPaymentMethodBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.balance.models.CreditsResponse;
import amrk.app.amrk_driver.pages.balance.viewModels.BalanceViewModel;
import amrk.app.amrk_driver.pages.subscriptions.RenewSubscriptionsFragment;
import amrk.app.amrk_driver.pages.subscriptions.SubscriptionsDateFragment;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.helper.MovementHelper;
import amrk.app.amrk_driver.utils.session.UserHelper;


public class PaymentFragment extends BaseFragment {
    FragmentPaymentMethodBinding paymentMethodBinding;
    @Inject
    BalanceViewModel viewModel;

    public PaymentFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        paymentMethodBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment_method, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        paymentMethodBinding.setPaymentViewModel(viewModel);
        viewModel.balance();
        liveDataListeners();
        return paymentMethodBinding.getRoot();
    }

    private void liveDataListeners() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.CREDITS:
                    viewModel.setCreditData(((CreditsResponse) ((Mutable) o).object).getData());
                    break;
                case Constants.BANKING_TRANSFER:
                    MovementHelper.startActivity(context, MyBankingTransferFragment.class.getName(), getString(R.string.banking_transfer_text), null);
                    break;
                case Constants.BANKING_RECEIVABLE:
                    MovementHelper.startActivityForResultWithBundle(context, new PassingObject(viewModel.getCreditData().getAdminCredit()), getString(R.string.payment_method), PayReceivablesFragment.class.getName(), null);
                    break;
                case Constants.ADD_BALANCE:
                    MovementHelper.startActivityForResultWithBundle(context, new PassingObject(), getString(R.string.add_credit), AddCreditFragment.class.getName(), null);
                    break;
                case Constants.RENEW_SUBSCRIPTIONS:
                    MovementHelper.startActivity(context, RenewSubscriptionsFragment.class.getName(), getString(R.string.renew_subscribtion), null);
                    break;
                case Constants.DATE_SUBSCRIPTIONS:
                    MovementHelper.startActivity(context, SubscriptionsDateFragment.class.getName(), getString(R.string.date_subscribtion), null);
                    break;

            }
        });
        getActivityBase().connectionMutableLiveData.observe(((LifecycleOwner) context), isConnected -> {
            if (isConnected)
                viewModel.balance();
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
        if (Constants.DATA_CHANGE) {
            Constants.DATA_CHANGE = false;
            viewModel.balance();
            viewModel.userData = UserHelper.getInstance(requireActivity()).getUserData();
            viewModel.notifyChange();
        }
    }

}
