package amrk.app.amrk_driver.pages.balance.models.bankingTransfer;

import com.google.gson.annotations.SerializedName;

public class MyBankingData {

	@SerializedName("transfer_no")
	private String transferNo;

	@SerializedName("image")
	private String image;

	@SerializedName("bank_account_id")
	private int bankAccountId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("bank_name")
	private String bankName;

	@SerializedName("transfer_value")
	private String transferValue;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("deleted_at")
	private Object deletedAt;

	public String getTransferNo(){
		return transferNo;
	}

	public String getImage(){
		return image;
	}

	public int getBankAccountId(){
		return bankAccountId;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getUserId(){
		return userId;
	}

	public String getBankName(){
		return bankName;
	}

	public String getTransferValue(){
		return transferValue;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public Object getDeletedAt(){
		return deletedAt;
	}
}