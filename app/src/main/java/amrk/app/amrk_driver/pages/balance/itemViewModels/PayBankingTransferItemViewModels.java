package amrk.app.amrk_driver.pages.balance.itemViewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.pages.balance.models.bankingTransfer.TransferData;
import amrk.app.amrk_driver.utils.Constants;


public class PayBankingTransferItemViewModels extends BaseViewModel {
    TransferData transferData;

    public PayBankingTransferItemViewModels(TransferData transferData) {
        this.transferData = transferData;
    }

    @Bindable
    public TransferData getTransferData() {
        return transferData;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.BANKING_ACCOUNTS);
    }

}
