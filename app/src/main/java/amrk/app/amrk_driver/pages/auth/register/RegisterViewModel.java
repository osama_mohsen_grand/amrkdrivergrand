package amrk.app.amrk_driver.pages.auth.register;

import android.util.Log;
import android.widget.RadioGroup;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import amrk.app.amrk_driver.BR;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.connection.FileObject;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.auth.models.RegisterRequest;
import amrk.app.amrk_driver.pages.auth.models.carNational.CarsNationals;
import amrk.app.amrk_driver.pages.countries.models.Countries;
import amrk.app.amrk_driver.repository.AuthRepository;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.resources.ResourceManager;
import amrk.app.amrk_driver.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class RegisterViewModel extends BaseViewModel {
    MutableLiveData<Mutable> liveData;
    List<FileObject> fileObject;
    String cpp;
    @Inject
    AuthRepository repository;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    RegisterRequest request;
    private List<Countries> countriesList, citiesList;
    private CarsNationals carsNationals;

    @Inject
    public RegisterViewModel(AuthRepository repository) {
        carsNationals = new CarsNationals();
        fileObject = new ArrayList<>();
        countriesList = new ArrayList<>();
        citiesList = new ArrayList<>();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
        getRequest().setGender("0");
    }

    public void register() {
        getRequest().setToken(UserHelper.getInstance(MyApplication.getInstance()).getToken());
        getRequest().setCountry_id("188");
        getRequest().setCity_id("KSA");
        if (getRequest().isValid()) {
            if (fileObject != null) {
//                if (!getRequest().getPhone().startsWith(cpp)) {
//                    getRequest().setPhone(cpp + getRequest().getPhone());
//                }
                setMessage(Constants.SHOW_PROGRESS);
                compositeDisposable.add(repository.register(getRequest(), fileObject));
            } else {
                liveData.setValue(new Mutable(Constants.ERROR, ResourceManager.getString(R.string.select_image_profile)));
            }
        } else
            liveData.setValue(new Mutable(Constants.WARNING));
    }


    public void toSelectLocation() {
        liveData.setValue(new Mutable(Constants.PICK_UP_LOCATION));
    }

    public void onGenderChange(RadioGroup radioGroup, int id) {
//        if (id == R.id.radio_male) {
//            getRequest().setGender("0");
//        } else
//            getRequest().setGender("1");
    }

    public void getCountries() {
        compositeDisposable.add(repository.getCountries());
    }

    public void getCities() {
        if (getRequest().getCountry_id() != null)
            compositeDisposable.add(repository.getCities(Integer.parseInt(getRequest().getCountry_id())));
        else
            liveData.setValue(new Mutable(Constants.SELECT_COUNTRY));
    }

    public void carTypes() {
        compositeDisposable.add(repository.getCarsNationals());
    }

    public void imageSubmit() {
        liveData.setValue(new Mutable(Constants.IMAGE));
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Bindable
    public RegisterRequest getRequest() {
        return this.request == null ? this.request = new RegisterRequest() : this.request;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public List<Countries> getCountriesList() {
        return countriesList;
    }

    @Bindable
    public void setCountriesList(List<Countries> countriesList) {
        this.countriesList = countriesList;
    }

    public List<Countries> getCitiesList() {
        return citiesList;
    }

    @Bindable
    public void setCitiesList(List<Countries> citiesList) {
        this.citiesList = citiesList;
    }

    public List<FileObject> getFileObject() {
        return fileObject;
    }

    public CarsNationals getCarsNationals() {
        return carsNationals;
    }

    @Bindable
    public void setCarsNationals(CarsNationals carsNationals) {
        notifyChange(BR.carsNationals);
        this.carsNationals = carsNationals;
    }


}
