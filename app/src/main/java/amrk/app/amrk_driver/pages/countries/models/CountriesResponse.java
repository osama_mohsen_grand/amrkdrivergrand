package amrk.app.amrk_driver.pages.countries.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_driver.model.base.StatusMessage;


public class CountriesResponse extends StatusMessage {
    @SerializedName("data")
    private List<Countries> countriesList;

    public List<Countries> getCountriesList() {
        return countriesList;
    }
}