package amrk.app.amrk_driver.pages.countries;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;


import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.base.ParentActivity;
import amrk.app.amrk_driver.databinding.FragmentDetectLocationBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.helper.MovementHelper;

public class UserDetectLocation extends BaseFragment {
    private Context context;
    @Inject
    CountriesViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentDetectLocationBinding locationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_detect_location, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        locationBinding.setViewmodel(viewModel);
        setEvent();
        return locationBinding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (((Mutable) o).message.equals(Constants.HOME)) {
                ((ParentActivity) context).enableLocationDialog();
                MovementHelper.startActivityMain(context);
            } else if (((Mutable) o).message.equals(Constants.PICK_UP_LOCATION)) {
//                MovementHelper.startMapActivityForResultWithBundle(context, new PassingObject(1,getResources().getString(R.string.choose_location)));
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RESULT_CODE) {
            Intent intent = new Intent();
//            intent.putExtra(Constants.LAT, data.getDoubleExtra(Constants.LAT, 0.0));
//            intent.putExtra(Constants.LNG, data.getDoubleExtra(Constants.LNG, 0.0));
//            UserHelper.getInstance(context).addSaveLastKnownLocation(new LatLng(data.getDoubleExtra(Constants.LAT, 0.0), data.getDoubleExtra(Constants.LNG, 0.0)));
//            MovementHelper.startActivityBase(context, HomeMainFragment.class.getName(), null, null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
