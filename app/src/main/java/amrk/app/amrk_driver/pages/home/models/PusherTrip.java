package amrk.app.amrk_driver.pages.home.models;

import com.google.gson.annotations.SerializedName;

public class PusherTrip {
    @SerializedName("trip")
    private TripDataFromNotifications dataFromNotifications;

    public TripDataFromNotifications getDataFromNotifications() {
        return dataFromNotifications;
    }

}
