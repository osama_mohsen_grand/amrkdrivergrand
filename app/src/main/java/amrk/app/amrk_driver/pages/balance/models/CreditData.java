package amrk.app.amrk_driver.pages.balance.models;

import com.google.gson.annotations.SerializedName;

public class CreditData {

	@SerializedName("admin_credit")
	private String adminCredit;

	@SerializedName("driver_credit")
	private String driverCredit;

	public String getAdminCredit(){
		return adminCredit;
	}

	public String getDriverCredit(){
		return driverCredit;
	}
}