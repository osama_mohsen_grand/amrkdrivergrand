package amrk.app.amrk_driver.pages.home.models.cancelTripReasons;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_driver.model.base.StatusMessage;


public class CancelReasonsResponse extends StatusMessage {
	@SerializedName("data")
	private List<CancelReasonsData> data;

	public List<CancelReasonsData> getData(){
		return data;
	}
}