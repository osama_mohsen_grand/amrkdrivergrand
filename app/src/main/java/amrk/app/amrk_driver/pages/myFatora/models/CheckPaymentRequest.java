package amrk.app.amrk_driver.pages.myFatora.models;

import com.google.gson.annotations.SerializedName;

public class CheckPaymentRequest {
    @SerializedName("amount")
    private double amount;
    @SerializedName("transaction_id")
    private String transactionId;
    @SerializedName("status")
    private int status;
    @SerializedName("type")
    private int type;
    @SerializedName("cc_token")
    private String cc_token;
    @SerializedName("card_num")
    private String card_num;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCc_token() {
        return cc_token;
    }

    public void setCc_token(String cc_token) {
        this.cc_token = cc_token;
    }

    public String getCard_num() {
        return card_num;
    }

    public void setCard_num(String card_num) {
        this.card_num = card_num;
    }
}
