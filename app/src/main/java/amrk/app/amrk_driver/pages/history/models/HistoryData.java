package amrk.app.amrk_driver.pages.history.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_driver.pages.auth.models.UserData;


public class HistoryData {


    @SerializedName("date")
    @Expose
    private String date;

    private String staticMap;

    @SerializedName("driver_id")
    @Expose
    private int driverId;

    @SerializedName("driver_rate")
    @Expose
    private String driverRate;
    @SerializedName("rate")
    @Expose
    private String rate;

    @SerializedName("trip_total")
    @Expose
    private String tripTotal;

    @SerializedName("end_lat")
    @Expose
    private String endLat;

    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("user_rate")
    @Expose
    private String userRate;

    @SerializedName("start_lng")
    @Expose
    private String startLng;

    @SerializedName("amrk_driver")
    @Expose
    private UserData driver;

    @SerializedName("user_id")
    @Expose
    private int userId;

    @SerializedName("start_address")
    @Expose
    private String startAddress;

    @SerializedName("start_lat")
    @Expose
    private String startLat;

    @SerializedName("end_lng")
    @Expose
    private String endLng;

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("currency")
    @Expose
    private String currency;

    @SerializedName("end_address")
    @Expose
    private String endAddress;

    @SerializedName("car_info")
    @Expose
    private CarInfo carInfo;

    @SerializedName("user")
    @Expose
    private UserData user;
    @SerializedName("trip_paths")
    @Expose
    private List<TripPathsItem> tripPathsItem;

    public List<TripPathsItem> getTripPathsItem() {
        return tripPathsItem;
    }

    public String getDate() {
        return date;
    }

    public int getDriverId() {
        return driverId;
    }

    public String getDriverRate() {
        return driverRate;
    }

    public void setDriverRate(String driverRate) {
        this.driverRate = driverRate;
    }

    public String getTripTotal() {
        return tripTotal;
    }

    public void setTripTotal(String tripTotal) {
        this.tripTotal = tripTotal;
    }

    public String getEndLat() {
        return endLat;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getType() {
        return type;
    }

    public String getUserRate() {
        return userRate;
    }

    public String getStartLng() {
        return startLng;
    }

    public UserData getDriver() {
        return driver;
    }

    public int getUserId() {
        return userId;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public String getStartLat() {
        return startLat;
    }

    public String getEndLng() {
        return endLng;
    }

    public int getId() {
        return id;
    }

    public String getTime() {
        return time;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public CarInfo getCarInfo() {
        return carInfo;
    }

    public UserData getUser() {
        return user;
    }

    public String getCurrency() {
        return currency;
    }

    public String getStaticMap() {
        return staticMap;
    }

    public void setStaticMap(String staticMap) {
        this.staticMap = staticMap;
    }

    public String getRate() {
        return rate;
    }
}