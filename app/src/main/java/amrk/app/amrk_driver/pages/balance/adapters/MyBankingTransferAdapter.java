package amrk.app.amrk_driver.pages.balance.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.databinding.MyBankingTransferItemBinding;
import amrk.app.amrk_driver.pages.balance.itemViewModels.MyBankingTransferItemViewModels;
import amrk.app.amrk_driver.pages.balance.models.bankingTransfer.MyBankingData;


public class MyBankingTransferAdapter extends RecyclerView.Adapter<MyBankingTransferAdapter.ViewHolder> {
    public List<MyBankingData> myBankingData;
    Context context;

    public MyBankingTransferAdapter() {
        myBankingData = new ArrayList<>();
    }


    @Override
    public @NotNull ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_banking_transfer_item,
                parent, false);
        context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MyBankingData dataModel = myBankingData.get(position);
        MyBankingTransferItemViewModels homeItemViewModels = new MyBankingTransferItemViewModels(dataModel);

        holder.setViewModel(homeItemViewModels);
    }

    @Override
    public int getItemCount() {
        return this.myBankingData.size();
    }

    //
    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@NotNull List<MyBankingData> data) {
        this.myBankingData.clear();

        this.myBankingData.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        MyBankingTransferItemBinding itemBinding;

        //
        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemBinding == null) {
                itemBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemBinding != null) {
                itemBinding.unbind();
            }
        }

        void setViewModel(MyBankingTransferItemViewModels itemViewModels) {
            if (itemBinding != null) {
                itemBinding.setMyBankingItemViewModels(itemViewModels);
            }
        }
    }
}
