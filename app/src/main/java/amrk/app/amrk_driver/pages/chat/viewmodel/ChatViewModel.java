
package amrk.app.amrk_driver.pages.chat.viewmodel;

import android.text.TextUtils;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.connection.FileObject;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.chat.adapter.ChatAdapter;
import amrk.app.amrk_driver.pages.chat.model.ChatRequest;
import amrk.app.amrk_driver.repository.ChatRepository;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class ChatViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    @Inject
    public ChatRepository repository;
    public ChatAdapter adapter = new ChatAdapter();
    public ChatRequest request = new ChatRequest();
    public List<FileObject> fileObjectList = new ArrayList<>();
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public ChatViewModel(ChatRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void chat() {
        compositeDisposable.add(repository.getChat(UserHelper.getInstance(MyApplication.getInstance()).getTripId()));
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public void select() {
        liveData.setValue(new Mutable(Constants.IMAGE));
    }

    public void sendMessage() {
        request.setTrip_id(String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getTripId()));
        request.setIs_captin("1");
        if (!TextUtils.isEmpty(request.getMessage()))
            repository.sendChat(request);
    }
}
