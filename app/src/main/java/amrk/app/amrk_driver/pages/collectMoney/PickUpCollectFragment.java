package amrk.app.amrk_driver.pages.collectMoney;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import javax.inject.Inject;

import amrk.app.amrk_driver.PassingObject;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.databinding.FragmentPickUpCollectBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.collectMoney.viewModels.CollectViewModels;
import amrk.app.amrk_driver.pages.home.models.TripDataFromNotifications;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.helper.MovementHelper;


public class PickUpCollectFragment extends BaseFragment {
    FragmentPickUpCollectBinding pickupCollectBinding;
    @Inject
    CollectViewModels collectViewModels;

    public PickUpCollectFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        pickupCollectBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_pick_up_collect, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        pickupCollectBinding.setCollectViewModel(collectViewModels);
        liveDataListeners();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            collectViewModels.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            collectViewModels.setTripDataFromNotifications(new Gson().fromJson(String.valueOf(collectViewModels.getPassingObject().getObjectClass()), TripDataFromNotifications.class));
        }
        return pickupCollectBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        collectViewModels.getHomeRepository().setLiveData(collectViewModels.liveData);
    }

    private void liveDataListeners() {
        collectViewModels.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            collectViewModels.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (((Mutable) o).message.equals(Constants.COLLECT_MONEY)) {
                if (Double.parseDouble(collectViewModels.getCollectMoneyRequest().getMoney()) > collectViewModels.getTripDataFromNotifications().getTripTotal()) {
                    double tripTotal = Double.parseDouble(collectViewModels.getCollectMoneyRequest().getMoney()) - collectViewModels.getTripDataFromNotifications().getTripTotal();
                    double reset = (int) (Math.round(tripTotal * 100)) / 100.0;
                    collectViewModels.getTripDataFromNotifications().setTripTotal(reset);
                    MovementHelper.startActivityForResultWithBundle(context, new PassingObject(collectViewModels.getTripDataFromNotifications()), null, PutMoneyWalletFragment.class.getName(), null);
                } else
                    MovementHelper.finishWithResult(new PassingObject(collectViewModels.getTripDataFromNotifications()), context);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            MovementHelper.finishWithResult(new PassingObject(collectViewModels.getTripDataFromNotifications()), context);
        } else {
            Log.e("onActivityResult", "onActivityResult: error");
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

}
