package amrk.app.amrk_driver.pages.home.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class StatusRequest {
    @SerializedName("trip_id")
    private int trip_id;
    @SerializedName("status")
    private int status;
    @SerializedName("cancel_id")
    private int cancel_id;
    @SerializedName("distance")
    private String distance;

    @SerializedName("locations")
    private ArrayList<EndTripLocationsRequest> endTripLocationsRequests;

    public ArrayList<EndTripLocationsRequest> getEndTripLocationsRequests() {
        return endTripLocationsRequests;
    }

    public void setEndTripLocationsRequests(ArrayList<EndTripLocationsRequest> endTripLocationsRequests) {
        this.endTripLocationsRequests = endTripLocationsRequests;
    }

    public int getTrip_id() {
        return trip_id;
    }

    public int getCancel_id() {
        return cancel_id;
    }

    public void setCancel_id(int cancel_id) {
        this.cancel_id = cancel_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setTrip_id(int trip_id) {
        this.trip_id = trip_id;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
