package amrk.app.amrk_driver.pages.auth.models.home;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_driver.model.base.StatusMessage;

public class HomeDriverResponse extends StatusMessage {

    @SerializedName("data")
    private HomeDriverData data;

    public HomeDriverData getData() {
        return data;
    }

}