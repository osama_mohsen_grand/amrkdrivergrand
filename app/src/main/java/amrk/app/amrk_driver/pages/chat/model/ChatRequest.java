package amrk.app.amrk_driver.pages.chat.model;

import com.google.gson.annotations.SerializedName;

public class ChatRequest {
    @SerializedName("trip_id")
    private String trip_id;
    @SerializedName("is_captin")
    private String is_captin;
    @SerializedName("message")
    private String message;

    public boolean isValid() {
        return (message != null && !message.equals("") && !message.equals(" "));
    }

    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public String getIs_captin() {
        return is_captin;
    }

    public void setIs_captin(String is_captin) {
        this.is_captin = is_captin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
