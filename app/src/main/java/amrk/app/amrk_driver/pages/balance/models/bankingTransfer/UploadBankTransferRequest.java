package amrk.app.amrk_driver.pages.balance.models.bankingTransfer;

import com.google.gson.annotations.SerializedName;

public class UploadBankTransferRequest {
    @SerializedName("bank_account_id")
    private int bank_account_id;

    public UploadBankTransferRequest(int bank_account_id) {
        this.bank_account_id = bank_account_id;
    }

    public int getBank_account_id() {
        return bank_account_id;
    }

    public void setBank_account_id(int bank_account_id) {
        this.bank_account_id = bank_account_id;
    }
}
