package amrk.app.amrk_driver.pages.home;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.pushnotifications.BeamsCallback;
import com.pusher.pushnotifications.PushNotifications;
import com.pusher.pushnotifications.PusherCallbackError;
import com.pusher.pushnotifications.auth.AuthData;
import com.pusher.pushnotifications.auth.BeamsTokenProvider;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_driver.PassingObject;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.activity.MainActivity;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.base.ParentActivity;
import amrk.app.amrk_driver.base.maps.MapHelper;
import amrk.app.amrk_driver.base.maps.models.DistanceTimeResponse;
import amrk.app.amrk_driver.databinding.CallUserSheetBinding;
import amrk.app.amrk_driver.databinding.CancelTripBottomSheetBinding;
import amrk.app.amrk_driver.databinding.FragmentHomeBinding;
import amrk.app.amrk_driver.databinding.TripRateDialogBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.model.base.StatusMessage;
import amrk.app.amrk_driver.pages.auth.models.UserData;
import amrk.app.amrk_driver.pages.auth.models.UsersResponse;
import amrk.app.amrk_driver.pages.chat.model.RateRequest;
import amrk.app.amrk_driver.pages.chat.view.ChatFragment;
import amrk.app.amrk_driver.pages.collectMoney.MainCollectFragment;
import amrk.app.amrk_driver.pages.home.models.ChangeStatusResponse;
import amrk.app.amrk_driver.pages.home.models.PusherTrip;
import amrk.app.amrk_driver.pages.home.models.TripDataFromNotifications;
import amrk.app.amrk_driver.pages.home.models.cancelTripReasons.CancelReasonsData;
import amrk.app.amrk_driver.pages.home.models.cancelTripReasons.CancelReasonsResponse;
import amrk.app.amrk_driver.pages.home.viewModels.HomeViewModel;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.helper.AppHelper;
import amrk.app.amrk_driver.utils.helper.MovementHelper;
import amrk.app.amrk_driver.utils.resources.ResourceManager;
import amrk.app.amrk_driver.utils.services.RealTimeReceiver;
import amrk.app.amrk_driver.utils.session.LanguagesHelper;
import amrk.app.amrk_driver.utils.session.UserHelper;
import amrk.app.amrk_driver.utils.timer.circleCounter.CircularTimerListener;
import amrk.app.amrk_driver.utils.timer.circleCounter.TimeFormatEnum;

public class HomeFragment extends BaseFragment implements OnMapReadyCallback, RealTimeReceiver.NewTripReceiverListener, RoutingListener {
    Context context;
    FragmentHomeBinding binding;
    @Inject
    HomeViewModel viewModel;
    GoogleMap mMap;
    LatLng start, end = null, mid = null;
    Marker startTripLocation = null;
    List<Polyline> polyLines = null;
    Polyline polyLineOld = null;
    MapHelper mapHelper;
    BottomSheetBehavior sheetBehavior;
    BottomSheetDialog sheetReasons, callSheet;
    Dialog rateDialog;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        sheetBehavior = BottomSheetBehavior.from(binding.userSelectedBottomSheet.selectCard);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        init(savedInstanceState);
        liveDataListeners();
        connectPusher(Constants.tripEventName, Constants.tripBeamName.concat(viewModel.userData.getJwt()), Constants.tripChannelName.concat(viewModel.userData.getJwt()));
        return binding.getRoot();
    }


    private void liveDataListeners() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (((Mutable) o).message) {
                case Constants.GOOGLE_API:
                    startTimer();
                    binding.userSelectedBottomSheet.totalTimeBetweenDriverPickipLocation.setText(((DistanceTimeResponse) mutable.object).getRows().get(0).getElements().get(0).getDuration().getText());
                    break;
                case Constants.CHANGE_STATUS:
                    viewModel.setTripDataFromNotifications(((ChangeStatusResponse) mutable.object).getTripDataFromNotifications());
                    showTripSheet();
                    break;
                case Constants.ERROR_NOT_FOUND:
                    UserHelper.getInstance(MyApplication.getInstance()).addTripId(0);
                    viewModel.setTripDataFromNotifications(new TripDataFromNotifications());
                    callInCancel();
                    break;
                case Constants.CANCEL_REASONS:
                    showTripCancel(((CancelReasonsResponse) mutable.object).getData());
                    break;
                case Constants.SHOW_CALL_DIALOG:
                    showCallDialog();
                    break;
                case Constants.RATE_TRIP:
                    toastMessage(((StatusMessage) mutable.object).mMessage);
                    viewModel.setRateRequest(new RateRequest());
                    callInCancel();
                    rateDialog.dismiss();
                    break;
                case Constants.CALL_CLIENT:
                    sheetBehavior.setHideable(false);
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    AppHelper.openDialNumber(context, viewModel.getTripDataFromNotifications().getUser().getPhone());
                    callSheet.dismiss();
                    break;
                case Constants.HIDE_CALL_DIALOG:
                    callSheet.dismiss();
                    // show after hide dialog
                    sheetBehavior.setHideable(false);
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    break;
                case Constants.CHAT:
                    MovementHelper.startActivity(context, ChatFragment.class.getName(), getString(R.string.chat), null);
                    break;
                case Constants.MAP_WITH_START_ADDRESS:
                    AppHelper.startAndroidGoogleMap(context, new LatLng(viewModel.getTripDataFromNotifications().getStartLat(), viewModel.getTripDataFromNotifications().getStartLng()));
                    break;
                case Constants.FULL_ADDRESS:
                    AppHelper.startAndroidGoogleMap(context, new LatLng(mapHelper.driverLat, mapHelper.driverLng), viewModel.getTripDataFromNotifications());
                    break;
                case Constants.WARNING:
                    toastErrorMessage(getString(R.string.rate_trip_header));
                    break;
                case Constants.UPDATE_PROFILE:
                    toastErrorMessage(((UsersResponse) mutable.object).mMessage);
                    UserHelper.getInstance(context).userLogin(((UsersResponse) mutable.object).getData());
                    break;
                case Constants.CURRENT_LOCATION:
                    mapHelper.addMarker(new LatLng(mapHelper.driverLat, mapHelper.driverLng));
                    break;
                case Constants.PUSHER_LIVE_DATA:
                    navigateFromPusher();
                    break;

            }
        });
        ((MainActivity) context).homeActionBarView.layoutActionBarHomeBinding.imgOnline.setOnClickListener(v -> {
            UserData userData = UserHelper.getInstance(context).getUserData();
            if (UserHelper.getInstance(context).getUserData().getOnline() == 1) {
                userData.setOnline(0);
            } else
                userData.setOnline(1);
            UserHelper.getInstance(context).userLogin(userData);
            ((MainActivity) context).homeActionBarView.changeDriverStatus(userData.getOnline());
            viewModel.changeDriverOnlineStatus(userData.getOnline());
        });
    }

    private void navigateFromPusher() {
//        || viewModel.getTripDataFromNotifications().getStatus() == Constants.TRIP_WAITING
        if (viewModel.getTripDataFromNotifications().getStatus() == Constants.TRIP_CANCEL ) {
            callInCancel();
        } else {
            UserHelper.getInstance(context).addTripId(viewModel.getTripDataFromNotifications().getId());
            showTripSheet();
        }
    }

    @Override
    public void onTripChanged(String historyData) {
//        viewModel.setTripDataFromNotifications(historyData);
//        if (viewModel.getTripDataFromNotifications().getStatus() == Constants.TRIP_CANCEL) {
//            callInCancel();
//        } else {
//            UserHelper.getInstance(context).addTripId(historyData.getId());
//            showTripSheet();
//        }
        viewModel.getLastTrip(0);
    }

    private void callInCancel() {
        // hide when cancel
        sheetBehavior.setHideable(true);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        viewModel.getRateRequest().setComment(null);
        mMap.clear();
        mapHelper.driverMarker = null;
        mapHelper.requestLocationUpdates();
        if (sheetReasons != null)
            sheetReasons.dismiss();
        if (callSheet != null)
            callSheet.dismiss();
        viewModel.notifyChange();
        getActivityMain().stopTrackerService();
        closeKeyboard();
        getActivityMain().startTrackerService();
    }

    private void showTripSheet() {
        sheetBehavior.setPeekHeight(200);
        sheetBehavior.setHideable(false);
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        if (viewModel.getTripDataFromNotifications().getStatus() == Constants.TRIP_ACCEPTED || viewModel.getTripDataFromNotifications().getStatus() == Constants.TRIP_START) { // accepted
            if (viewModel.getTripDataFromNotifications().getStatus() == 2 && viewModel.getTripDataFromNotifications().getTripPaths().size() == 1) {
                toastMessage(getString(R.string.leading_captin));
                mMap.clear();
                binding.userSelectedBottomSheet.textArrivalVal.setText(getString(R.string.empty_dash));
                binding.userSelectedBottomSheet.textDistanceVal.setText(getString(R.string.empty_dash));
                mapHelper.getLastKnownLocation();
            } else {
                findRoutes();
            }
        } else if (viewModel.getTripDataFromNotifications().getStatus() == Constants.TRIP_ARRIVE) { // Driver arrive
            viewModel.checkTimeForCancel();
            findRoutes();
            startTimer();
            binding.userSelectedBottomSheet.btnDecline.setEnabled(false); // disabled till waiting arrive time finished
        } else if (viewModel.getTripDataFromNotifications().getStatus() == Constants.TRIP_CANCEL) { // Driver Cancel
            viewModel.setTripDataFromNotifications(new TripDataFromNotifications());
            callInCancel();
        } else if (viewModel.getTripDataFromNotifications().getStatus() == Constants.TRIP_FINISH) { // Driver FINISH
            viewModel.setFinishVisible(View.GONE);
            if (viewModel.getTripDataFromNotifications().getPay_status() == Constants.UNPAID) {
                if (viewModel.getTripDataFromNotifications().getPayment() == 0)
                    MovementHelper.startActivityForResultWithBundle(context, new PassingObject(viewModel.getTripDataFromNotifications()), null, MainCollectFragment.class.getName(), null);
            }
            viewModel.getHomeDriverData().setCollectedMoneyToday(viewModel.getHomeDriverData().getCollectedMoneyToday() + viewModel.getTripDataFromNotifications().getTripTotal());
            viewModel.setHomeDriverData(viewModel.getHomeDriverData());
            callInCancel();
        } else if (viewModel.getTripDataFromNotifications().getStatus() == Constants.TRIP_WAITING) {
            viewModel.timeBetweenDriverAndUserStartLocation(new LatLng(mapHelper.driverLat, mapHelper.driverLng));
        }
    }

    private void showTripCancel(List<CancelReasonsData> cancelReasonsData) {
        sheetBehavior.setHideable(true);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN); // hide when cancel reasons dialog opened
        viewModel.getReasonsAdapter().updateData(cancelReasonsData);
        CancelTripBottomSheetBinding sheetBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.cancel_trip_bottom_sheet, null, false);
        sheetReasons = new BottomSheetDialog(context);
        sheetReasons.setContentView(sheetBinding.getRoot());
        sheetBinding.setCancelReasonBottomSheetViewModels(viewModel);
        sheetReasons.setOnCancelListener(dialog -> {
            // show after hide dialog
            sheetBehavior.setHideable(false);
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            // reset last select reasons for avoid cash and cancel trip by accident
            viewModel.getReasonsAdapter().lastSelection = 0;
        });
        sheetReasons.show();
    }

    private void showCallDialog() {
        sheetBehavior.setHideable(true);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);// hide when cancel reasons dialog opened
        CallUserSheetBinding sheetBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.call_user_sheet, null, false);
        callSheet = new BottomSheetDialog(context);
        callSheet.setContentView(sheetBinding.getRoot());
        sheetBinding.setDriverBottomSheetViewModels(viewModel);
        callSheet.setOnCancelListener(dialog -> {
            // show after hide dialog
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        callSheet.show();
    }

    private void showRateDialog(TripDataFromNotifications tripDataFromNotifications) {
        rateDialog = new Dialog(context, R.style.PauseDialog);
        rateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(rateDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        rateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TripRateDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(rateDialog.getContext()), R.layout.trip_rate_dialog, null, false);
        rateDialog.setContentView(binding.getRoot());
        binding.setViewModel(viewModel);
        viewModel.setTripDataFromNotifications(tripDataFromNotifications);
        rateDialog.setOnDismissListener(dialog -> {
            UserHelper.getInstance(MyApplication.getInstance()).addTripId(0);
            viewModel.setTripDataFromNotifications(new TripDataFromNotifications());
        });
        rateDialog.show();
    }

    private void showDocsWarningDialog() {
        //Update drive collect money
        ((ParentActivity) context).toastError(getString(R.string.admin_warning));
//        HomeDriverData homeDriverData = viewModel.homeDriverData;
//        homeDriverData.setCollectedMoneyToday(homeDriverData.getCollectedMoneyToday() + viewModel.getTripDataFromNotifications().getTripTotal());
//        binding.money.setText(String.valueOf(homeDriverData.getCollectedMoneyToday()));
//        UserHelper.getInstance(context).userHomeData(homeDriverData);
//        Dialog dialog = new Dialog(context, R.style.PauseDialog);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.setCancelable(true);
//        WarningDocumentExpiredBinding binding = DataBindingUtil.inflate(LayoutInflater.from(dialog.getContext()), R.layout.warning_document_expired, null, false);
//        dialog.setContentView(binding.getRoot());
//        binding.setViewModel(viewModel);
//        binding.sendFeedBack.setOnClickListener(v -> MovementHelper.startActivity(context, DocumentsFragment.class.getName(), getResources().getString(R.string.tv_account_upload_documents), null));
//        dialog.show();
    }

    private void findRoutes() {
        // new Trip
        start = new LatLng(mapHelper.driverLat, mapHelper.driverLng);
        if (viewModel.getTripDataFromNotifications().getStatus() == Constants.TRIP_ACCEPTED || viewModel.getTripDataFromNotifications().getStatus() == Constants.TRIP_ARRIVE) {
            end = new LatLng(viewModel.getTripDataFromNotifications().getStartLat(), viewModel.getTripDataFromNotifications().getStartLng());
        } else if (viewModel.getTripDataFromNotifications().getStatus() == Constants.TRIP_START) { //trip started
//            start = new LatLng(viewModel.getTripDataFromNotifications().getStartLat(), viewModel.getTripDataFromNotifications().getStartLng());
            if (viewModel.getTripDataFromNotifications().getTripPaths().size() == 3)
                mid = new LatLng(Double.parseDouble(viewModel.getTripDataFromNotifications().getTripPaths().get(1).getLat()), Double.parseDouble(viewModel.getTripDataFromNotifications().getTripPaths().get(1).getLng()));
            end = new LatLng(Double.parseDouble(viewModel.getTripDataFromNotifications().getTripPaths().get(viewModel.getTripDataFromNotifications().getTripPaths().size() - 1).getLat()), Double.parseDouble(viewModel.getTripDataFromNotifications().getTripPaths().get(viewModel.getTripDataFromNotifications().getTripPaths().size() - 1).getLng()));
        }
        if (start == null || end == null) {
            Toast.makeText(getActivity(), "Unable to get location", Toast.LENGTH_LONG).show();
        } else {
            Routing routing;//also define your api key here.
            if (mid != null) {
                routing = new Routing.Builder()
                        .travelMode(AbstractRouting.TravelMode.DRIVING)
                        .withListener(this)
                        .alternativeRoutes(true)
                        .waypoints(start, mid, end)
                        .key(getResources().getString(R.string.google_map))  //also define your api key here.
                        .build();
            } else {
                routing = new Routing.Builder()
                        .travelMode(AbstractRouting.TravelMode.DRIVING)
                        .withListener(this)
                        .alternativeRoutes(true)
                        .waypoints(start, end)
                        .key(getResources().getString(R.string.google_map))  //also define your api key here.
                        .build();
            }
            routing.execute();
        }
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        findRoutes();
    }

    @Override
    public void onRoutingStart() {
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        mMap.clear();
        PolylineOptions polyOptions = new PolylineOptions();
        polyLines = new ArrayList<>();
        //add route(s) to the map using polyline
        for (int i = 0; i < route.size(); i++) {
            if (i == shortestRouteIndex) {
                binding.userSelectedBottomSheet.textArrivalVal.setText(route.get(i).getDurationText());
                binding.userSelectedBottomSheet.textDistanceVal.setText(route.get(i).getDistanceText());
                polyOptions.color(getResources().getColor(R.color.black));
                polyOptions.width(7);
                polyOptions.addAll(route.get(i).getPoints());
                Polyline polyline = mMap.addPolyline(polyOptions);
                if (polyLineOld != null) {
                    polyLineOld.remove();
                } else
                    polyLineOld = polyline;
                polyLines.add(polyline);
            }
        }
        mapHelper.driverMarker = null;
        mapHelper.latLngs.clear();
        mapHelper.addMarker(start);
        mapHelper.latLngs.add(start);
        //Add Marker on route ending position
        MarkerOptions endMarker = new MarkerOptions();
        endMarker.position(end);
        endMarker.anchor(0.5f, 0.5f);
        endMarker.icon(BitmapDescriptorFactory.fromBitmap(Objects.requireNonNull(AppHelper.resizeVectorIcon(ResourceManager.getDrawable(R.drawable.ic_current_location), 100, 100))));
        if (startTripLocation != null)
            startTripLocation.remove();
        startTripLocation = mMap.addMarker(endMarker);
        if (viewModel.getTripDataFromNotifications().getStatus() == 2 && viewModel.getTripDataFromNotifications().getTripPaths().size() == 3) {
            mapHelper.latLngs.add(mid);
            MarkerOptions midMarker = new MarkerOptions();
            midMarker.position(mid);
            midMarker.icon(BitmapDescriptorFactory.fromBitmap(Objects.requireNonNull(AppHelper.resizeVectorIcon(ResourceManager.getDrawable(R.drawable.ic_first_location), 100, 100))));
            midMarker.anchor(0.5f, 0.5f);
            mMap.addMarker(midMarker);
        }
        mapHelper.latLngs.add(end);
    }

    @Override
    public void onRoutingCancelled() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constants.RESULT_CODE) {
                showRateDialog(viewModel.getTripDataFromNotifications());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void startTimer() {
        binding.userSelectedBottomSheet.arrivedTime.setCircularTimerListener(new CircularTimerListener() {
            @Override
            public String updateDataOnTick(long remainingTimeInMs) {
                binding.userSelectedBottomSheet.btnDecline.setEnabled(false); // disabled after waiting arrive time finished
                if (viewModel.getTripDataFromNotifications().getStatus() != Constants.TRIP_WAITING && viewModel.getTripDataFromNotifications().getStatus() != Constants.TRIP_ARRIVE)
                    binding.userSelectedBottomSheet.arrivedTime.endTimer();
                return String.valueOf((int) Math.ceil((remainingTimeInMs / 1000.f)));
            }

            @Override
            public void onTimerFinished() {
                binding.userSelectedBottomSheet.btnDecline.setEnabled(true); // enabled after waiting arrive time finished
                if (viewModel.getTripDataFromNotifications().getStatus() == 0) {
                    UserHelper.getInstance(context).addTripId(0); // reset cash of trip id
                    sheetBehavior.setHideable(true);
                    sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    viewModel.updateCaptainBusy(); // for make driver not busy
                }
            }
        }, 30, TimeFormatEnum.SECONDS, 10);
// To start timer
        binding.userSelectedBottomSheet.arrivedTime.startTimer();
    }

    @Override
    public void onMapReady(@NotNull GoogleMap googleMap) {
        mMap = googleMap;
        mapHelper = new MapHelper(viewModel, mMap, context);
        mapHelper.requestLocationUpdates();
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
//            if (UserHelper.getInstance(context).getTripId() != 0)
            viewModel.getLastTrip(0);
        }, 3000);

        if (viewModel.getHomeDriverData().getFilesCompleted() == Constants.DOC_REQUIRED)
            showDocsWarningDialog();
    }

    private void init(Bundle savedInstanceState) {
        binding.mapview.onCreate(savedInstanceState);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((AppCompatActivity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
        } else {
            binding.mapview.getMapAsync(this);
        }
    }

    @Override
    public void onResume() {
        binding.mapview.onResume();
        viewModel.getHomeRepository().setLiveData(viewModel.liveData);
        MyApplication.getInstance().setTripReceiverListener(this);
        ((MainActivity) context).startTrackerService();
        super.onResume();
    }

    public void connectPusher(String eventName, String beamName, String channelName) {
        PusherOptions options = new PusherOptions();
        options.setCluster("eu");
        Pusher pusher = new Pusher("75b53060b1f415501d21", options);
        pusher.connect();
        Channel channel = pusher.subscribe(channelName);
        channel.bind(eventName, event -> {
            Log.e("connectPusher", "connectPusher: "+event.getData() );
            PusherTrip pusherTrip = new Gson().fromJson(event.getData(), PusherTrip.class);
            viewModel.setTripDataFromNotifications(pusherTrip.getDataFromNotifications());
            viewModel.liveData.postValue(new Mutable(Constants.PUSHER_LIVE_DATA));
        });
        PushNotifications.start(MyApplication.getInstance(), "4b9224fd-ac88-4e17-86bf-65c347bc0fbd");
        PushNotifications.clearAllState();
        PushNotifications.addDeviceInterest("drivers");
        BeamsTokenProvider tokenProvider = new BeamsTokenProvider(Constants.PUSHER_TOKEN_URL, () -> {
            // Headers and URL query params your auth endpoint needs to
            // request a Beams Token for a given user
            HashMap<String, String> headers = new HashMap<>();
            // for example:
            headers.put("jwt", LanguagesHelper.getJwt());
            HashMap<String, String> queryParams = new HashMap<>();
            return new AuthData(
                    headers,
                    queryParams
            );
        });

        PushNotifications.setUserId(beamName, tokenProvider, new BeamsCallback<Void, PusherCallbackError>() {
            @Override
            public void onSuccess(@NonNull Void... values) {
                Log.e("onSuccess", "onSuccess: ");
            }

            @Override
            public void onFailure(PusherCallbackError error) {
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onPause() {
        super.onPause();
        binding.mapview.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.mapview.onDestroy();
        if (UserHelper.getInstance(context).getTripId() == 0)
            ((MainActivity) context).stopTrackerService();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapview.onLowMemory();
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
