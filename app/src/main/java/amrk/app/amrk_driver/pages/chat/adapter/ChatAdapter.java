package amrk.app.amrk_driver.pages.chat.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.databinding.ItemChatBinding;
import amrk.app.amrk_driver.pages.chat.model.Chat;
import amrk.app.amrk_driver.pages.chat.viewmodel.ItemChatViewModel;
import amrk.app.amrk_driver.utils.helper.MovementHelper;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    List<Chat> chatList;
    private Context context;
    MutableLiveData<Object> chatObjectMutableLiveData = new MutableLiveData<>();
    public int lastPosition;

    public ChatAdapter() {
        this.chatList = new ArrayList<>();
    }

    public List<Chat> getChatList() {
        return chatList;
    }

    public MutableLiveData<Object> getChatObjectMutableLiveData() {
        return chatObjectMutableLiveData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat,
                parent, false);
        this.context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        Chat dataModel = chatList.get(position);
        ItemChatViewModel itemMenuViewModel = new ItemChatViewModel(dataModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) MovementHelper.unwrap(context)), o -> {
            lastPosition = position;
            chatObjectMutableLiveData.setValue(o);
        });
        holder.setViewModel(itemMenuViewModel);
    }


    public void update(List<Chat> dataList) {
        this.chatList.clear();
        chatList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemChatBinding itemMenuBinding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemChatViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemChatViewModel(itemViewModels);
            }
        }
    }
}
