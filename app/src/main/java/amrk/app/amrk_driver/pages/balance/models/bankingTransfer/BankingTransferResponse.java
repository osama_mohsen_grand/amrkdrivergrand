package amrk.app.amrk_driver.pages.balance.models.bankingTransfer;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_driver.model.base.StatusMessage;

public class BankingTransferResponse extends StatusMessage {

	@SerializedName("data")
	private List<TransferData> data;

	public List<TransferData> getData(){
		return data;
	}

}