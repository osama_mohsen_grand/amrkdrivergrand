package amrk.app.amrk_driver.pages.balance;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import javax.inject.Inject;

import amrk.app.amrk_driver.BR;
import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.databinding.FragmentMyTransferBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.balance.models.bankingTransfer.MyBankingResponse;
import amrk.app.amrk_driver.pages.balance.viewModels.BalanceViewModel;
import amrk.app.amrk_driver.utils.Constants;


public class MyBankingTransferFragment extends BaseFragment {
    FragmentMyTransferBinding myTransferBinding;
    @Inject
    BalanceViewModel viewModel;


    public MyBankingTransferFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myTransferBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_transfer, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        myTransferBinding.setMyBankingViewModel(viewModel);
        viewModel.bankingTransfers();
        liveDataListeners();
        return myTransferBinding.getRoot();
    }


    private void liveDataListeners() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.BANKING_TRANSFER.equals(((Mutable) o).message)) {
                viewModel.getTransferAdapter().updateData(((MyBankingResponse) mutable.object).getData());
                viewModel.notifyChange(BR.transferAdapter);
            }
        });
    }

}
