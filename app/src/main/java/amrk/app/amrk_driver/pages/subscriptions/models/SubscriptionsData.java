package amrk.app.amrk_driver.pages.subscriptions.models;

import com.google.gson.annotations.SerializedName;

public class SubscriptionsData {

    @SerializedName("monthley_subscribtion")
    private String monthleySubscribtion;

    @SerializedName("total")
    private String total;

    @SerializedName("tax")
    private String tax;
    @SerializedName("date")
    private String dateRenew;

    public String getDateRenew() {
        return dateRenew;
    }

    public String getMonthleySubscribtion() {
        return monthleySubscribtion;
    }

    public String getTotal() {
        return total;
    }

    public String getTax() {
        return tax;
    }
}