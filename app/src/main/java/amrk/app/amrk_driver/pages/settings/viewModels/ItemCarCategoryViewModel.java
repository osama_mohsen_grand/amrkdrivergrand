package amrk.app.amrk_driver.pages.settings.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.pages.settings.models.carCategories.CarLevels;

public class ItemCarCategoryViewModel extends BaseViewModel {
    public CarLevels carLevels;

    public ItemCarCategoryViewModel(CarLevels carLevels) {
        this.carLevels = carLevels;
    }

    @Bindable
    public CarLevels getCarLevels() {
        return carLevels;
    }

    public void itemAction() {
        if (carLevels.getIsSelected() == 0) {
            carLevels.setIsSelected(1);
        } else
            carLevels.setIsSelected(0);
        notifyChange();
    }

}
