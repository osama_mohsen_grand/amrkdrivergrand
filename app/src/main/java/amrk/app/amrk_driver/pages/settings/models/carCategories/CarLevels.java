package amrk.app.amrk_driver.pages.settings.models.carCategories;

import com.google.gson.annotations.SerializedName;

public class CarLevels {

	@SerializedName("image")
	private String image;

	@SerializedName("distance_trip_unit")
	private double distanceTripUnit;

	@SerializedName("is_selected")
	private int isSelected;

	@SerializedName("name")
	private String name;

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private int id;

	@SerializedName("start_trip_unit")
	private double startTripUnit;

	public String getImage(){
		return image;
	}

	public double getDistanceTripUnit(){
		return distanceTripUnit;
	}

	public int getIsSelected(){
		return isSelected;
	}

	public void setIsSelected(int isSelected) {
		this.isSelected = isSelected;
	}

	public String getName(){
		return name;
	}

	public String getDescription(){
		return description;
	}

	public int getId(){
		return id;
	}

	public double getStartTripUnit(){
		return startTripUnit;
	}
}