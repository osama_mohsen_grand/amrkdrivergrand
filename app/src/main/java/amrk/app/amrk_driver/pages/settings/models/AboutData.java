package amrk.app.amrk_driver.pages.settings.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AboutData {

    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("name")
    @Expose
    private String name;

    public String getBody() {
        return body;
    }

    public String getName() {
        if (body != null)
            name = body;
        return name;
    }
}