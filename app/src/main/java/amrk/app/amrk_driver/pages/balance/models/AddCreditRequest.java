package amrk.app.amrk_driver.pages.balance.models;

import com.google.gson.annotations.SerializedName;

public class AddCreditRequest {
    @SerializedName("wallet")
    private String wallet;

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }
}
