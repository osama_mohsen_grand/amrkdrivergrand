package amrk.app.amrk_driver.pages.myFatora;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.myfatoorah.sdk.entity.executepayment.MFExecutePaymentRequest;
import com.myfatoorah.sdk.entity.executepayment_cardinfo.MFCardInfo;
import com.myfatoorah.sdk.entity.executepayment_cardinfo.MFDirectPaymentResponse;
import com.myfatoorah.sdk.entity.initiatepayment.MFInitiatePaymentRequest;
import com.myfatoorah.sdk.entity.initiatepayment.MFInitiatePaymentResponse;
import com.myfatoorah.sdk.entity.paymentstatus.MFGetPaymentStatusResponse;
import com.myfatoorah.sdk.utils.MFAPILanguage;
import com.myfatoorah.sdk.utils.MFBaseURL;
import com.myfatoorah.sdk.utils.MFCurrencyISO;
import com.myfatoorah.sdk.utils.MFMobileISO;
import com.myfatoorah.sdk.views.MFResult;
import com.myfatoorah.sdk.views.MFSDK;

import java.util.Objects;

import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.myFatora.models.SendPaymentRequest;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.session.LanguagesHelper;
import amrk.app.amrk_driver.utils.session.UserHelper;
import kotlin.Unit;

public class MyFatoraInit {
    //    private static final String API_KEY = "rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL";
    private static final String API_KEY = "KLJew3bKpkx9DxixsosqzZY4QZI9mB7pEnKZkAo6auT7C1b7KBqLq29aimpDB-R0EvjhDa-aXttwyk7s0kkenahjFPP6E2sZLvlODeqxRNyxmVdf-JCrws6t-K_sxu6ksAQimg9bkEMimV_8m-ek2o68helzvSy0cROKbcM7vKajou8W6TlK5hspviqrEZoBWGyeuCPXTkfl004HUwj9diWudQlkaG7X_17KFUJLeypgwXN86NTz8tF14v4F4QDG1xv0Xe13Gm5z35Xk-iIABVeeQ6tWfN4gAJtTdBACl5EYmlT42Uxjv9-BZkhp5t0T9_QqpR9A9yXg3YEOXRVuYRZYnmn3h0plyzHytlQ-uUNqIvw_Cy6I3azY2Runyqalsb3-iyowZaXJWdjzesNnkiSd7ThrUViYVdXaA8JXXpqDZJLjHzY9Zx_Iflrb5KAqDA-uhD5C8TfiwB_t864KXNJx1RPqL3srYvZVLRzs-tbByo_anK7LWK7vSbwWEhEUahwEo3P3YtbEUhCA2lw5io3fh0WSFbYh77ERAt5fYAfME3u6iegI_JLq6EaU6_CcL-nYTV4-PHz6-v8ATaIxGT0NZMA7XIWNPC2RZV9GXWFE0MsjBYA_f9Gg8TXIgHSPTNyRyZ2H-ZZpqQx42w3r01nk6_E-lH9a1SrwPZNZLYX5ObZnPwle95BwBKOPgVHcW5-St1XpKOHrb90k1CcT0qBvh84";
    private static final String TAG = "MyFatoraInit";
    Context context;
    MutableLiveData<Mutable> liveData;

    public MyFatoraInit(Context context, MutableLiveData<Mutable> liveData) {
        this.context = context;
        this.liveData = liveData;
        MFSDK.INSTANCE.init(MFBaseURL.LIVE.getUrl(), API_KEY);
    }

    public void initiatePayment(double invoiceAmount) {
        liveData.setValue(new Mutable(Constants.SHOW_PROGRESS));
        MFInitiatePaymentRequest request = new MFInitiatePaymentRequest(
                invoiceAmount, MFCurrencyISO.SAUDI_ARABIA_SAR);

        MFSDK.INSTANCE.initiatePayment(request, LanguagesHelper.getCurrentLanguage().equals("en") ? MFAPILanguage.EN : MFAPILanguage.AR,
                (MFResult<MFInitiatePaymentResponse> result) -> {
                    liveData.setValue(new Mutable(Constants.HIDE_PROGRESS));
                    if (result instanceof MFResult.Success) {
                        Log.d(TAG, "Response: " + new Gson().toJson(
                                ((MFResult.Success<MFInitiatePaymentResponse>) result).getResponse()));
                        MFInitiatePaymentResponse paymentResponse = ((MFResult.Success<MFInitiatePaymentResponse>) result).getResponse();
                        liveData.setValue(new Mutable(Constants.MY_FATOORA_METHODS, paymentResponse));
                    } else if (result instanceof MFResult.Fail) {
                        Log.d(TAG, "Error: " + new Gson().toJson(((MFResult.Fail) result).getError()));
                    }
                    return Unit.INSTANCE;
                });
    }


    public void sendPayment(int payMethod, double invoiceAmount) {
        MFExecutePaymentRequest request = new MFExecutePaymentRequest(payMethod, invoiceAmount);
        request.setCustomerName(UserHelper.getInstance(MyApplication.getInstance()).getUserData().getName());
        request.setCustomerEmail(UserHelper.getInstance(MyApplication.getInstance()).getUserData().getEmail()); // The email required if you choose MFNotificationOption.ALL or MFNotificationOption.EMAIL
        request.setCustomerMobile(UserHelper.getInstance(MyApplication.getInstance()).getUserData().getPhone()); // The mobile required if you choose MFNotificationOption.ALL or MFNotificationOption.SMS
        request.setMobileCountryCode(MFMobileISO.SAUDI_ARABIA);
        request.setDisplayCurrencyIso(MFCurrencyISO.SAUDI_ARABIA_SAR);
        request.setUserDefinedField("CK-Driver" + UserHelper.getInstance(MyApplication.getInstance()).getUserData().getId());
        MFSDK.INSTANCE.executePayment((Activity) context, request, LanguagesHelper.getCurrentLanguage().equals("en") ? MFAPILanguage.EN : MFAPILanguage.AR,
                (String invoiceId, MFResult<MFGetPaymentStatusResponse> result) -> {
                    if (result instanceof MFResult.Success) {
                        Log.e(TAG, "Response: " + new Gson().toJson(((MFResult.Success<MFGetPaymentStatusResponse>) result)));
                        MFGetPaymentStatusResponse response = ((MFResult.Success<MFGetPaymentStatusResponse>) result).getResponse();
                        if (Objects.requireNonNull(response.getInvoiceStatus()).equals("Paid")) {
                            liveData.setValue(new Mutable(Constants.SEND_PAYMENT, response));
                        }
                    } else if (result instanceof MFResult.Fail) {
                        Log.e(TAG, "Error: " + new Gson().toJson(((MFResult.Fail) result).getError()));
                    }
                    Log.e(TAG, "invoiceId: " + invoiceId);
                    return Unit.INSTANCE;
                });
    }

    public void customPaymentUI(int paymentMethod, double invoiceAmount, SendPaymentRequest sendPaymentRequest) {
        MFExecutePaymentRequest request = new MFExecutePaymentRequest(paymentMethod, invoiceAmount);
        request.setCustomerEmail(UserHelper.getInstance(MyApplication.getInstance()).getUserData().getEmail()); // The email required if you choose MFNotificationOption.ALL or MFNotificationOption.EMAIL
        request.setCustomerMobile(UserHelper.getInstance(MyApplication.getInstance()).getUserData().getPhone()); // The mobile required if you choose MFNotificationOption.ALL or MFNotificationOption.SMS
        request.setMobileCountryCode(MFMobileISO.SAUDI_ARABIA);

//        MFCardInfo mfCardInfo = new MFCardInfo("Your token here");
        String[] separated = sendPaymentRequest.getCardExpireDate().split("/");

        MFCardInfo mfCardInfo = new MFCardInfo(
                sendPaymentRequest.getCardNumber(),
                separated[0],
                separated[1],
                sendPaymentRequest.getCardCvv(),
                sendPaymentRequest.getCardHolderName(),
                false,
                sendPaymentRequest.isSave()
        );
        MFSDK.INSTANCE.executeDirectPayment((Activity) context, request, mfCardInfo, MFAPILanguage.EN,
                (String invoiceId, MFResult<MFDirectPaymentResponse> result) -> {
                    liveData.setValue(new Mutable(Constants.HIDE_PROGRESS));
                    if (result instanceof MFResult.Success) {
                        Log.e(TAG, "Response: " + new Gson().toJson(
                                ((MFResult.Success<MFDirectPaymentResponse>) result).getResponse()));
                    } else if (result instanceof MFResult.Fail) {
                        Log.e(TAG, "Error: " + new Gson().toJson(((MFResult.Fail) result).getError()));
                    }
                    return Unit.INSTANCE;
                });
    }

}
