package amrk.app.amrk_driver.pages.settings.models.carCategories;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_driver.model.base.StatusMessage;

public class CarCategoriesResponse extends StatusMessage {

	@SerializedName("data")
	private List<CarLevels> data;

	public List<CarLevels> getData(){
		return data;
	}

}