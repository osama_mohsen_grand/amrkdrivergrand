package amrk.app.amrk_driver.pages.auth.login;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.base.BaseFragment;
import amrk.app.amrk_driver.base.IApplicationComponent;
import amrk.app.amrk_driver.base.MyApplication;
import amrk.app.amrk_driver.databinding.FragmentLoginBinding;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.model.base.StatusMessage;
import amrk.app.amrk_driver.pages.auth.confirmCode.ConfirmCodeFragment;
import amrk.app.amrk_driver.pages.auth.forgetPassword.ForgetPasswordFragment;
import amrk.app.amrk_driver.pages.auth.models.UsersResponse;
import amrk.app.amrk_driver.pages.auth.register.RegisterFragment;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.helper.MovementHelper;
import amrk.app.amrk_driver.utils.session.LanguagesHelper;
import amrk.app.amrk_driver.utils.session.UserHelper;

public class LoginFragment extends BaseFragment {
    private Context context;
    FragmentLoginBinding binding;
    @Inject
    LoginViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
//        binding.ccp.setCustomMasterCountries(UserHelper.getInstance(context).getCountryCodes());
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
//        viewModel.cpp = binding.ccp.getDefaultCountryCode();
//        binding.ccp.setOnCountryChangeListener(() -> {
//            if (viewModel.getLoginRequest().getPhone().contains(viewModel.cpp)) {
//                viewModel.getLoginRequest().setPhone(viewModel.getLoginRequest().getPhone().replace(viewModel.cpp, ""));
//            }
//            viewModel.cpp = binding.ccp.getSelectedCountryCode();
//        });

        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (((Mutable) o).message) {
                case Constants.PHONE_VERIFIED:
                    toastMessage(((StatusMessage) mutable.object).mMessage);
                    viewModel.setLoginStatus(View.GONE);
                    break;
                case Constants.LOGIN:
                    toastMessage(((StatusMessage) mutable.object).mMessage);
                    UserHelper.getInstance(context).userLogin(((UsersResponse) ((Mutable) o).object).getData());
                    MovementHelper.startActivityMain(context);
                    break;
                case Constants.FORGET_PASSWORD:
                    MovementHelper.startActivity(context, ForgetPasswordFragment.class.getName(), null, null);
                    break;
                case Constants.ERROR_NOT_FOUND:
                    if (viewModel.getLoginStatus() == View.VISIBLE)
                        MovementHelper.startActivity(context, RegisterFragment.class.getName(), null, null);
                    else
                        showError(String.valueOf(mutable.object));
                    break;
                case Constants.NOT_VERIFIED:
                    MovementHelper.startActivity(context, ConfirmCodeFragment.class.getName(), null, null);
                    break;
                case Constants.REGISTER:
                    MovementHelper.startActivity(context, RegisterFragment.class.getName(), null, null);
                    break;
                case Constants.PRIVACY:
                    MovementHelper.openCustomTabs(context, Constants.POLICY_URL + "2/" + LanguagesHelper.getCurrentLanguage(), getString(R.string.privacy));
//                    MovementHelper.startActivityWithBundle(context, new PassingObject(Constants.PRIVACY), getString(R.string.privacy), TermsFragment.class.getName(), null, null);
                    break;
                case Constants.TERMS:
                    MovementHelper.openCustomTabs(context, Constants.TERMS_URL + "2/" + LanguagesHelper.getCurrentLanguage(), getString(R.string.terms));
//                    MovementHelper.startPaymentActivityForResultWithBundle(context, Constants.TERMS_URL + 0 + "/" + LanguagesHelper.getCurrentLanguage(), getString(R.string.terms));
//                    MovementHelper.startActivityWithBundle(context, new PassingObject(Constants.TERMS), getString(R.string.terms), TermsFragment.class.getName(), null, null);
                    break;

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
