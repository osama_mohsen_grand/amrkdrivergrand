package amrk.app.amrk_driver.pages.notifications.viewModels;


import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.notifications.adapters.NotificationsAdapter;
import amrk.app.amrk_driver.repository.SettingsRepository;
import io.reactivex.disposables.CompositeDisposable;

public class NotificationsViewModels extends BaseViewModel {
    private NotificationsAdapter notificationsAdapter;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    SettingsRepository settingsRepository;
    public MutableLiveData<Mutable> liveData;

    @Inject
    public NotificationsViewModels(SettingsRepository settingsRepository) {
        this.settingsRepository = settingsRepository;
        this.liveData = new MutableLiveData<>();
        settingsRepository.setLiveData(liveData);
    }

    public void notifications() {
        compositeDisposable.add(settingsRepository.getNotifications());
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public SettingsRepository getSettingsRepository() {
        return settingsRepository;
    }

    @Bindable
    public NotificationsAdapter getNotificationsAdapter() {
        return this.notificationsAdapter == null ? this.notificationsAdapter = new NotificationsAdapter() : this.notificationsAdapter;
    }
}
