package amrk.app.amrk_driver.pages.history.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_driver.model.base.StatusMessage;


public class HistoryResponse extends StatusMessage {

    @SerializedName("data")
    private HistoryMainData data;

    public HistoryMainData getData() {
        return data;
    }
}