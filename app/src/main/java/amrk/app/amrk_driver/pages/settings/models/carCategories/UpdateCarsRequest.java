package amrk.app.amrk_driver.pages.settings.models.carCategories;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateCarsRequest {
    @SerializedName("car_levels")
    private List<Integer> selectedCar;

    public UpdateCarsRequest(List<Integer> selectedCar) {
        this.selectedCar = selectedCar;
    }

    public List<Integer> getSelectedCar() {
        return selectedCar;
    }

    public void setSelectedCar(List<Integer> selectedCar) {
        this.selectedCar = selectedCar;
    }
}
