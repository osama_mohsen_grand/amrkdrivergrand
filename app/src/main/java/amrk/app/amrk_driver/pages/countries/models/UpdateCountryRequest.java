package amrk.app.amrk_driver.pages.countries.models;

import com.google.gson.annotations.SerializedName;

public class UpdateCountryRequest {
    @SerializedName("user_country_id")
    private int userCountryId;

    public UpdateCountryRequest(int userCountryId) {
        this.userCountryId = userCountryId;
    }

    public int getUserCountryId() {
        return userCountryId;
    }

    public void setUserCountryId(int userCountryId) {
        this.userCountryId = userCountryId;
    }
}
