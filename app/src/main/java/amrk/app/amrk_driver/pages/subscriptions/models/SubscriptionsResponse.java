package amrk.app.amrk_driver.pages.subscriptions.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_driver.model.base.StatusMessage;

public class SubscriptionsResponse extends StatusMessage {

    @SerializedName("data")
    private SubscriptionsData data;

    public SubscriptionsData getData() {
        return data;
    }

}