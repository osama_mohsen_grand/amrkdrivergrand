package amrk.app.amrk_driver.pages.collectMoney.viewModels;

import android.text.TextUtils;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_driver.BR;
import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.collectMoney.models.CollectMoneyRequest;
import amrk.app.amrk_driver.pages.home.models.TripDataFromNotifications;
import amrk.app.amrk_driver.repository.HomeRepository;
import amrk.app.amrk_driver.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;


public class CollectViewModels extends BaseViewModel {
    TripDataFromNotifications tripDataFromNotifications;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    CollectMoneyRequest collectMoneyRequest;
    public MutableLiveData<Mutable> liveData;
    @Inject
    HomeRepository homeRepository;
    public ObservableField<Boolean> visibleField = new ObservableField<>();

    @Inject
    public CollectViewModels(HomeRepository homeRepository) {
        collectMoneyRequest = new CollectMoneyRequest();
        tripDataFromNotifications = new TripDataFromNotifications();
        this.homeRepository = homeRepository;
        this.liveData = new MutableLiveData<>();
        homeRepository.setLiveData(liveData);
    }

    public CollectMoneyRequest getCollectMoneyRequest() {
        return collectMoneyRequest;
    }

    public void collectMoney() {
        getCollectMoneyRequest().setTripId(getTripDataFromNotifications().getId());
        if (!TextUtils.isEmpty(getCollectMoneyRequest().getMoney()))
//            compositeDisposable.add(homeRepository.collectMoney(getCollectMoneyRequest()));
            liveData.setValue(new Mutable(Constants.COLLECT_MONEY));
    }

    public void putReset() {
        visibleField.set(true);
        setMessage(Constants.SHOW_PROGRESS);
        compositeDisposable.add(homeRepository.putReset(getTripDataFromNotifications().getTripTotal(), getTripDataFromNotifications().getId()));
    }

    public void pickMoney() {
        liveData.setValue(new Mutable(Constants.PICK_MONEY));
    }

    public void close() {
        liveData.setValue(new Mutable(Constants.HOME));
    }

    @Bindable
    public TripDataFromNotifications getTripDataFromNotifications() {
        return tripDataFromNotifications;
    }

    @Bindable
    public void setTripDataFromNotifications(TripDataFromNotifications tripDataFromNotifications) {
        notifyChange(BR.tripDataFromNotifications);
        this.tripDataFromNotifications = tripDataFromNotifications;
    }

    public HomeRepository getHomeRepository() {
        return homeRepository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}

