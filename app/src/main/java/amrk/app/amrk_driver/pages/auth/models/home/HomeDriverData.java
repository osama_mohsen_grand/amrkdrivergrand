package amrk.app.amrk_driver.pages.auth.models.home;

import com.google.gson.annotations.SerializedName;

public class HomeDriverData {

    @SerializedName("collectedMoneyToday")
    private double collectedMoneyToday;

    @SerializedName("files_completed")
    private int filesCompleted;

    public HomeDriverData() {
    }

    public HomeDriverData(double collectedMoneyToday, int filesCompleted) {
        this.collectedMoneyToday = collectedMoneyToday;
        this.filesCompleted = filesCompleted;
    }

    public double getCollectedMoneyToday() {
        return collectedMoneyToday;
    }

    public void setCollectedMoneyToday(double collectedMoneyToday) {
        this.collectedMoneyToday = collectedMoneyToday;
    }

    public int getFilesCompleted() {
        return filesCompleted;
    }
}