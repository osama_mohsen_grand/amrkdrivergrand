package amrk.app.amrk_driver.pages.countries.models;

import com.google.gson.annotations.SerializedName;

public class Countries {

    @SerializedName("image")
    private String image;
    @SerializedName("currency")
    private String currency;

    @SerializedName("code")
    private String code;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;

    public String getImage() {
        return image;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getCurrency() {
        return currency;
    }
}