package amrk.app.amrk_driver.pages.settings.viewModels;

import androidx.lifecycle.MutableLiveData;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.model.base.Mutable;
import io.reactivex.disposables.CompositeDisposable;

public class MyAccountSettingsViewModel extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public MyAccountSettingsViewModel() {
        this.liveData = new MutableLiveData<>();
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
