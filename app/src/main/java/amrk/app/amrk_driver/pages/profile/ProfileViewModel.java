package amrk.app.amrk_driver.pages.profile;

import android.text.TextUtils;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import javax.inject.Inject;

import amrk.app.amrk_driver.base.BaseViewModel;
import amrk.app.amrk_driver.connection.FileObject;
import amrk.app.amrk_driver.model.base.Mutable;
import amrk.app.amrk_driver.pages.auth.models.RegisterRequest;
import amrk.app.amrk_driver.repository.AuthRepository;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.validation.Validate;
import io.reactivex.disposables.CompositeDisposable;

public class ProfileViewModel extends BaseViewModel {
    MutableLiveData<Mutable> liveData;
    ArrayList<FileObject> fileObject;
    @Inject
    AuthRepository repository;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    public RegisterRequest request;

    @Inject
    public ProfileViewModel(AuthRepository repository) {
        fileObject = new ArrayList<>();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
        request = new RegisterRequest();
    }

    public void submit() {
        if (!TextUtils.isEmpty(request.getPassword()) && !TextUtils.isEmpty(request.getConfirmPassword()))
            if (Validate.isMatchPassword(request.getPassword(), request.getConfirmPassword())) {
                setMessage(Constants.SHOW_PROGRESS);
                compositeDisposable.add(repository.updateProfile(request, fileObject));
            } else
                liveData.setValue(new Mutable(Constants.NOT_MATCH_PASSWORD));
        else if (getFileObject().size() > 0) {
            request.setPassword(null);
            request.setConfirmPassword(null);
            setMessage(Constants.SHOW_PROGRESS);
            compositeDisposable.add(repository.updateProfile(request, fileObject));
        }
    }

    public ArrayList<FileObject> getFileObject() {
        return fileObject;
    }

    public void imageSubmit() {
        liveData.setValue(new Mutable(Constants.IMAGE));
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
