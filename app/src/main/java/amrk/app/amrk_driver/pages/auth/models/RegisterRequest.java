package amrk.app.amrk_driver.pages.auth.models;

import android.text.TextUtils;

import androidx.databinding.ObservableField;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_driver.R;
import amrk.app.amrk_driver.utils.Constants;
import amrk.app.amrk_driver.utils.resources.ResourceManager;
import amrk.app.amrk_driver.utils.validation.Validate;

public class RegisterRequest {
    @SerializedName("f_name")
    private String name;
    @SerializedName("l_name")
    private String l_name;
    @SerializedName("phone")
    private String phone;
    @SerializedName("password")
    private String password;
    private String confirmPassword;
    @SerializedName("country_id")
    private String country_id;
    @SerializedName("city_id")
    private String city_id;
    @SerializedName("token")
    private String token;
    @SerializedName("email")
    private String email;
    @SerializedName("national_id_type")
    private String national_id_type;
    @SerializedName("national_id")
    private String national_id;
    @SerializedName("car_num")
    private String car_num;
    @SerializedName("car_level")
    private String car_level;
    @SerializedName("bank_name")
    private String bank_name;
    @SerializedName("bank_account_name")
    private String bank_account_name;
    @SerializedName("bank_account_num")
    private String bank_account_num;
    @SerializedName("car_color")
    private String car_color;
    @SerializedName("color_name")
    private String color_name;
    private String user_image;
    @SerializedName("gender")
    private String gender;
    @SerializedName("online")
    private String online;
    @SerializedName("subscription_type")
    private String subscription_type;
    @SerializedName("car_name")
    private String carName;
    @SerializedName("car_model")
    private String carModel;
    @SerializedName("lang")
    private String lang;
    @Expose
    @SerializedName("city")
    private String cityName;
    private transient String backImage;
    private transient String frontImage;
    private transient String insuranceImage;
    private transient String licenseImage;
    private transient String civilImage;
    private transient String identityImage;
    private transient int identityNum;

    public transient ObservableField<String> nameError = new ObservableField<>();
    public transient ObservableField<String> l_nameError = new ObservableField<>();
    public transient ObservableField<String> phoneError = new ObservableField<>();
    public transient ObservableField<String> passwordError = new ObservableField<>();
    public transient ObservableField<String> emailError = new ObservableField<>();
    public transient ObservableField<String> national_id_typeError = new ObservableField<>();
    public transient ObservableField<String> national_idError = new ObservableField<>();
    public transient ObservableField<String> car_numError = new ObservableField<>();
    public transient ObservableField<String> car_levelError = new ObservableField<>();
    public transient ObservableField<String> bank_nameError = new ObservableField<>();
    public transient ObservableField<String> bank_account_nameError = new ObservableField<>();
    public transient ObservableField<String> bank_account_numError = new ObservableField<>();
    public transient ObservableField<String> subscription_typeError = new ObservableField<>();
    public transient ObservableField<String> carNameError = new ObservableField<>();
    public transient ObservableField<String> carModelError = new ObservableField<>();
    public transient ObservableField<String> backImageError = new ObservableField<>();
    public transient ObservableField<String> frontImageError = new ObservableField<>();
    public transient ObservableField<String> insuranceImageError = new ObservableField<>();
    public transient ObservableField<String> licenseImageError = new ObservableField<>();
    public transient ObservableField<String> civilImageError = new ObservableField<>();
    public transient ObservableField<String> identityImageError = new ObservableField<>();
    public transient ObservableField<String> colorError = new ObservableField<>();
    public transient ObservableField<String> colorTextError = new ObservableField<>();
    public transient ObservableField<String> cityError = new ObservableField<>();

    public RegisterRequest() {
    }

    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(name, Constants.FIELD)) {
            nameError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(l_name, Constants.FIELD)) {
            l_nameError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(phone, Constants.FIELD)) {
            phoneError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(cityName, Constants.FIELD)) {
            cityError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(password, Constants.FIELD)) {
            passwordError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(national_id_type, Constants.FIELD)) {
            national_id_typeError.set(Validate.error);
            valid = false;
        }
        if (TextUtils.isEmpty(national_id)) {
            national_idError.set(ResourceManager.getString(R.string.fieldRequired).concat(" ( " + identityNum + " )").concat(" ").concat(ResourceManager.getString(R.string.number)));
            valid = false;
        } else if (!TextUtils.isEmpty(national_id)) {
            if (national_id.length() != identityNum) {
                national_idError.set(ResourceManager.getString(R.string.fieldRequired).concat(" ( " + identityNum + " )").concat(" ").concat(ResourceManager.getString(R.string.number)));
                valid = false;
            }
        }
        if (!Validate.isValid(subscription_type, Constants.FIELD)) {
            subscription_typeError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(car_level, Constants.FIELD)) {
            car_levelError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(carName, Constants.FIELD)) {
            carNameError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(carModel, Constants.FIELD)) {
            carModelError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(car_num, Constants.FIELD)) {
            car_numError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(frontImage, Constants.FIELD)) {
            frontImageError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(backImage, Constants.FIELD)) {
            backImageError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(insuranceImage, Constants.FIELD)) {
            insuranceImageError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(licenseImage, Constants.FIELD)) {
            licenseImageError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(civilImage, Constants.FIELD)) {
            civilImageError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(identityImage, Constants.FIELD)) {
            identityImageError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(color_name, Constants.FIELD)) {
            colorTextError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(car_color, Constants.FIELD)) {
            colorError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(bank_name, Constants.FIELD)) {
            bank_nameError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(bank_account_num, Constants.FIELD)) {
            bank_account_numError.set(Validate.error);
            valid = false;
        }
        if (!Validate.isValid(bank_account_name, Constants.FIELD)) {
            bank_account_nameError.set(Validate.error);
            valid = false;
        }
        return valid;
//
//        return (!TextUtils.isEmpty(name)
//                && !TextUtils.isEmpty(l_name)
//                && !TextUtils.isEmpty(phone)
//                && !TextUtils.isEmpty(password)
//                && !TextUtils.isEmpty(token)
//                && !TextUtils.isEmpty(bank_account_num)
//                && !TextUtils.isEmpty(bank_account_name)
//                && !TextUtils.isEmpty(bank_name)
//                && !TextUtils.isEmpty(carName)
//                && !TextUtils.isEmpty(car_num)
//                && !TextUtils.isEmpty(national_id)
//                && !TextUtils.isEmpty(national_id_type)
//                && !TextUtils.isEmpty(car_num)
////                && !TextUtils.isEmpty(country_id)
////                && !TextUtils.isEmpty(city_id)
//                && !TextUtils.isEmpty(car_level)
//                && !TextUtils.isEmpty(carModel)
////                && !TextUtils.isEmpty(color_name)
//        );
    }

    public boolean isUpdateValid() {
        return (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(phone));
    }

    public boolean isPasswordsValid() {
        return (!TextUtils.isEmpty(password) && !TextUtils.isEmpty(confirmPassword));
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        nameError.set(null);
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        phoneError.set(null);
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        passwordError.set(null);
        this.password = password;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        emailError.set(null);
        this.email = email;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        l_nameError.set(null);
        this.l_name = l_name;
    }

    public String getNational_id_type() {
        return national_id_type;
    }

    public void setNational_id_type(String national_id_type) {
        national_id_typeError.set(null);
        this.national_id_type = national_id_type;
    }

    public String getNational_id() {
        return national_id;
    }

    public void setNational_id(String national_id) {
        national_idError.set(null);
        this.national_id = national_id;
    }

    public String getCar_num() {
        return car_num;
    }

    public void setCar_num(String car_num) {
        car_numError.set(null);
        this.car_num = car_num;
    }

    public String getCarName() {
        carNameError.set(null);
        return carName;
    }

    public void setCarName(String carName) {
        carNameError.set(null);
        this.carName = carName;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        carModelError.set(null);
        this.carModel = carModel;
    }

    public String getCar_level() {
        return car_level;
    }

    public void setCar_level(String car_level) {
        car_levelError.set(null);
        this.car_level = car_level;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        bank_nameError.set(null);
        this.bank_name = bank_name;
    }

    public String getBank_account_name() {
        return bank_account_name;
    }

    public void setBank_account_name(String bank_account_name) {
        bank_account_nameError.set(null);
        this.bank_account_name = bank_account_name;
    }

    public String getBank_account_num() {
        return bank_account_num;
    }

    public void setBank_account_num(String bank_account_num) {
        bank_account_numError.set(null);
        this.bank_account_num = bank_account_num;
    }

    public String getCar_color() {
        return car_color;
    }

    public void setCar_color(String car_color) {
        colorError.set(null);
        this.car_color = car_color;
    }

    public String getColor_name() {
        return color_name;
    }

    public void setColor_name(String color_name) {
        colorTextError.set(null);
        this.color_name = color_name;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getSubscription_type() {
        return subscription_type;
    }

    public void setSubscription_type(String subscription_type) {
        subscription_typeError.set(null);
        this.subscription_type = subscription_type;
    }

    public String getBackImage() {
        return backImage;
    }

    public void setBackImage(String backImage) {
        backImageError.set(null);
        this.backImage = backImage;
    }

    public String getFrontImage() {
        return frontImage;
    }

    public void setFrontImage(String frontImage) {
        frontImageError.set(null);
        this.frontImage = frontImage;
    }

    public String getInsuranceImage() {
        return insuranceImage;
    }

    public void setInsuranceImage(String insuranceImage) {
        insuranceImageError.set(null);
        this.insuranceImage = insuranceImage;
    }

    public String getLicenseImage() {
        return licenseImage;
    }

    public void setLicenseImage(String licenseImage) {
        licenseImageError.set(null);
        this.licenseImage = licenseImage;
    }

    public String getCivilImage() {
        return civilImage;
    }

    public void setCivilImage(String civilImage) {
        civilImageError.set(null);
        this.civilImage = civilImage;
    }

    public String getIdentityImage() {
        return identityImage;
    }

    public void setIdentityImage(String identityImage) {
        identityImageError.set(null);
        this.identityImage = identityImage;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public int getIdentityNum() {
        return identityNum;
    }

    public void setIdentityNum(int identityNum) {
        this.identityNum = identityNum;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        cityError.set(null);
        this.cityName = cityName;
    }
}
